package GNSL53_P;
@ISA = ("RUNBPE");
# ============================================================================
#
# Name    :  GNSL53_P
#
# Purpose :  Execute GPSEST for L5/L3 ambiguity resolution
#
# Remark  :  Using the variable V_GNSSAR = ALL/GPS is defined whether a
#            also GLONASS or only GPS ambiguities are resolved
#
#            All previously resolved ambiguities for this baseline are
#            re-initialized
#
# PARAMs  :  PARAM1 - 4-character baseline name
#
# Author  :  S. Schaer
# Created :  30-Mar-2005
#
# Changes :  11-Aug-2011 RD: Updated for version 5.2
#
# ============================================================================
use strict;

use lib $ENV{BPE};
use bpe_util qw(prtMess setMinusPlus setUserVar);

sub run {
  my $bpe = shift;

# Get variables
# -------------
  my ($ssss,
      $param1, $gnssar) =
  $bpe->getKeys(
      '$S+0',
      'PARAM1', 'V_GNSSAR');

  setMinusPlus($bpe,0,0);

# Set filename variable
# ---------------------
  setUserVar($bpe,"FFFF",$param1);

# Select specific processing options, if desired
# ----------------------------------------------
  unless ($gnssar eq "") {
    $bpe->putKey("$ENV{U}/PAN/GPSEST.INP","SATSYS",$gnssar);
    $bpe->putKey("$ENV{U}/PAN/GPSEST.INP","SATSYS_AR",$gnssar);
    prtMess($bpe,"GNSS selection for ambiguity resolution L53: $gnssar");
  }

# Run program
# -----------
  my $PGMNAM = "SATMRK";
  $bpe->RUN_PGMS($PGMNAM);

# DO NOT INTRODUCE L5 INTEGERS:
  $bpe->putKey("$ENV{U}/PAN/GPSEST.INP","APRWIDE","0");

  $PGMNAM = "GPSEST";
  $bpe->RUN_PGMS($PGMNAM);

# DO NOT INTRODUCE L1/L2 INTEGERS:
  $bpe->putKey("$ENV{U}/PAN/GPSEST.INP","APRL1L2","0");
  $bpe->putKey("$ENV{U}/PAN/GPSEST.INP","SYSOUT","\$(FFFF)\$S+0_3");
  $bpe->putKey("$ENV{U}/PAN/GPSEST.INP","IONOS","");
  $bpe->putKey("$ENV{U}/PAN/GPSEST.INP","FREQUENCY","L3");
  $bpe->putKey("$ENV{U}/PAN/GPSEST.INP","APRWIDE","1");

  $PGMNAM = "GPSEST";
  $bpe->RUN_PGMS($PGMNAM);

}
