package PPPEDTAP;
@ISA = ("RUNBPE");
# ============================================================================
#
# Name    :  PPPEDTAP
#
# Purpose :  Prepare parallel precise-point-positioning (PPP) analysis
#
# PARAMs  :  PARAM2 - output file identifier (default: V_C)
#            PARAM3 - empty: stop if no PZH files are found
#
# Author  :  M. Meindl
# Created :  23-Jul-2003
#
# Changes :  31-Jan-2005 MM: New initPar call (w/o tmpFil)
#            11-Aug-2011 RD: Updated for version 5.2
#
# ============================================================================
use strict;

use File::Basename;

use lib $ENV{BPE};
use bpe_util qw(prtErr initPar_Bl initPar_Cl);

sub run {
  my $bpe = shift;

# Get variables
# -------------
  my ($ssss,
      $clu, $dirBpe, $c, $param2, $param3,
      $dirPzh,   $dirOut,   $dirRes,   $dirEdt,   $dirNeq,   $dirCrd,
      $extPzh,   $extOut,   $extRes,   $extEdt,   $extNeq,   $extCrd,
      $dirTrp,   $dirTro,   $dirSnx,   $dirRxc,   $dirSum,
      $extTrp,   $extTro,   $extSnx,   $extRxc,   $extSum) =
  $bpe->getKeys
     ('$S+0',
      'V_CLU', 'DIR_BPEPRT', 'V_C', 'PARAM2', 'PARAM3',
      'DIR_PZH', 'DIR_OUT', 'DIR_RES', 'DIR_EDT', 'DIR_NEQ', 'DIR_CRD',
      'EXT_PZH', 'EXT_OUT', 'EXT_RES', 'EXT_EDT', 'EXT_NEQ', 'EXT_CRD',
      'DIR_TRP', 'DIR_TRO', 'DIR_SNX', 'DIR_RXC', 'DIR_SUM',
      'EXT_TRP', 'EXT_TRO', 'EXT_SNX', 'EXT_RXC', 'EXT_SUM');


# Delete old files (OUT, RES, NEQ, EDT)
# -------------------------------------
  my $cluster = $clu ne "" ? "${ssss}???" : "${ssss}????";

  my $edt = "EDT";
  if ( $param2 ne "" ) {
    $edt = defined $$bpe{$param2} ? $$bpe{$param2} : $param2;
  }
  unlink glob("${dirSum}WRN${cluster}.${extSum}");
  unlink glob("${dirOut}ED[FL]${cluster}.${extOut}");
  unlink glob("${dirRes}ED[FL]${cluster}.${extRes}");
  unlink glob("${dirEdt}EDT${cluster}.${extEdt}");
  unlink glob("${dirNeq}${edt}${cluster}.${extNeq}");
  unlink glob("${dirCrd}${edt}${cluster}.${extCrd}");
  unlink glob("${dirTrp}${edt}${cluster}.${extTrp}");
  unlink glob("${dirTro}${edt}${cluster}.${extTro}");
  unlink glob("${dirSnx}${edt}${cluster}.${extSnx}");
  unlink glob("${dirRxc}${edt}${cluster}.${extRxc}");

# Initialize parallel run
# -----------------------
  my $bpeFil = "${dirBpe}EDT${ssss}";
  my @pzhFil = glob("${dirPzh}????${ssss}.${extPzh}");

  if ($clu ne "") {
    initPar_Cl($bpe,$bpeFil,@pzhFil,0,$clu,0);
  } else {
    @pzhFil = map { substr(basename($_),0,4) } @pzhFil;
    initPar_Bl($bpe,@pzhFil);
  }

# Stop if no CZH files found
# --------------------------
  if (@pzhFil == 0 && $param3 eq "") {
    prtErr($bpe,"No PZH files found") and
      die "No PZH files found for session ${ssss}";
  }

}
