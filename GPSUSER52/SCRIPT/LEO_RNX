package LEO_RNX;
@ISA = ("RUNBPE");
# ============================================================================
#
# Name    :  LEO_RNX
#
# Purpose :  Copy the observation files for LEOPOD.PCF
#
# PARAMs  :  PARAM1=MEXTJOB
#            PARAM3=Next PID if no obervation files have been copied
#                   (typically the last PID of the BPE: 999)
#
# Author  :  H. Bock
# Created :  05-Mar-2012
#
# Changes :
#
# ============================================================================
use strict;

use File::Basename;
use File::Copy;

use lib $ENV{BPE};
use bpe_util qw(prtGoto crz2rnx);

sub run{
  my $bpe = shift;

# Get variables
# -------------
  my ($leo   , $leoDir, $attinf, $ssss,  $yy, $param3,
      $dirRxo,  $dirAtt,  $extRxo,  $extAtt ) =
  $bpe->getKeys
     ('V_LEO','V_LEODIR','V_ATTINF', '$S+0', '$Y', 'PARAM3',
      'DIR_RXO','DIR_ATT','EXT_RXO','EXT_ATT');

# Copy the RINEX files into the RAW-directory of the campaign
# -----------------------------------------------------------
  my $rnxDir = "$ENV{D}/$leoDir/RINEX/";
  crz2rnx("${rnxDir}${leo}${ssss}.${yy}D.Z",${dirRxo},"-u");

# Copy the attitude files into the ORB-directory of the campaign
# --------------------------------------------------------------
  my $attDir = "$ENV{D}/$leoDir/ATTIT/";

  copy("${attDir}${attinf}.${extAtt}.gz",${dirAtt});
  system("gzip -df ${dirAtt}${attinf}.${extAtt}.gz");

# Jump to the end if no observation files found for this session
# --------------------------------------------------------------
  if ( $param3 ne "" && ! -s "${dirRxo}${leo}${ssss}.${extRxo}" ) {
    prtGoto($bpe,$param3);
  }
}
