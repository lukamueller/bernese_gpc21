package ION_MRG;
@ISA = ("RUNBPE");
# ============================================================================
#
# Name    :  ION_MRG
#
# Purpose :  Remove duplicated epochs from a Bernese ionosphere file
#
# PARAMs  :
#
# Author  :  R. Dach
# Created :  11-Aug-2011
#
# Changes :
#
# ============================================================================
use strict;


sub run{
  my $bpe = shift;

# Get variables
# -------------
  my ($yyssss,
      $hoiFil,
      $dirIon,
      $extIon) =
  $bpe->getKeys
     ('$YSS+0',
      'V_HOIFIL',
      'DIR_ION',
      'EXT_ION');



# Construct the ionosphere filename
# ---------------------------------
  my $ionFil = "${dirIon}${hoiFil}.${extIon}";
  if ( -e $ionFil ) {

    # Read the ionosphere file into @ion and create some statistics in %ionIdx
    my %ionIdx = ();
    my @ion = ();

    my $ii = 0;
    my $nam = "";

    open(ION,"$ionFil");
    while(<ION>) {

      push @ion,$_;

      # Save where a new model component starts
      if ( /MODEL NUMBER \/ STATION NAME/ ) {
        $ionIdx{$nam}{END} = $ii-3 unless $nam eq "";
        $nam = substr($_,49);
        $ionIdx{$nam}{BEG} = $ii-2;
      }

      # Save the epoch of the model component
      if ( /FROM EPOCH \/ REFERENCE EPOCH/ ) {
        $ionIdx{$nam}{EPO} = substr($_,49);
      }
      $ii++;
    }
    $ionIdx{$nam}{END} = #$ion-1;
    close HOI;

    # Check whether there are identical epochs
    my $lstEpo = "";
    foreach my $nam (sort {$ionIdx{$a}{EPO} cmp $ionIdx{$b}{EPO}} keys %ionIdx) {

      # If we have the same epoch more than once, flag one of them for removal
      if ( $lstEpo ne "" && $ionIdx{$lstEpo}{EPO} eq $ionIdx{$nam}{EPO} ) {
        my $tbd = $nam < $lstEpo ? $nam : $lstEpo;
        for ( my $iLin = $ionIdx{$nam}{BEG}; $iLin <= $ionIdx{$nam}{END}; $iLin++ ) {
          $ion[$iLin] = " >> tbd << ";
        }
      }
      $lstEpo = $nam;
    }

    # Write a new ionosphere file without double epochs
    open(ION,"> ${ionFil}");
    foreach my $lin ( @ion ) {
      print ION $lin unless $lin eq " >> tbd << ";
    }
    close ION;
  }

}

