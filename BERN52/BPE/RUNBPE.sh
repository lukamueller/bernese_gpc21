#!/bin/sh
# ==============================================================================
#
# Name:       RUNBPE.sh
#
# Purpose:    Shell interface for RUNBPE.pm Perl modules
#
# Authors:
#
# Created:    __-___-____
# Last mod.:  14-Jan-2011
#
# Changes:    14-Jan-2011 SL: use $BPE instead of full path
#
# ==============================================================================

$BPE/RUNBPE.pm $1 $2 $3 $4

# ==============================================================================
