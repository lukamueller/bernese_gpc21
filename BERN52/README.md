GitHub Project “Bernese”
Bernese software development and routines for automatic orbit determination

Introduction
Git is a distributed version control system. Clients fully mirror the repository (collection of files), including its full history to the local environment (clone). This allows to set up several types of workflows (non-linear development using branching). Every time you save (commit) your state, Git stores the files which have been changed and a link to the files which have not been changed in your local repository. After you commit, it is very difficult to lose any information, especially of you regularly push your database to another repository, e.g. on the gitlab (management software incl. repository management, code reviews, wikis, …) server.

Possible state of your data
- Modified means that you have changed the file but have not committed it to your database
- Staged means that you have marked a modified file to go into your next commit snapshot
- Committed means that the data is safely stored in your local database

Further information: https://git-scm.com/book/en/v2

Set up git(lab) environment
- In the following, the initial steps are described for setting up a git repository on your local machine.
- Go to https://gitlab.ethz.ch/, register for the service and login to the web applications (LDAP).
- Check if all relevant projects are listed. If not go to Explore projects and search for a missing project
- Open the project, go to Clone and copy the Clone with HTTPS link, e.g. https://gitlab.ethz.ch/gmoeller/bernese.git
- Install git on your local machine (sudo apt install git-all, requires admin rights). On a managed Linux machine git should be already available. You can check by typing git in your command line terminal. If git is installed, a list of git commands is provided


- Setup your identity using the following commands:
        git config user.name “Your first name surname”
        git config user.email “Your email address”

- Setup your default text editor (e.g. pico or emacs):
        git config core.editor pico

- Check your settings with: git config --list


- Download the project files to your target directory, e.g. BERN52: git clone https://gitlab.ethz.ch/gmoeller/bernese.git BERN52
    Enter your username (same as for https://gitlab.ethz.ch/) and password
- Now all files of this project are available on your local machine

Basic workflow
1.	Cloning the repository: git clone https://gitlab.ethz.ch/gmoeller/bernese.git BERN52
2.	Compile the Bernese software routines (if changes in the source code have been made)
    see document Astrocast_Bernese_installation_darkside.docx
3.	Start making changes in the files until you have reached a state you want to record
4.	Check file status: go to ../BERN52 and type in: git status
5.	Add untracked files/folders to repository (in their current status):
        git add <filename> or <foldername>
6.	Stage files/folders (i.e. mark modified files/folders in their current status for the next commit)
        git add <filename> or <foldername>
7.	Commit changes to your local repository
        git commit -> will open the editor: Enter commit message, e.g. Update list of campaigns, and exit editor (ctrl-x in pico editor).
    This will create a file COMMIT_EDITMSG and commit your changes. 
        git commit –m “Update list of campaigns” will do the same
    If you are sure about the files you have changed and want to commit. The two steps, add/stage and commit, can be combined using the
    following command:
        git commit –a
8.	Push changes to your remotes (e.g. gitlab server)
        Define server: git remote add <shortname> <url>
            git remote add origin https://gitlab.ethz.ch/gmoeller/bernese
	
    	Upload and merge files in a branch: git push <remote> <branch>
            git push –u origin master
		Enter user name and password (same as setup for https://gitlab.ethz.ch/)
