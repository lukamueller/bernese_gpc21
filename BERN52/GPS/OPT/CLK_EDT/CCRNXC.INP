
! Some keywords for automatic output
! ----------------------------------
CAMPAIGN 1  "./DUMMY"
  ## widget = initmenu; pointer = ACTIVE_CAMPAIGN

SESSION_TABLE 1  "${X}/SKL/SESSIONS.SES"
  ## widget = initmenu; pointer = SESSION_TABLE

DESCR_SESSION_TABLE 1  "Session table"

YR4_INFO 1  "1980"
  ## widget = comment
  # $Y+0

SES_INFO 1  "0060"
  ## widget = comment
  # $S+0

USR_INFO 1  "${USER}"
  ## widget = comment

! Environment Variables
! ---------------------
ENVIRONMENT 1  "" "${}"
  ## widget = initmenu; pointer = ENVIR_LIST

! Show general files
! ------------------
SHOWGEN 1  "1"
  ## widget = checkbox

MSG_SHOWGEN 1  "Show all general files"

! Clock RINEX input file name
! -----------------------------
RCLKINP 0
  ## widget = selwin; path = DIR_RXC; ext = EXT_RXC
  # $H$S+0???

RCLKINP_TXT_COL_1 1  "Clock rinex input files"

MSG_RCLKINP 1  "Clock RINEX files"

RCLKINP_TXT_COL_2 1  "Bernese satellite clock files"

RCLKINP_PTH_COL_2 1  "./DUMMY/ORB/"
  ## widget = initmenu; pointer = DIR_CLK

RCLKINP_EXT_COL_2 1  "CLK"
  ## widget = initmenu; pointer = EXT_CLK

! Replace station coordinates
! ---------------------------
COORD 1  "./DUMMY/STA/$A800060.CRD"
  ## widget = selwin; path = DIR_CRD; ext = EXT_CRD; maxfiles = 1
  ## emptyallowed = true
  # $A$YSS+0

DESCR_COORD 1  "Replace station coordinates"

! Clock RINEX output file name
! -----------------------------
RCLKOUT 1  "./DUMMY/OUT/$H_800060.CLK"
  ## widget = lineedit; path = DIR_RXC; ext = EXT_RXC
  # $H_$YSS+0

DESCR_RCLKOUT 1  "Combined clock RINEX file"

! Satellite clock file name (output)
! ----------------------------------
SCLKOUT 1  "./DUMMY/ORB/$H_800060.CLK"
  ## widget = lineedit; path = DIR_CLK; ext = EXT_CLK
  # $H_$YSS+0

DESCR_SCLKOUT 1  "Bernese satellite clock file"

! Sigma file with rms of lin. fit
! -------------------------------
LINFITRS 0
  ## widget = lineedit; path = DIR_SIG; ext = EXT_SIG
  # 

DESCR_LINFITRS 1  "Sigma file with linear fit RMS"

! Program Output File
! -------------------
SYSODEF 1  "0"
  ## widget = checkbox

MSG_SYSODEF 1  "Program output"

SYSOUT 1  "./DUMMY/OUT/$H_800060.OUT"
  ## widget = lineedit; path = DIR_OUT; ext = EXT_OUT
  ## activeif = SYSODEF == 0; emptyallowed = false
  # $H_$YSS+0

DESCR_SYSOUT 1  "Program output"

! Error Message File
! ------------------
ERRMRG 1  "0"
  ## widget = checkbox

MSG_ERRMRG 1  "Error message"

SYSERR 1  "${U}/WORK/ERROR.MSG"
  ## widget = lineedit; path = PTH_ERR; ext = EXT_ERR
  ## activeif = ERRMRG == 0; emptyallowed = false
  # ERROR

DESCR_SYSERR 1  "Error message"

! Title line for satellite clock file
! -----------------------------------
TITLE 1  "CLKDET_800060: Combine clock files from preprocessing (code+phase)"
  ## widget = lineedit; check_strlen = 80
  # CLKDET_$YSS+0: Combine clock files from preprocessing (code+phase)

MSG_TITLE 1  "Title line"

! Observation Window
! ------------------
USEWIN 1  "1"
  ## widget = checkbox

MSG_USEWIN 1  "Use time window"

RADIO_1 1  "1"
  ## widget = radiobutton; group = DUMMY
  ## radiokeys = SESSION_YEAR SESSION_STRG

MSG_RADIO_1 1  "Defined by session identifier, year and table"

RADIO_2 1  "0"
  ## widget = radiobutton; group = DUMMY

MSG_RADIO_2 1  "Defined by start and end times"

! Observation Window from Session Table
! -------------------------------------
SESSION_YEAR 1  "1980"
  ## widget = lineedit; multilineallowed = true; emptyallowed = false
  # $Y+0

MSG_SESSION_YEAR 1  "Year for sessions def. the time window"

SESSION_STRG 1  "0060"
  ## widget = lineedit; multilineallowed = true; emptyallowed = false
  # $S+0

MSG_SESSION_STRG 1  "Sessions defining the time window"

! Observation Window from Datum
! -----------------------------
STADAT 1  "1980 01 06"
  ## widget = lineedit; activeif = RADIO_2 = 1; check_type = date
  # $YMD_STR+0

MSG_STADAT 1  "Start of time window (year month day)"

STATIM 1  "00 00 00"
  ## widget = lineedit; emptyallowed = false
  ## activeif = RADIO_2 = 1 AND STADAT / _; check_type = time

MSG_STATIM 1  "Start of time window (hour min. sec.)"

ENDDAT 1  "1980 01 06"
  ## widget = lineedit; activeif = RADIO_2 = 1; check_type = date
  # $YMD_STR+0

MSG_ENDDAT 1  "End of time window (year month day)"

ENDTIM 1  "23 59 59"
  ## widget = lineedit; emptyallowed = false
  ## activeif = RADIO_2 = 1 AND ENDDAT / _; check_type = time

MSG_ENDTIM 1  "End of time window (hour min. sec.)"

! Clock Output: Sampling Rate
! ---------------------------
DELTAT 1  "$(SAMPL)"
  ## widget = lineedit; check_type = integer; check_min = 0

MSG_DELTAT 1  "Sampling rate for clocks"

! Selection of clocks for output
! ------------------------------
SELSTA 1  "ALL"
  ## widget = combobox; editable = false
  ## cards = NONE FROM_FILE FROM_LIST ALL

MSG_SELSTA 1  "Selection of stations clocks"

SELSAT 1  "ALL"
  ## widget = combobox; editable = false
  ## cards = NONE FROM_FILE FROM_LIST ALL

MSG_SELSAT 1  "Selection of satellites clocks"

SFILSTA 0
  ## widget = selwin; path = DIR_FIX; ext = EXT_FIX; maxfiles = 1
  ## emptyallowed = true; activeif = SELSTA = FROM_FILE
  # 

MSG_SFILSTA 1  "List from file"

SFILSAT 0
  ## widget = selwin; path = DIR_FIX; ext = EXT_FIX; maxfiles = 1
  ## emptyallowed = true; activeif = SELSAT = FROM_FILE
  # 

MSG_SFILSAT 1  "List from file"

SLSTSTA 0
  ## widget = selwin; emptyallowed = true; pointer = STALST
  ## activeif = SELSTA = FROM_LIST
  # 

MSG_SLSTSTA 1  "Manual selection"

SLSTSAT 0
  ## widget = selwin; emptyallowed = true; pointer = SATLST
  ## activeif = SELSAT = FROM_LIST
  # 

MSG_SLSTSAT 1  "Manual selection"

! Remove satellite clocks with eclipse flag
! -----------------------------------------
DELECLIPS 1  "0"
  ## widget = combobox; editable = false
  ## cards = KEEP REMOVE_FLAG_ONLY REMOVE_ALL_RECORDS

MSG_DELECLIPS 1  "Remove satellite clocks with eclipse flag"

! Use only Reference clocks for combination
! -----------------------------------------
ONLYREF 1  "0"
  ## widget = checkbox

MSG_ONLYREF 1  "Use only reference clocks"

! Use station clocks for combination
! ----------------------------------
USESTA 1  "1"
  ## widget = checkbox; activeif = ONLYREF = 0

MSG_USESTA 1  "Use all station clocks"

! Use satellite clocks for combination
! ------------------------------------
USESAT 1  "1"
  ## widget = checkbox; activeif = ONLYREF = 0

MSG_USESAT 1  "Use all satellite clocks"

! Apriori unit of weight
! ----------------------
SIGMA0 1  "0.02"
  ## widget = lineedit; check_type = real; check_min = 0.0

MSG_SIGMA0 1  "A priori sigma of unit weight"

! Maximal residual allowed
! ------------------------
MAXRESI 1  "99999"
  ## widget = lineedit; check_type = real; check_min = 0.0

MSG_MAXRESI 1  "Maximum residuum allowed"

! Strategy for combination
! ------------------------
COMBI 1  "INPUT_FILES"
  ## widget = combobox; editable = false
  ## cards = UNWEIGHTED COMBINATION INPUT_FILES

MSG_COMBI 1  "Strategy for computation of mean value"

! Maximal deviation from mean
! ---------------------------
MAXMEAN 1  "99999"
  ## widget = lineedit; check_type = real; check_min = 0.0

MSG_MAXMEAN 1  "Maximum deviation from mean"

! Minimum number of valid clocks for mean (satellites)
! ---------------------------------------
MINSAT 1  "1"
  ## widget = spinbox; range = 1 10 1

MSG_MINSAT 1  "Minimum number of valid clocks for mean (satellites)"

! Minimum number of valid clocks for mean (stations)
! ---------------------------------------
MINSTA 1  "1"
  ## widget = spinbox; range = 1 10 1

MSG_MINSTA 1  "Minimum number of valid clocks for mean (stations)"

! Sigma for output file
! ---------------------
OUTSIG 1  "INPUT_FILES"
  ## widget = combobox; editable = false; cards = INPUT_FILES COMBINATION

MSG_OUTSIG 1  "Compute sigma in resulting clock RINEX file from"

! Output ref.Clk from RINEX input file
! ------------------------------------
RADIO_F 1  "0"
  ## widget = radiobutton; group = REFSEL; radiokeys = RCLKREF

MSG_RADIO_F 1  "Retain the reference clock from an input file"

RCLKREF 0
  ## widget = selwin; path = DIR_RXC; ext = EXT_RXC; maxfiles = 1
  ## emptyallowed = true; pointer = RCLKINP
  # 

MSG_RCLKREF 1  "Retain the reference clock from an input file"

! Output of a new ref.Clk
! ------------------------
RADIO_N 1  "1"
  ## widget = radiobutton; group = REFSEL

MSG_RADIO_N 1  "Select a new reference clock for the output file"

REFCLOCK 1  "ALL_STATIONS"
  ## widget = combobox; editable = false
  ## cards = FIRST_STATION LAST_STATION ALL_STATIONS MANUAL FROM_FILE

MSG_REFCLOCK 1  "Selection of potential reference clocks"

! Output ref.Clk from file
! ------------------------
STAFIL 0
  ## widget = selwin; activeif = REFCLOCK = FROM_FILE; path = DIR_FIX
  ## ext = EXT_FIX; maxfiles = 1; emptyallowed = true
  # CLK$YD+0

MSG_STAFIL 1  "Get list from file for stations"

SATFIL 0
  ## widget = selwin; activeif = REFCLOCK = FROM_FILE; path = DIR_FIX
  ## ext = EXT_FIX; maxfiles = 1; emptyallowed = true
  # 

MSG_SATFIL 1  "Get list from file for satellites"

! Output ref.Clk from manual list
! -------------------------------
STAMAN 0
  ## widget = selwin; activeif = REFCLOCK = MANUAL; emptyallowed = true
  ## pointer = STALST

MSG_STAMAN 1  "Manual selection for stations"

SATMAN 0
  ## widget = selwin; activeif = REFCLOCK = MANUAL; emptyallowed = true
  ## pointer = SATLST
  # 

MSG_SATMAN 1  "Manual selection for satellites"

! Deg. of polynom for alignment
! -----------------------------
NALIGN 1  "1"
  ## widget = spinbox; range = 0 10 1

MSG_NALIGN 1  "Polynomial degree for alignment"

! Max. RMS for ref. clock alignment
! ---------------------------------
MAXREF 0
  ## widget = lineedit; check_type = integer; check_min = 0
  # 

MSG_MAXREF 1  "Maximum allowed RMS error for alignment"

! Select only one reference clock
! -------------------------------
ONEREF 1  "1"
  ## widget = checkbox

MSG_ONEREF 1  "Select only one reference clock"

! Jump detection, enable
! ----------------------
DOJUMP 1  "1"
  ## widget = checkbox

MSG_DOJUMP 1  "Enable clock jump detection"

! Jump detection, confidence interval
! -----------------------------------
NJMPSIG 1  "5"
  ## widget = spinbox; range = 1 10 1

MSG_NJMPSIG 1  "Confidence interval"

! Jump detection, min. RMS
! ------------------------
MJMPRMS 1  "1.0"
  ## widget = lineedit; check_type = real; check_min = 0.0

MSG_MJMPRMS 1  "Minimum RMS for jump detection"

! Jump detection, min. epochs between to jumps
! --------------------------------------------
NJMPEPO 1  "3"
  ## widget = spinbox; range = 0 10 1

MSG_NJMPEPO 1  "Maximum time interval for outlier detection"

! Jump detection, remove outliers
! -------------------------------
DJMPSTA 1  "0"
  ## widget = checkbox

MSG_DJMPSTA 1  "Remove outliers from output for station"

DJMPSAT 1  "0"
  ## widget = checkbox

MSG_DJMPSAT 1  "Remove outliers from output for satellite"

! Jump detection, enable/disable estimation
! -----------------------------------------
DJMPEST 1  "0"
  ## widget = checkbox

MSG_DJMPEST 1  "Enable the clock jump verification"

! Jump detection, min. epochs between to jumps
! --------------------------------------------
NJMPPOL 1  "10"
  ## widget = spinbox; range = 0 20 1; activeif = DJMPEST = 1

MSG_NJMPPOL 1  "Polynomial degree for jump size estimation"

! Clock Output: Interpolation
! ---------------------------
DOINTER 1  "0"
  ## widget = checkbox

MSG_DOINTER 1  "Allow interpolation"

! Clock Output: Extrapolation
! ---------------------------
DOEXTRA 1  "0"
  ## widget = checkbox

MSG_DOEXTRA 1  "Enable extrapolation"

! Clock Output: Polynom for Inter-/Extrapolation
! ----------------------------------------------
POLYNOM 1  "1"
  ## widget = spinbox; range = 0 99 1

MSG_POLYNOM 1  "Polynomial degree"

! Clock Output: SIN/COS-terms for Inter-/Extrapolation
! ----------------------------------------------------
PERIOD 1  "2"
  ## widget = spinbox; range = 0 99 1

MSG_PERIOD 1  "Periodical terms"

! Clock Output: More Parameter for Inter-/Extrapolation
! -----------------------------------------------------
MOREINEX 1  "0"
  ## widget = checkbox

MSG_MOREINEX 1  "Detailed setup for model function"

FUNCSTR 6
  "" "0.5" "1980 01 03" "00 00 00" "1980 01 06" "23 59 59"
  "" "1" "1980 01 04" "00 00 00" "1980 01 06" "23 59 59"
  "" "2" "1980 01 04" "00 00 00" "1980 01 06" "23 59 59"
  "0" "" "1980 01 03" "00 00 00" "1980 01 06" "23 59 59"
  "1" "" "1980 01 03" "00 00 00" "1980 01 06" "23 59 59"
  "2" "" "1980 01 04" "00 00 00" "1980 01 06" "23 59 59"
  ## widget = uniline; check_type.1 = integer; check_type.2 = real
  ## check_type.3 = date; check_type.4 = time; check_Type.5 = date
  ## check_type.6 = time; check_min.1 = 0; check_min.2 = 0; numlines = 10
  # "" "0.5" "$YMD_STR-3" "00 00 00" "$YMD_STR+0" "23 59 59"
  # "" "1" "$YMD_STR-2" "00 00 00" "$YMD_STR+0" "23 59 59"
  # "" "2" "$YMD_STR-2" "00 00 00" "$YMD_STR+0" "23 59 59"
  # "0" "" "$YMD_STR-3" "00 00 00" "$YMD_STR+0" "23 59 59"
  # "1" "" "$YMD_STR-3" "00 00 00" "$YMD_STR+0" "23 59 59"
  # "2" "" "$YMD_STR-2" "00 00 00" "$YMD_STR+0" "23 59 59"

MSG_FUNCSTR 1  "Additional parameter for clock function"

! Clock Output: Polynom for Inter-/Extrapolation
! ----------------------------------------------
POLYRMS 1  "5.0"
  ## widget = lineedit; check_type = real; check_min = 0.0

MSG_POLYRMS 1  "Maximum allowed RMS for fit"

! Clock Output: Polynom for Inter-/Extrapolation
! ----------------------------------------------
POLYOBS 1  "750"
  ## widget = spinbox; range = 0 5000 50

MSG_POLYOBS 1  "Minimum number of clock values for fit"

! Program output
! --------------
PRTINP 1  "0"
  ## widget = checkbox

MSG_PRTINP 1  "Detailed report on input clock RINEX files"

PRTCMB 1  "0"
  ## widget = checkbox

MSG_PRTCMB 1  "Detailed report on clock combination"

PRTREF 1  "0"
  ## widget = checkbox; activeif = RADIO_N = 1

MSG_PRTREF 1  "Detailed report on reference clock selection"

PRTJMP 1  "0"
  ## widget = checkbox; activeif = DOJUMP = 1

MSG_PRTJMP 1  "Detailed report on clock jump detection"

PRTEXT 1  "0"
  ## widget = checkbox; activeif = DOEXTRA = 1

MSG_PRTEXT 1  "Detailed report on clock extrapolation"

PRTRES 1  "1"
  ## widget = checkbox

MSG_PRTRES 1  "Statistic about the resulting clocks"

! Sort order in clock statistic
! -----------------------------
CLKSORT 1  "SIGMA"
  ## widget = combobox; editable = false; cards = NONE ALPHA SIGMA
  ## activeif = PRTRES = 1

MSG_CLKSORT 1  "Sort order for clock statistic"

! Header of the new RINEX file: RUNBY
! -----------------------------------
RUNBY 1  "AIUB"
  ## widget = lineedit; check_strlen = 20

MSG_RUNBY 1  "Run by"

! Header of the new RINEX file: AC
! --------------------------------
AC 1  "COD"
  ## widget = lineedit; check_strlen = 3

MSG_AC 1  "AC"

! Header of the new RINEX file: ACName
! ------------------------------------
ACNAME 1  "Center for Orbit Determination in Europe"
  ## widget = lineedit; check_strlen = 55

MSG_ACNAME 1  "AC name"

! Header of the new RINEX file: Comments
! --------------------------------------
COMMENT 2
  "CODE PRECISE CLOCK INFORMATION"
  "BASED ON CODE AND PHASE MEASUREMENTS"
  ## widget = uniline; check_strlen.1 = 60; numlines = 10

MSG_COMMENT 1  "Comment lines"

! MENUAUX selection lists
! -----------------------
STALST 0
  ## widget = selwin; menuaux = MENUAUX; action = SELREF_STA
  ## menuauxkeys = RCLKINP

SATLST 0
  ## widget = selwin; menuaux = MENUAUX; action = SELREF_SAT
  ## menuauxkeys = RCLKINP


# BEGIN_PANEL NO_CONDITION #####################################################
# COMBINE/MANIPULATE CLOCK RINEX FILES - CCRNXC 1: Filenames                   #
#                                                                              #
# GENERAL FILES                                                                #
#   Show all general files          > % <                                      # SHOWGEN
#                                                                              #
# INPUT FILES                                                                  #
#   Clock RINEX files               > %%%%%%%% <                               # RCLKINP
#   Replace station coordinates     > %%%%%%%% <                               # COORD
#                                                                              #
# RESULT FILES                                                                 #
#   Combined clock RINEX file       > %%%%%%%% <                               # RCLKOUT
#   Bernese satellite clock file    > %%%%%%%% <                               # SCLKOUT
#   Sigma file with linear fit RMS  > %%%%%%%% <                               # LINFITRS
#                                                                              #
# GENERAL OUTPUT FILES                                                         #
#   Program output      > % <  use CCRNXC.Lnn            or    > %%%%%%%% <    # SYSODEF SYSOUT
#   Error messages      > % <  merged to program output  or    > %%%%%%%% <    # ERRMRG SYSERR
#                                                                              #
# END_PANEL ####################################################################


# BEGIN_PANEL SHOWGEN = 1 ######################################################
# CCRNXC 1.1: General Files                                                    #
#                                                                              #
# MENU SETTINGS                                                                #
#   Selected campaign       > %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% <   # CAMPAIGN
#   Selected session          year> %%%% <  session> %%%% <                    # YR4_INFO SES_INFO
#   Session table           > %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% <   # SESSION_TABLE
#                                                                              #
# END_PANEL ####################################################################


# BEGIN_PANEL NO_CONDITION #####################################################
# CCRNXC 2: Clock/Epoch Selection for Processing                               #
#                                                                              #
# TITLE > %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% <   # TITLE
#                                                                              #
# DEFINE EPOCHS TO BE PROCESSED                                                #
#   Use time window                > % <                                       # USEWIN
#   Sampling rate for clocks       > %%%%% <seconds                            # DELTAT
#                                                                              #
# DEFINE A LIST OF CLOCKS TO BE PROCESSED                                      #
#   Selection of station clocks    > %%%%%%%%%%% <                             # SELSTA
#   List from file                 > %%%%%%%% <                                # SFILSTA
#   Manual selection               > %%%%%%%%%%%%%%%%%%%% <                    # SLSTSTA
#                                                                              #
#   Selection of satellite clocks  > %%%%%%%%%%% <                             # SELSAT
#   List from file                 > %%%%%%%% <                                # SFILSAT
#   Manual selection               > %%%%%%%% <                                # SLSTSAT
#   Remove clocks with eclipse flag> %%%%%%%%%%%%%%%%%%%% <                    # DELECLIPS
#                                                                              #
# END_PANEL ####################################################################


# BEGIN_PANEL USEWIN = 1 #######################################################
# CCRNXC 2.1: Time Window                                                      #
#                                                                              #
# TIME INTERVAL TO BE PROCESSED                                                #
#   > % < Defined by Year and Session identifier                               # RADIO_1
#         Year > %%%% <     Session > %%%% <                                   # SESSION_YEAR SESSION_STRG
#                                                                              #
#   > % < Defined by Start and End times                                       # RADIO_2
#                 yyyy mm dd     hh mm ss           yyyy mm dd     hh mm ss    #
#         Start > %%%%%%%%%% < > %%%%%%%% <   End > %%%%%%%%%% < > %%%%%%%% <  # STADAT STATIM ENDDAT ENDTIM
#                                                                              #
# END_PANEL ####################################################################


# BEGIN_PANEL NO_CONDITION #####################################################
# CCRNXC 3: Options for Clock RINEX File Combination                           #
#                                                                              #
# CLOCK RINEX FILE OFFSET ESTIMATION                                           #
#   Use all station clocks                             > % <                   # USESTA
#   Use all satellite clocks                           > % <                   # USESAT
#   Use only reference clocks                          > % <                   # ONLYREF
#   A priori sigma of unit weight                      > %%%%%% <nanoseconds   # SIGMA0
#   Maximum residuum allowed                           > %%%%%% <nanoseconds   # MAXRESI
#                                                                              #
# OPTIONS FOR CLOCK COMBINATION                                                #
#   Strategy for computation of mean value             > %%%%%%%%%%%% <        # COMBI
#   Maximum deviation from mean                        > %%%%%% <nanoseconds   # MAXMEAN
#   Minimum number of valid clocks for mean            > %%%% <  for stations  # MINSTA
#                                                      > %%%% <  for satellites# MINSAT
#   Compute sigma in resulting clock RINEX file from   > %%%%%%%%%%%% <        # OUTSIG
#                                                                              #
# END_PANEL ####################################################################


# BEGIN_PANEL NO_CONDITION #####################################################
# CCRNXC 4: Select Program Functions, Program Output                           #
#                                                                              #
# REFERENCE CLOCK SELECTION                                                    #
# > % <Select a new reference clock for the output file                        # RADIO_N
# > % <Retain the reference clock from an input file   > %%%%%%%% <            # RADIO_F RCLKREF
#                                                                              #
# ENABLE OTHER PROGRAM FUNCTIONS                                               #
#   Enable clock jump detection                        > % <                   # DOJUMP
#   Enable extrapolation                               > % <                   # DOEXTRA
#                                                                              #
# PROGRAM OUTPUT OPTIONS                                                       #
#   Detailed report on input clock RINEX files         > % <                   # PRTINP
#   Detailed report on clock combination               > % <                   # PRTCMB
#   Detailed report on reference clock selection       > % <                   # PRTREF
#   Detailed report on clock jump detection            > % <                   # PRTJMP
#   Detailed report on clock extrapolation             > % <                   # PRTEXT
#   Statistic about the resulting clocks               > % <                   # PRTRES
#     Sort order for clock statistics                  > %%%%%%%%%% <          # CLKSORT
#                                                                              #
# END_PANEL ####################################################################


# BEGIN_PANEL RADIO_F == 0 #####################################################
# CCRNXC 5: Select a New Reference Clock for Output File                       #
#                                                                              #
# REFERENCE CLOCK SELECTION                                                    #
#   Selection of potential reference clocks        > %%%%%%%%%%%%%% <          # REFCLOCK
#   Manual selection for     stations              > %%%%%%%%%%%%%%%%%%%% <    # STAMAN
#                            satellites            > %%%%%%%%%%%%%%%%%%%% <    # SATMAN
#   Get list from file for   stations              > %%%%%%%% <                # STAFIL
#                            satellites            > %%%%%%%% <                # SATFIL
#                                                                              #
# ALIGNMENT OF NEW REFERENCE CLOCK                                             #
#   Polynomial degree for alignment                > %% <                      # NALIGN
#   Maximum allowed RMS error for alignment        > %%%% <nanoseconds         # MAXREF
#                                                                              #
# END_PANEL ####################################################################


# BEGIN_PANEL DOJUMP = 1 #######################################################
# CCRNXC 6: Options for Clock Jump Detection                                   #
#                                                                              #
# CLOCK JUMP DETECTION                                                         #
#   Confidence interval                        > %%%% <  sigmas                # NJMPSIG
#   Minimum RMS for jump detection             > %%%%%% <nanosecond/300 seconds# MJMPRMS
#                                                                              #
# CLOCK JUMP OR OUTLIER                                                        #
#   Maximum time interval for outlier detection> %%%% <  epochs                # NJMPEPO
#   Remove outliers from output for stations   > % <                           # DJMPSTA
#   Remove outliers from output for satellites > % <                           # DJMPSAT
#                                                                              #
# CLOCK JUMP VERIFICATION                                                      #
#   Enable the clock jump verification         > % <                           # DJMPEST
#   Polynomial degree for jump size estimation > %%%% <                        # NJMPPOL
#                                                                              #
# END_PANEL ####################################################################


# BEGIN_PANEL DOEXTRA = 1 ######################################################
# CCRNXC 7: Options for Clock Extrapolation                                    #
#                                                                              #
# DEFINE EXTRAPOLATION MODEL                                                   #
#   Polynomial degree                             > %% <                       # POLYNOM
#   Periodical terms                              > %% <                       # PERIOD
#   Detailed setup for model function             > % <     (advanced)         # MOREINEX
#                                                                              #
# APPLY EXTRAPOLATION MODEL                                                    #
#   Maximum allowed RMS for fit                   > %%%%%% <nanoseconds        # POLYRMS
#   Minimum number of clock values for fit        > %%%% <                     # POLYOBS
#                                                                              #
# END_PANEL ####################################################################


# BEGIN_PANEL DOEXTRA = 1 AND MOREINEX = 1 #####################################
# CCRNXC 7.1: Extrapolation, Advanced Setup                                    #
#                                                                              #
# DETAILED SETUP FOR MODEL FUNCTION                                            #
#  Polynomial  Periodical            from                    to                #
# > __degree__ ___terms___ __yyyy_mm_dd _hh_mm_ss __yyyy_mm_dd _hh_mm_ss  <    # FUNCSTR
#                                                                              #
# END_PANEL ####################################################################


# BEGIN_PANEL RCLKOUT / _ ######################################################
# CCRNXC 8: Clock RINEX File Output                                            #
#                                                                              #
# HEADER ENTRIES FOR THE OUTPUT CLOCK RINEX FILE                               #
#   Run by   > %%%%%%%%%%%%%%%%%%% <                                           # RUNBY
#   AC       > %%% <                                                           # AC
#   AC name  > %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% <        # ACNAME
#                                                                              #
#   > Comment_lines_______________________________________________   <         # COMMENT
#                                                                              #
# END_PANEL ####################################################################
