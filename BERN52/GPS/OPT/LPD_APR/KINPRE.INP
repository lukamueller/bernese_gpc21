
! Environment Variables
! ---------------------
ENVIRONMENT 1  "" "${}"
  ## widget = initmenu; pointer = ENVIR_LIST

! SOME KEYWORDS FOR AUTOMATIC OUTPUT
! ----------------------------------
CAMPAIGN 1  "./DUMMY"
  ## widget = initmenu; pointer = ACTIVE_CAMPAIGN

USR_INFO 1  "${USER}"
  ## widget = comment

SESSION_TABLE 1  "${X}/SKL/SESSIONS.SES"
  ## widget = initmenu; pointer = SESSION_TABLE

DESCR_SESSION_TABLE 1  "Session table"

YR4_INFO 1  "1980"
  ## widget = comment
  # $Y+0

SES_INFO 1  "0060"
  ## widget = comment
  # $S+0

! -------------------------------------------------------------------------
! Show general files
! ------------------
SHOWGEN 1  "1"
  ## widget = checkbox

MSG_SHOWGEN 1  "Show all general files"

! Satellite Information File
! --------------------------
SATELL 1  "${X}/GEN/$(SATINF).$(PCV)"
  ## widget = selwin; path = PTH_GEN; ext = NO_EXT; maxfiles = 1
  # $(SATINF).$(PCV)

DESCR_SATELL 1  "Satellite information"

! Check Kinematic Orbit
! ---------------------
ICHKKIN 1  "0"
  ## widget = checkbox

MSG_ICHKKIN 1  "Check kinematic positions"

! Write Accuracy Info
! -------------------
IWRQXX 1  "0"
  ## widget = checkbox

MSG_IWRQXX 1  "Write accuracy info"

! Standard Orbit
! --------------
LEOSTD 0
  ## widget = selwin; path = DIR_STD; ext = EXT_STD; maxfiles = 1
  ## activeif = ICHKKIN = 1
  # 

DESCR_LEOSTD 1  "Standard orbits"

! Edited KIN file
! ---------------
KINOUT 0
  ## widget = lineedit; path = DIR_PRE; ext = EXT_KIN; emptyallowed = true
  ## activeif = ICHKKIN = 1
  # 

DESCR_KINOUT 1  "Write edited KIN file"

! General Constants
! -----------------
CONST 1  "${X}/GEN/CONST."
  ## widget = selwin; path = PTH_GEN; maxfiles = 1
  # CONST.

DESCR_CONST 1  "General constants"

! Pole File
! ---------
POLE 0
  ## widget = selwin; path = DIR_ERP; ext = EXT_ERP; maxfiles = 1
  ## activeif = ICHKKIN = 1
  # 

DESCR_POLE 1  "Pole file"

! Max difference between kinematic and standard orbit
! ---------------------------------------------------
EDTKIN 1  "0.13"
  ## widget = lineedit; check_type = real

MSG_EDTKIN 1  "Max. 3D difference"

! Subdaily Pole Model
! -------------------
SUBMOD 0
  ## widget = selwin; path = PTH_GEN; ext = EXT_SUB; maxfiles = 1
  ## activeif = ICHKKIN = 1
  # IERS2010XY

DESCR_SUBMOD 1  "Subdaily pole model"

! Nutation Model
! -------------------
NUTMOD 0
  ## widget = selwin; path = PTH_GEN; ext = EXT_NUT; maxfiles = 1
  ## activeif = ICHKKIN = 1
  # IAU2000R06

DESCR_NUTMOD 1  "Nutation model"

! Input Files
! -----------
KININP 1  "./DUMMY/STA/$A800060.KIN"
  ## widget = selwin; path = DIR_KIN; ext = EXT_KIN; maxfiles = 999
  ## emptyallowed = false
  # $A$YSS+0

MSG_KININP 1  "Kinematic file"

KININP_TXT_COL_1 1  "Kinematic files"

! Extensions of the Output Files
! ------------------------------
KININP_EXT_COL_2 1  "PRE"
  ## widget = initmenu; pointer = EXT_PRE

KININP_PTH_COL_2 1  "./DUMMY/ORB/"
  ## widget = initmenu; pointer = DIR_PRE

KININP_TXT_COL_2 1  "Precise orbit files"

! List of Output Files
! -------------------
PREOUT 0
  ## widget = lineedit; path = DIR_PRE; ext = EXT_PRE; emptyallowed = true
  # 

DESCR_PREOUT 1  "Output file precise orbits"

! Program Output File
! -------------------
SYSODEF 1  "0"
  ## widget = checkbox

MSG_SYSODEF 1  "Program output"

SYSOUT 1  "./DUMMY/OUT/KP$A800060.OUT"
  ## widget = lineedit; path = DIR_OUT; ext = EXT_OUT
  ## activeif = SYSODEF == 0; emptyallowed = false
  # KP$A$YSS+0

DESCR_SYSOUT 1  "Program output"

! Error Message File
! ------------------
ERRMRG 1  "0"
  ## widget = checkbox

MSG_ERRMRG 1  "Error message"

SYSERR 1  "${U}/WORK/ERROR.MSG"
  ## widget = lineedit; path = PTH_ERR; ext = EXT_ERR
  ## activeif = ERRMRG == 0; emptyallowed = false
  # ERROR

DESCR_SYSERR 1  "Error message"

! Satellite Clocks
! ----------------
RNXCLK 1  "./DUMMY/OUT/$A800060.CLK"
  ## widget = selwin; path = DIR_RXC; ext = EXT_RXC; maxfiles = 1
  ## emptyallowed = true
  # $A$YSS+0

DESCR_RNXCLK 1  "Satellite clocks"

! Title
! -----
TITLE 1  "LPD_800060_$(LEO): Conversion of kinematic to precise orbit format"
  ## widget = lineedit
  # LPD_$YSS+0_$(LEO): Conversion of kinematic to precise orbit format

MSG_TITLE 1  "Title line"

! Options
! -------
! Data description
! ----------------
DATADES 1  "CODE"
  ## widget = lineedit

MSG_DATADES 1  "Data description"

! Orbit Type
! ----------
ORBTYP 1  "KIN"
  ## widget = lineedit

MSG_ORBTYP 1  "Orbit type"

! Agency Generating Orbit
! -----------------------
AGENCY 0
  ## widget = lineedit
  # 

MSG_AGENCY 1  "Agency gen. orbit"

! Specific Accuracy
! -----------------
ACCURA 1  "0"
  ## widget = lineedit

MSG_ACCURA 1  "LEO spec. accuracy"

! Title Lines for Precise File
! ----------------------------
TIPRE1 0
  ## widget = lineedit
  # 

MSG_TIPRE1 1  "Title line 1"

TIPRE2 0
  ## widget = lineedit
  # 

MSG_TIPRE2 1  "Title line 2"

TIPRE3 0
  ## widget = lineedit
  # 

MSG_TIPRE3 1  "Title line 3"

TIPRE4 0
  ## widget = lineedit
  # 

MSG_TIPRE4 1  "Title line 4"

! Observation Window
! ------------------
RADIO_0 1  "1"
  ## widget = radiobutton; group = OBSWIN

MSG_RADIO_0 1  "Convert all KIN records, do not use any obs. window"

RADIO_1 1  "0"
  ## widget = radiobutton; group = OBSWIN
  ## radiokeys = SESSION_YEAR SESSION_STRG

MSG_RADIO_1 1  "Defined by year and session identifier"

RADIO_2 1  "0"
  ## widget = radiobutton; group = OBSWIN

MSG_RADIO_2 1  "Defined by start and end times"

SESSION_YEAR 1  "1980"
  ## widget = lineedit; multilineallowed = true; emptyallowed = false
  # $Y+0

MSG_SESSION_YEAR 1  "Year for sessions def. the time window"

SESSION_STRG 1  "0060"
  ## widget = lineedit; multilineallowed = true; emptyallowed = false
  # $S+0

MSG_SESSION_STRG 1  "Sessions defining the time window"

! Use Obs. Window
! ---------------
STADAT 1  "1980 01 06"
  ## widget = lineedit; activeif = RADIO_2 = 1; check_type = date
  # $YMD_STR+0

MSG_STADAT 1  "Start of time window (year month day)"

STATIM 1  "00 00 00"
  ## widget = lineedit; emptyallowed = false
  ## activeif = RADIO_2 = 1 AND STADAT / _; check_type = time

MSG_STATIM 1  "Start of time window (hour min. sec.)"

ENDDAT 1  "1980 01 06"
  ## widget = lineedit; activeif = RADIO_2 = 1; check_type = date
  # $YMD_STR+0

MSG_ENDDAT 1  "End of time window (year month day)"

ENDTIM 1  "23 59 59"
  ## widget = lineedit; emptyallowed = false
  ## activeif = RADIO_2 = 1 AND ENDDAT / _; check_type = time

MSG_ENDTIM 1  "End of time window (hour min. sec.)"


# BEGIN_PANEL NO_CONDITION #####################################################
# CONVERT KINEMATIC POSITIONS TO PRECISE ORBITS - KINPRE 1: Input/Output files #
#                                                                              #
# GENERAL FILES                                                                #
#   Show all general files     > % <                                           # SHOWGEN
#                                                                              #
# INPUT FILES                                                                  #
#   Kinematic file             > %%%%%%%% <                                    # KININP
#   Satellite clocks (RINEX)   > %%%%%%%% <                                    # RNXCLK
#                                                                              #
# RESULT FILES                                                                 #
#   Output file precise orbits > %%%%%%%% <      (blank: same as input file(s))# PREOUT
#   Check kinematic positions  > % <             (comparing to dynamic orbit)  # ICHKKIN
#                                                                              #
# GENERAL OUTPUT FILES                                                         #
#   Program output             > % < use KINPRE.Lnn            or > %%%%%%%% < # SYSODEF SYSOUT
#   Error messages             > % < merged to program output  or > %%%%%%%% < # ERRMRG SYSERR
#                                                                              #
# END_PANEL ####################################################################


# BEGIN_PANEL SHOWGEN = 1 ######################################################
# KINPRE 1.1: General files                                                    #
#                                                                              #
# GENERAL INPUT FILES                                                          #
#   Satellite information   > %%%%%%%%%%%% <                                   # SATELL
#   General constants       > %%%%%%%%%%%% <                                   # CONST
#   Pole file               > %%%%%%%% <                                       # POLE
#   Subdaily pole model     > %%%%%%%% <                                       # SUBMOD
#   Nutation model          > %%%%%%%% <                                       # NUTMOD
#                                                                              #
# MENU SETTINGS                                                                #
#   Selected campaign       > %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% <   # CAMPAIGN
#   Selected session          year> %%%% <  session> %%%% <                    # YR4_INFO SES_INFO
#   Session table           > %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% <   # SESSION_TABLE
#                                                                              #
# END_PANEL ####################################################################


# BEGIN_PANEL ICHKKIN = 1  #####################################################
# KINPRE 1.2: Check kinematic positions with dynamic orbit                     #
#                                                                              #
#   Max. 3D difference      > %%%%% <         (w.r.t. standard orbit)          # EDTKIN
#   Standard orbits         > %%%%%%%%%%%% <                                   # LEOSTD
#                                                                              #
#   Write edited KIN file   > %%%%%%%%%%%% <                                   # KINOUT
#                                                                              #
# END_PANEL ####################################################################


# BEGIN_PANEL NO_CONDITION #####################################################
# KINPRE 2: Options                                                            #
#                                                                              #
# TITLE > %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% <         # TITLE
#                                                                              #
# SPECIAL INFORMATION FOR PRECISE ORBIT FILE                                   #
#   Data description    > %%%%% <                                              # DATADES
#   Orbit type          > %%% <                                                # ORBTYP
#   Agency gen. orbit   > %%%% <                                               # AGENCY
#   LEO spec. accuracy  > %%%% <                                               # ACCURA
#   Title lines                    (first line from KIN file)                  #
#   > %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% <              # TIPRE1
#   > %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% <              # TIPRE2
#   > %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% <              # TIPRE3
#   > %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% <              # TIPRE4
#                                                                              #
#   Write accuracy info > % <                                                  # IWRQXX
#                                                                              #
# END_PANEL ####################################################################


# BEGIN_PANEL NO_CONDITION #####################################################
#  KINPRE 3: Observation window                                                #
#                                                                              #
#  USE OBSERVATION WINDOW:                                                     #
#                                                                              #
#  > % <  Take all kinematic positions                                         # RADIO_0
#                                                                              #
#  > % <  Defined by Year and Session identifier                               # RADIO_1
#         Year> %%%% <       Session> %%%% <                                   # SESSION_YEAR SESSION_STRG
#                                                                              #
#  > % <  Defined by Start and End times                                       # RADIO_2
#                 yyyy mm dd     hh mm ss           yyyy mm dd     hh mm ss    #
#         Start > %%%%%%%%%% < > %%%%%%%% <   End > %%%%%%%%%% < > %%%%%%%% <  # STADAT STATIM ENDDAT ENDTIM
#                                                                              #
# END_PANEL ####################################################################
