
! Environment Variables
! ---------------------
ENVIRONMENT 1  "" "${}"
  ## widget = initmenu; pointer = ENVIR_LIST

! Some title information
! ----------------------
USR_INFO 1  "${USER}"
  ## widget = comment

SESSION_TABLE 1  "${X}/SKL/SESSIONS.SES"
  ## widget = initmenu; pointer = SESSION_TABLE

DESCR_SESSION_TABLE 1  "Session table"

YR4_INFO 1  "1980"
  ## widget = comment
  # $Y+0

SES_INFO 1  "0060"
  ## widget = comment
  # $S+0

! Active Campaign
! ---------------
CAMPAIGN 1  "./DUMMY"
  ## widget = initmenu; pointer = ACTIVE_CAMPAIGN

! General Constants
! -----------------
! Show general files
! ------------------
SHOWGEN 1  "1"
  ## widget = checkbox

MSG_SHOWGEN 1  "Show all general files"

CONST 1  "${X}/GEN/CONST."
  ## widget = selwin; path = PTH_GEN; ext = NO_EXT; maxfiles = 1
  # CONST.

DESCR_CONST 1  "General constants"

! Difference of GPS-TIME - UTC-TIME
! ---------------------------------
GPSUTC 1  "${X}/GEN/GPSUTC."
  ## widget = selwin; path = PTH_GEN; ext = NO_EXT; maxfiles = 1
  # GPSUTC.

DESCR_GPSUTC 1  "Difference GPS-UTC"

! Satellite Information
! ---------------------
SATELL 1  "${X}/GEN/$(SATINF).$(PCV)"
  ## widget = selwin; path = PTH_GEN; ext = NO_EXT; maxfiles = 1
  # $(SATINF).$(PCV)

DESCR_SATELL 1  "Satellite information"

! Station Name Abbreviations
! --------------------------
ABBREV 1  "./DUMMY/STA/$(CRDINF).ABB"
  ## widget = selwin; path = DIR_ABB; ext = EXT_ABB; maxfiles = 1
  # $(CRDINF)

DESCR_ABBREV 1  "Abbreviation table"

! STAINFO with information for the Bernese files
! ----------------------------------------------
STAINFO 1  "./DUMMY/STA/$(CRDINF).STA"
  ## widget = selwin; path = DIR_STA; ext = EXT_STA; maxfiles = 1
  # $(CRDINF)

DESCR_STAINFO 1  "Station information"

! List of CSTG Input Files
! -------------------------
QLFILE 1  "./DUMMY/ORX/$(NP).800106"
  ## widget = selwin; path = DIR_OXO; ext = NO_EXT; maxfiles = 999
  # $(NP).$Y$M$D

MSG_QLFILE 1  "SLR normal point files"

! Program Output File
! -------------------
SYSODEF 1  "0"
  ## widget = checkbox

MSG_SYSODEF 1  "Program output"

SYSOUT 1  "./DUMMY/OUT/QRX80006.OUT"
  ## widget = lineedit; path = DIR_OUT; ext = EXT_OUT
  ## activeif = SYSODEF == 0; emptyallowed = false
  # QRX$YD+0

DESCR_SYSOUT 1  "Program output"

! Error Message File
! ------------------
ERRMRG 1  "0"
  ## widget = checkbox

MSG_ERRMRG 1  "Error message"

SYSERR 1  "${U}/WORK/ERROR.MSG"
  ## widget = lineedit; path = PTH_ERR; ext = EXT_ERR
  ## activeif = ERRMRG == 0; emptyallowed = false
  # ERROR

DESCR_SYSERR 1  "Error message"

! Scratch File
! ------------
AUXFIL 1  "${U}/WORK/QLRINEXO.SCR"
  ## widget = lineedit; path = PTH_SCR; ext = EXT_SCR; emptyallowed = false
  # QLRINEXO$J

DESCR_AUXFIL 1  "Scratch file"

DELETE_FILES 1  "AUXFIL"

! Title
! -----
TITLE 1  "SLRVAL_800060: Conversion of SLR quicklook data into RINEX"
  ## widget = lineedit
  # SLRVAL_$YSS+0: Conversion of SLR quicklook data into RINEX

MSG_TITLE 1  "Title line"

! Session ID
! ----------
SESSID 1  "0060"
  ## widget = lineedit; check_strlen = 4
  # $S+0

MSG_SESSID 1  "Session ID used for RINEX observation file names"

! Year ID
! -------
YEARID 1  "80"
  ## widget = lineedit; check_strlen = 2
  # $Y

MSG_YEARID 1  "Year ID used for RINEX observation file names"

! Observation Window
! ------------------
RADIO_0 1  "0"
  ## widget = radiobutton; group = OBSWIN

MSG_RADIO_0 1  "Take all observations"

RADIO_1 1  "1"
  ## widget = radiobutton; group = OBSWIN
  ## radiokeys = SESSION_YEAR SESSION_STRG WINDOW

MSG_RADIO_1 1  "Observation window defined by year and session identifier"

RADIO_2 1  "0"
  ## widget = radiobutton; group = OBSWIN
  ## radiokeys = STADAT STATIM ENDDAT ENDTIM

MSG_RADIO_2 1  "Observation window defined by start and end time"

SESSION_YEAR 1  "1980"
  ## widget = lineedit; multilineallowed = true; emptyallowed = false
  # $Y+0

MSG_SESSION_YEAR 1  "Year for sessions def. the time window"

SESSION_STRG 1  "0060"
  ## widget = lineedit; multilineallowed = true; emptyallowed = false
  # $S+0

MSG_SESSION_STRG 1  "Sessions defining the time window"

! List of satellites
! ------------------
SATLST 0
  ## widget = lineedit
  # 

MSG_SATLST 1  "LIST OF SATELLITES"

! Use Obs. Window
! ---------------
STADAT 1  "1980 01 06"
  ## widget = lineedit; emptyallowed = false; check_type = date
  # $YMD_STR+0

MSG_STADAT 1  "Start of time window (year month day)"

STATIM 1  "00 00 00"
  ## widget = lineedit; emptyallowed = false; check_type = time

MSG_STATIM 1  "Start of time window (hour min. sec.)"

ENDDAT 1  "1980 01 06"
  ## widget = lineedit; emptyallowed = false; check_type = date
  # $YMD_STR+0

MSG_ENDDAT 1  "End of time window (year month day)"

ENDTIM 1  "23 59 59"
  ## widget = lineedit; emptyallowed = false; check_type = time

MSG_ENDTIM 1  "End of time window (hour min. sec.)"

! Path of the Output Files
! ------------------------
RXOFILE_PTH_COL_1 1  "./DUMMY/RAW/"
  ## widget = initmenu; pointer = DIR_RXO


# BEGIN_PANEL NO_CONDITION #####################################################
# CONVERT SLR NORMAL POINT FILES (CSTG) TO RINEX - QLRINEXO 1: Filenames       #
#                                                                              #
# GENERAL FILES                                                                #
#   Show all general files   > % <                                             # SHOWGEN
#                                                                              #
# INPUT FILES                                                                  #
#   SLR normal point files   > %%%%%%%%%%%%%%%% <                              # QLFILE
#                                                                              #
# GENERAL OUTPUT FILES                                                         #
#   Program output           > % < use QLRINEXO.Lnn          or > %%%%%%%% <   # SYSODEF SYSOUT
#   Error messages           > % < merged to program output  or > %%%%%%%% <   # ERRMRG SYSERR
#                                                                              #
# END_PANEL ####################################################################


# BEGIN_PANEL SHOWGEN = 1 ######################################################
# QLRINEXO 1.1: General Files                                                  #
#                                                                              #
# GENERAL INPUT FILES                                                          #
#   General constants       > %%%%%%%%%%%% <                                   # CONST
#   Satellite information   > %%%%%%%%%%%% <                                   # SATELL
#   GPS-UTC seconds         > %%%%%%%%%%%% <                                   # GPSUTC
#   Abbreviation table      > %%%%%%%%%%%% <                                   # ABBREV
#   Station information     > %%%%%%%%%%%% <                                   # STAINFO
#                                                                              #
# MENU SETTINGS                                                                #
#   Selected campaign       > %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% <   # CAMPAIGN
#   Selected session          year> %%%% <  session> %%%% <                    # YR4_INFO SES_INFO
#   Session table           > %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% <   # SESSION_TABLE
#                                                                              #
# TEMPORARY FILES                                                              #
#   Scratch file            > %%%%%%%%%% <                                     # AUXFIL
#                                                                              #
# END_PANEL ####################################################################


# BEGIN_PANEL NO_CONDITION #####################################################
# QLRINEXO 2: Input Options                                                    #
#                                                                              #
# TITLE   > %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% <  # TITLE
#                                                                              #
# CREATION OF RINEX OBSERVATION FILE NAMES                                     #
#   Session ID used for RINEX observation file names  > %%%% <                 # SESSID
#   Year@@@ ID used for RINEX observation file names  > %% <                   # YEARID
#                                                                              #
# LIST OF SATELLITES                                                           #
#   > %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% <       # SATLST
#    (Satellite number according to satellite information file:                #
#                        e.g.   5 ... GPS 35                                   # 
#                               6 ... GPS 36                                   #
#                             103 ... GLONASS 87                               #
#                             105 ... GLONASS 88                               #
#                             901 ... TOPEX/Poseidon                           #
#                             906 ... CHAMP                                    #
#                             908 ... Jason                                    #
#                             909 ... Grace A                                  #
#                             910 ... Grace B                                  #
#                             951 ... LAGEOS-1                                 #
#                             952 ... LAGEOS-2                                 #
#                                                                              #
# END_PANEL ####################################################################


# BEGIN_PANEL NO_CONDITION #####################################################
# QLRINEXO 3: Observation Window                                               #
#                                                                              #
# OBSERVATION WINDOW                                                           #
#                                                                              #
#   > % < Take all observations                                                # RADIO_0
#                                                                              #
#   > % < Defined by year and session identifier                               # RADIO_1
#         Year  > %%%% <   Session > %%%% <                                    # SESSION_YEAR SESSION_STRG
#                                                                              #
#   > % < Defined by start and end time                                        # RADIO_2
#                 yyyy mm dd     hh mm ss           yyyy mm dd     hh mm ss    #
#         Start > %%%%%%%%%% < > %%%%%%%% <   End > %%%%%%%%%% < > %%%%%%%% <  # STADAT STATIM ENDDAT ENDTIM
#                                                                              #
# END_PANEL ####################################################################
