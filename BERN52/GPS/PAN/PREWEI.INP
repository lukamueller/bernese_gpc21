
! Some keywords for automatic output
! ----------------------------------
CAMPAIGN     0
  ## widget = initmenu; pointer = ACTIVE_CAMPAIGN

SESSION_TABLE     1  "SESSIONS"
  ## widget = initmenu; pointer = SESSION_TABLE

DESCR_SESSION_TABLE 1  "Session table"

YR4_INFO     1  "$Y+0"
  ## widget = comment

SES_INFO     1  "$S+0"
  ## widget = comment


USR_INFO     1  "${USER}"
  ## widget = comment

! Environment Variables
! ---------------------
ENVIRONMENT     1  "" ""
  ## widget = initmenu; pointer = ENVIR_LIST

! ---------------------------------------------------------------------
! INPUTFILES                                                         |
! ---------------------------------------------------------------------
! Show general files
! ------------------
SHOWGEN     1  "1"
  ## widget = checkbox

MSG_SHOWGEN 1  "Show all general files"

! General Constants
! -----------------
CONST     1  "CONST."
  ## widget = selwin; path = PTH_GEN; ext = NO_EXT; maxfiles = 1

DESCR_CONST 1  "General constants"

! ORBGEN List File
! ----------------
LIST     0
  ## widget = selwin; path = DIR_LST; ext = EXT_LST; maxfiles = 1

DESCR_LIST 1  "List file from ORBGEN"

! List of Input Files
! -------------------
PREFIL     0
  ## widget = selwin; path = DIR_PRE; ext = EXT_PRE; maxfiles = 999

MSG_PREFIL 1  "Precise orbit files"

PREFIL_TXT_COL_1 1  "Input files"

PREFIL_TXT_COL_2 1  "Output files"

! ---------------------------------------------------------------------
! OUTPUTFILES                                                        |
! ---------------------------------------------------------------------
! List of Output Files
! --------------------
PREOUT     0
  ## widget = lineedit; path = DIR_PRE; ext = EXT_PRE

MSG_PREOUT 1  "Precise orbit files"

! Extensions of the Output Files
! ------------------------------
PREFIL_PTH_COL_2     0
  ## widget = initmenu; pointer = DIR_PRE

PREFIL_EXT_COL_2     0
  ## widget = initmenu; pointer = EXT_PRE

! Program Output File
! -------------------
SYSODEF     1  "1"
  ## widget = checkbox

MSG_SYSODEF 1  "Program output"

SYSOUT     1  "PREWEI"
  ## widget = lineedit; path = DIR_OUT; ext = EXT_OUT
  ## activeif = SYSODEF == 0; emptyallowed = false

DESCR_SYSOUT 1  "Program output"

! Error Message File
! ------------------
ERRMRG     1  "0"
  ## widget = checkbox

MSG_ERRMRG 1  "Error message"

SYSERR     1  "ERROR"
  ## widget = lineedit; path = PTH_ERR; ext = EXT_ERR
  ## activeif = ERRMRG == 0; emptyallowed = false

DESCR_SYSERR 1  "Error message"

! Scratch Files
! -------------
SCRATCH     1  "PREWEI$J"
  ## widget = lineedit; path = PTH_SCR; ext = EXT_SCR
  ## emptyallowed = false

DESCR_SCRATCH 1  "Scratch file"

DELETE_FILES     1  "SCRATCH"

! --------------------------------------------------------------------
! OPTIONS                                                            |
! --------------------------------------------------------------------
! Title
! -----
TITLE     0
  ## widget = lineedit

MSG_TITLE 1  "Title line"


! Radiobuttons
! ------------
RADIO1     1  "0"
  ## widget = radiobutton; group = DUMMY

MSG_RADIO1 1  "Take accuracy codes from table (next panel)"


RADIO2     1  "1"
  ## widget = radiobutton; group = DUMMY

MSG_RADIO2 1  "Compute accuracy codes from RMS values"

! Minimum accuracy
! ----------------
MINACC     1  "5"
  ## widget = spinbox; range = 0 20 1; check_type = integer; check_min = 0.0

MSG_MINACC 1  "Minimum accuracy code P0"

! Maximum accuracy
! ----------------
MAXACC     1  "99"
  ## widget = spinbox; range = 0 99 1; check_type = integer; check_min = 0.0

MSG_MAXACC 1  "Satellites with accuracy code greater than"

! Consider codes from file
! ------------------------
FROMFILE     1  "0"
  ## widget = checkbox

MSG_FROMFILE 1  "Use codes from file"

! Flag or delete satellites
! -------------------------
FLGDEL     1  "RERATED"
  ## widget = combobox; cards = REMOVED FLAGGED RERATED

MSG_FLGDEL 1  "Satellites with accuracy code greater than"

! Limits for the accuracy code
! ----------------------------
RECORD01     1  "0.14"
  ## widget = lineedit; check_type = real; check_min = 0.0

MSG_RECORD01 1  "P = P0 + 1 : residual larger than"

RECORD02     1  "0.19"
  ## widget = lineedit; check_type = real; check_min = 0.0

MSG_RECORD02 1  "P = P0 + 2 : residual larger than"

RECORD03     1  "0.30"
  ## widget = lineedit; check_type = real; check_min = 0.0

MSG_RECORD03 1  "P = P0 + 3 : residual larger than"

RECORD04     1  "0.50"
  ## widget = lineedit; check_type = real; check_min = 0.0

MSG_RECORD04 1  "P = P0 + 4 : residual larger than"

RECORD05     1  "0.70"
  ## widget = lineedit; check_type = real; check_min = 0.0

MSG_RECORD05 1  "P = P0 + 5 : residual larger than"

RECORD06     1  "1.10"
  ## widget = lineedit; check_type = real; check_min = 0.0

MSG_RECORD06 1  "P = P0 + 6 : residual larger than"

RECORD07     1  "1.50"
  ## widget = lineedit; check_type = real; check_min = 0.0

MSG_RECORD07 1  "P = P0 + 7 : residual larger than"

RECORD08     1  "5.00"
  ## widget = lineedit; check_type = real; check_min = 0.0

MSG_RECORD08 1  "P = P0 + 8 : residual larger than"

RECORD09     0
  ## widget = lineedit; check_type = real; check_min = 0.0

MSG_RECORD09 1  "P = P0 + 9 : residual larger than"


# BEGIN_PANEL NO_CONDITION #####################################################
# SET ACCURACY CODES IN PRECISE ORBITS - PREWEI 1: Filenames                   #
#                                                                              #
# GENERAL FILES                                                                #
#   Show all general files  > % <                                              # SHOWGEN
#                                                                              #
# INPUT FILES                                                                  #
#   List file from ORBGEN   > %%%%%%%% <                                       # LIST
#   Precise orbit files     > %%%%%%%% <                                       # PREFIL
#                                                                              #
# RESULT FILES                                                                 #
#   Precise orbit files     > %%%%%%%% <       (blank: same as input)          # PREOUT
#                                                                              #
# GENERAL OUTPUT FILES                                                         #
#   Program output          > % < use PREWEI.Lnn            or    > %%%%%%%% < # SYSODEF SYSOUT
#   Error messages          > % < merged to program output  or    > %%%%%%%% < # ERRMRG SYSERR
#                                                                              #
# END_PANEL ####################################################################


# BEGIN_PANEL SHOWGEN = 1 ######################################################
# PREWEI 1.1: General Files                                                    #
#                                                                              #
# GENERAL INPUT FILES                                                          #
#   General constants       > %%%%%%%%%%%% <                                   # CONST
#                                                                              #
# MENU SETTINGS                                                                #
#   Selected campaign       > %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% <   # CAMPAIGN
#   Selected session          year> %%%% <  session> %%%% <                    # YR4_INFO SES_INFO
#   Session table           > %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% <   # SESSION_TABLE
#                                                                              #
# TEMPORARY FILES                                                              #
#   Scratch file            > %%%%%%%%%%%% <                                   # SCRATCH
#                                                                              #
# END_PANEL ####################################################################


# BEGIN_PANEL NO_CONDITION #####################################################
# PREWEI 2: Options                                                            #
#                                                                              #
# TITLE  > %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% <  # TITLE
#                                                                              #
# ACCURACY CODES                                                               #
#   > % < Take accuracy codes from table (next panel)                          # RADIO1
#   > % < Compute accuracy codes from RMS values                               # RADIO2
#                                                                              #
#   Minimum accuracy code P0 > %% <                                            # MINACC
#   Use codes from file      > % <                                             # FROMFILE
#                                                                              #
#   Satellites with accuracy code greater than > %% <   will be  > %%%%%%%% <  # MAXACC FLGDEL
#                                                                              #
# END_PANEL ####################################################################


# BEGIN_PANEL RADIO1 = 1 #######################################################
# PREWEI 3: Table for Accuracy Codes                                           #
#                                                                              #
# RESIDUAL LEVEL FOR ACCURACY CODES                                            #
#   P = P0 + 1 : residual larger than > %%%%%% < meters  ( 0.140)              # RECORD01
#   P = P0 + 2 : residual larger than > %%%%%% < meters  ( 0.190)              # RECORD02
#   P = P0 + 3 : residual larger than > %%%%%% < meters  ( 0.300)              # RECORD03
#   P = P0 + 4 : residual larger than > %%%%%% < meters  ( 0.500)              # RECORD04
#   P = P0 + 5 : residual larger than > %%%%%% < meters  ( 0.700)              # RECORD05
#   P = P0 + 6 : residual larger than > %%%%%% < meters  ( 1.100)              # RECORD06
#   P = P0 + 7 : residual larger than > %%%%%% < meters  ( 1.500)              # RECORD07
#   P = P0 + 8 : residual larger than > %%%%%% < meters  ( 5.000)              # RECORD08
#   P = P0 + 9 : residual larger than > %%%%%% < meters  (      )              # RECORD09
#                                                                              #
# END_PANEL ####################################################################
