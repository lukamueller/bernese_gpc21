<html>
<body>

<center>
<b>CONVERT RESIDUAL FILES (ASCII TO BINARY)</b>
<p>Program: FMTRES
</center>
<hr width="100%">

<a name="ENVIRONMENT"   default="%% %%"></a>
<a name="CAMPAIGN"      default=""></a>
<a name="SESSION_TABLE" default="SESSIONS"></a>
<a name="YR4_INFO"      default="$Y+0"></a>
<a name="SES_INFO"      default="$S+0"></a>
<a name="USR_INFO"      default="${USER}"></a>

<p><b>GENERAL DESCRIPTION</b>
<p>
The program FMTRES converts formatted ASCII residual files back into
binary files. The program allows the conversion of a number of ASCII residual
files in one run and is mainly used for residual file transfer between
different computer systems using different binary formats: The files are
first converted into ASCII using the program
<a href="PGMLST.HLP#RESFMT">RESFMT</a>, then transferred to the
target computer, and finally reconverted from ASCII into binary using this
program here.

<p><hr>

<p><b>FMTRES 1: Input/Output Files</b>

<a NAME="SHOWGEN" default="1"></a>
<p><b>GENERAL FILES</b>
<p><b>Show all general files:</b>
This program does not need general files. If you check this box, the currently
active campaign, session and session table are displayed.

<br><br>

<a NAME="RESFILE" default=""></a>
<p><b>INPUT FILES</b>
<p><b>Residual file(s):</b>
Specify a list of ASCII residual files to be converted. The binary
version will be written into files with the same names but with a
different extension (default: RES).

<br><br>

<a NAME="SYSODEF" default="1"></a>
<a NAME="SYSOUT"  default="FMTRES"></a>
<p><b>GENERAL OUTPUT FILES</b>
<p><b>Program output:</b>
You can choose to have the program output written to a separate file for each
program run (checkbox marked), or specify a common name for all program runs.
<br>
If you mark the checkbox, the filename will correspond to the program name
with an extension containing a counter (Lnn), which is automatically
incremented for each program run. If the counter reaches the maximum value, it
is automatically reset, and existing files will be overwritten.
The maximum value can be set in
<b>Menu &gt; Configure &gt; Program Names</b>, option "Maximum program output
file number".
<br>
If the checkbox is unmarked, you have to specify a name for the program
output file. This allows to characterize the program run, e.g., by using the
day and year (<tt>$YD+0</tt>) in the filename.

<a NAME="ERRMRG" default="0"></a>
<a NAME="SYSERR" default="ERROR"></a>
<p><b>Error messages:</b>
You can choose to have error messages written into the program's output
file (see above), or specify a separate file to which error messages are
written.

<br><br>

<a NAME="TITLE" default=""></a>
<p><b>TITLE</b>
<p>
This title line will be printed as header comment into the program output.
The title should characterize the program run.

<p><hr>

</body>
</html>
