<html>
<body>

<center>
<b>COORDINATE COMPARISON</b>
<p>Program: COMPAR2
</center>
<hr width="100%">

<a name="ENVIRONMENT"   default="%% %%"></a>
<a name="CAMPAIGN"      default=""></a>
<a name="SESSION_TABLE" default="SESSIONS"></a>
<a name="YR4_INFO"      default="$Y+0"></a>
<a name="SES_INFO"      default="$S+0"></a>
<a name="USR_INFO"      default="${USER}"></a>

<p><b>GENERAL DESCRIPTION</b>
<p>
The program COMPAR allows a comparison of two or more coordinate
sets including:
<ul>
<li>the estimation of repeatability RMS values for each site.
<li>the generation of a repeatability table for each station to detect
    problems in one of the input coordinate files.
<li>the computation of a mean coordinate set and save the results in a new
    coordinate file.
<li>the computation of a linear velocity field and saving the results in a 
    new velocity file.
<li>display baseline statistics in the north, east, and up component,
    and in baseline length.
</ul>

<p><i>Remark:</i>
The program issues a warning message if the epochs of the coordinate files
are different. The user has to decide for which time interval the
station coordinates are comparable without applying velocities. Alternatively
a velocity input file may be used to adjust the coordinates to a specific
epoch for comparison.

<p><hr>

<p><b>COMPAR2 1: Input Files</b>

<a name="SHOWGEN" default="1"></a>
<p><b>GENERAL FILES</b>
<p><b>Show all general files:</b>
Check this box to get a list of the general input files used by this program
as well as the currently active campaign, session, and session table.

<br><br>

<a name="COOFIL" default=""></a>
<p><b>INPUT FILES</b>
<p><b>Coordinates for comparison:</b>
Select the coordinate files to be compared.

<p><a href="/$X/DOC/EXAMPLE.CRD">Example</a>

<a name="STAINFO" default=""></a>
<p><b>Station information:</b>
This file contains information on stations. It is used by COMPAR2 to change
the station names in the input coordinate files before comparing the coordinates
or when estimating linear station velocities.

<p><a href="/$X/DOC/EXAMPLE.STA">Example</a>

<a name="COORD" default=""></a>
<p><b>A priori coordinates:</b>
When saving the resulting (mean) coordinates in a file you may specify
an a priori coordinate file as a skeleton. The resulting coordinate
file will then include all the coordinates estimated in this COMPAR run
and, in addition, all the coordinates of the other sites that are listed in
the a priori coordinate file to preserve the complete list of site
coordinates.

<p><a href="/$X/DOC/EXAMPLE.CRD">Example</a>

<a name="VELAPR" default=""></a>
<p><b>A priori velocities:</b>
The a priori velocities may either be used to unify the epochs of the input
coordinate sets before comparison, see option "REFERENCE EPOCH
CORRECTION" for further details. Alternatively, it can be used as the
"A priori coordinate" file as a skeleton when saving estimated linear 
velocities.

<p><a href="/$X/DOC/EXAMPLE.VEL">Example</a>

<a name="PSDDAT" default=""></a>
<p><b>PSD correction (ITRF14):</b>
The ITRF14 solution contains so called post-seismic deformation (PSD) 
corrections. They are provided together with the other ITRF-related
files.
<br>
Specify the psd-file to apply the PSD corrections according to the 
instructions for using the ITRF2014 solution 
(see http://itrf.ign.fr/ITRF_solutions/2014/psd.php) in conjunction with the
related linear station velocities (see above).

<p><i>Remark:</i>
The PSD corrections have to be applied in addition to the linear station
velocities provided with the ITRF2014 solution.

<p>As the ITRF2014 comes along with two formats of the PSD corrections we 
support them in both:
<br><a href="/$X/DOC/EXAMPLE.PSD">Example (CATREF internal)</a> or
<br><a href="/$X/DOC/EXAMPL2.PSD">Example (SINEX-style)</a>

<a name="BASDEF" default=""></a>
<p><b>Baseline definitions:</b>
A baseline definition file can be specified to select baselines
for the analysis of baseline repeatabilities in the north, east, and up
components, or in geocentric X,Y,Z components, and in baseline length.
<br>
A filename is needed in this option in order to activate the repeatability
option "LOCAL" or "GEOCENTRIC" in the following panel.

<p><a href="/$X/DOC/EXAMPLE.BSL">Example</a>

<p><hr>

<p><b>COMPAR2 1.1: General Files</b>

<a name="CONST" default="CONST."></a>
<p><b>GENERAL INPUT FILES</b>
<p><b>General constants:</b>
This file contains all the physical and astronomical constants used
in the Bernese GNSS Software. Most probably you will never have to
modify this file.

<p>Example: <a href="/$X/GEN/CONST.">$X/GEN/CONST.</a>

<a name="DATUM" default="DATUM."></a>
<p><b>Geodetic datum:</b>
This file contains the definition of the geodetic datum and of the
ellipsoid to be used to compute ellipsoidal coordinates. It is used
to apply antenna heights and for the printing of coordinates in the
local system (north, east, up). You only have to modify this file
when you want to introduce a new reference ellipsoid.

<p><i>Caution:</i>
This file has no effect on Cartesian coordinates.

<p>Example: <a href="/$X/GEN/DATUM.">$X/GEN/DATUM.</a>

<p><hr>

<p><b>COMPAR2 2: Output Files</b>

<a name="COORDRS" default=""></a>
<p><b>RESULT FILES</b>
<p><b>Mean station coordinates:</b>
To save the resulting mean/combined site coordinates enter a coordinate file
name here. If the field is blank, coordinates are not saved.

<p><i>Remark:</i>
Mean coordinates are only reported in the output file if they are not in
conflict with the options in the "NOTIFICATION OF POSSIBLE OUTLIERS" in panel
"COMPAR2 4: Comparison of Individual Solutions".

<p><a href="/$X/DOC/EXAMPLE.CRD">Example</a>

<a name="VELORS" default=""></a>
<p><b>Linear station velocities:</b>
If a result file is specified in this input field in addition to
the mean coordinates linear velocities are estimated for each coordinate
component. The results are stored in the given file.

<p><i>Remark:</i>
Station velocities will only be included in the output file if there is no
conflict with the options in the "NOTIFICATION OF POSSIBLE OUTLIERS" in panel
"COMPAR2 4: Comparison of Individual Solutions".

<p><a href="/$X/DOC/EXAMPLE.VEL">Example</a>

<br><br>

<a name="WEKSUM" default=""></a>
<p><b>OUTPUT FILES</b>
<p><b>Weekly summary file:</b>
To save a short statistic (the available sites and a summary of the
repeatabilities) enter a summary file name here.  This possibility may be
used to create a summary file for the weekly report of a regional analysis
center.
<br>
The summary file contains the list of stations, the number of coordinate
files contributing to the mean solution, the coordinate flags from the
first seven input files, and the repeatability for the north, east,
up component in millimeter.

<p>Output example:<!-- $S/RAP/IGSRAPID.11_323 -->
<pre>
 COMPARISON OF IGSRAPID (RQ) COORDINATE FILES           
 -------------------------------------------------------------------------------
 Total number of stations:   129
 -------------------------------------------------------------------------------
                       Weekday     Repeatability (mm)
 Station        #Days  0123456      N      E      U  
 -------------------------------------------------------------------------------
 ADIS 31502M001     7  XXXXXXX     1.63   2.01   4.04
 ALIC 50137M001     6  XXXXXX      1.27   0.64   2.67
 AMU2 66040M002     7  XXXXXXX     3.55   4.39   4.98
 AREQ 42202M005     7  XXXXXXX     1.93   1.94   5.04
 ARTU 12362M001     7  XXXXXXX     0.70   0.64   7.00
 ASPA 50503S006     7  XXXXXXX     2.59   4.19  11.12
 AUCK 50209M001     7  XXXXXXX     1.19   1.51   5.30
 AZCO 49454M001     7  XXXXXXX     1.65   1.97   4.45
 BADG 12338M002     7  XXXXXXX     1.27   1.17   1.18
 BAKE 40152M001     7  XXXXXXX     1.39   0.68   3.53
 BAKO 23101M002     7  XXXXXXX     2.87   1.89   6.53
 BJCO 32701M001     7  XXXXXXX     2.30   0.92   4.44
 BOGO 12207M002     2  XX          2.72   0.34   1.36
 BRFT 41602M002     6   XXXXXX     1.57   3.36   2.25
 BRMU 42501S004     7  XXXXXXX     1.38   1.76   2.62
...
 WTZZ 14201M014     5  XX XX X     0.89   0.85   8.19
 XMIS 50183M001     7  XXXXXXX     1.66   1.77   3.35
 YARR 50107M006     1    X         0.00   0.00   0.00
 ZIM2 14001M008     7  XXXXXXX     1.09   0.56   2.48
 ZIMM 14001M004     7  XXXXXXX     1.10   0.79   2.48
 -------------------------------------------------------------------------------
 # Coordinate estimates:   835     1.49   1.58   4.72
</pre>

<p><i>Remark:</i>
If you specify more than 7 input coordinate files
the flags of the stations in all files after the seventh file will not be
displayed. The repeatability will, however, be computed from all
contributing files.
<br>
If less than 7 input files are
selected only as many columns of flags as selected files are reported in this
summary.

<a name="PLOTRS" default=""></a>
<p><b>Plot file of results:</b>
Specify a filename in order to store coordinate repeatability residuals
in machine-readable form. The file will contains the station name, epoch
and component (1=North, 2=East, 3=Up) number, residual in meters, and epoch in
modified Julian date.

<p>Output example:<!-- $outf/AMUN.PLT -->
<pre>
ABMF 97103M001      1 1   0.00117   55890.49983
ABMF 97103M001      1 2   0.00139   55890.49983
ABMF 97103M001      1 3   0.00353   55890.49983
ABMF 97103M001      2 1   0.00143   55891.49983
ABMF 97103M001      2 2   0.00067   55891.49983
ABMF 97103M001      2 3  -0.00227   55891.49983
ABMF 97103M001      3 1   0.00108   55892.49983
...
ABMF 97103M001     30 1   0.00069   55919.49983
ABMF 97103M001     30 2   0.00150   55919.49983
ABMF 97103M001     30 3   0.00705   55919.49983
ABPO 33302M001      1 1   0.00195   55890.49983
ABPO 33302M001      1 2  -0.00051   55890.49983
ABPO 33302M001      1 3  -0.00102   55890.49983
ABPO 33302M001      2 1   0.00085   55891.49983
ABPO 33302M001      2 2  -0.00047   55891.49983
ABPO 33302M001      2 3  -0.00215   55891.49983
...
</pre>

<br><br>

<a name="SYSODEF" default="1"></a>
<a name="SYSOUT" default="COMPAR2"></a>
<p><b>GENERAL OUTPUT FILES</b>
<p><b>Program output:</b>
You can choose to have the program output written to a separate file for each
program run (checkbox marked), or specify a common name for all program runs.
<br>
If you mark the checkbox, the filename will correspond to the program name
with an extension containing a counter (Lnn), which is automatically
incremented for each program run. If the counter reaches the maximum value, it
is automatically reset, and existing files will be overwritten.
The maximum value can be set in
<b>Menu &gt; Configure &gt; Program Names</b>, option "Maximum program output
file number".
<br>
If the checkbox is unmarked, you have to specify a name for the program
output file. This allows to characterize the program run, e.g., by using the
day and year (<tt>$YD+0</tt>) in the filename.

<a name="ERRMRG" default="0"></a>
<a name="SYSERR" default="ERROR"></a>
<p><b>Error messages:</b>
You can choose to have error messages written into the program's output
file (see above), or specify a separate file to which error messages are
written.

<p><hr>

<p><b>COMPAR2 3: Processing Options</b>

<a name="TITLE" default=""></a>
<p><b>TITLE</b>
<p>
This title line will be printed as header comment into the program output
and will be saved in all the result files to document the program run.
The title should characterize the program run by, e.g., giving the most
important options used and the session.

<br><br>

<a name="BASREP" default="NO"></a>
<p><b>REPEATABILITY OPTION</b>
<p>
Specify whether baseline statistics are computed or not.
A "Baseline definitions" file must be given under "INPUT FILES" above in order
to enable this option. Only the
baselines listed in this file are considered. Wildcards (<tt>*</tt>) are not
allowed in the baseline definition file for the program COMPAR2.
<br>
Valid options are:
<ul>
<li><b>NO</b>: no baseline statistics at all
<li><b>LOCAL</b>: baseline residuals displayed in local north, east, up system
<li><b>GEOCENTRIC</b>: baseline residuals displayed in geocentric x,y,z system
</ul>

<p><i>Default value:</i> <tt>NO</tt>

<br><br>

<br><br>

<a name="REFEPO" default="MEAN"></a>
<a name="EPOCH" default=""></a>
<p><b>REFERENCE EPOCH CORRECTION</b>
<p>
If an a priori velocity file was specified, the reference epochs of the
input coordinate files may be specified. The epoch of comparison may be chosen
between
<ul>
<li><b>AS_IT_IS:</b> no epoch corrections are applied.
<li><b>FIRST:</b> all coordinates are corrected to the epoch of the first
    coordinate input file.
<li><b>LAST:</b> all coordinates are corrected to the epoch of the last
    coordinate input file.
<li><b>MEAN:</b> all coordinates are corrected to the mean epoch
    between the first and last coordinate input file.
<li><b>MANUAL:</b> all coordinates are corrected to the epoch
    specified in the input field.
</ul>

<p><i>Default value:</i> <tt>MEAN</tt>

<br><br>

<a name="CRXWARN" default="1"></a>
<p><b>OUTPUT OPTIONS</b>
<p><b>Notify changes due to station information file:</b>
With this option enabled a warning message is issued in the program output
file notifying each change of station information performed based on the
content of the station information file specified in panel "COMPAR2 1:
Input Files".

<p><i>Default value:</i> marked

<br><br>

<a name="FLAG" default="NONBLANK"></a>
<a name="FLAGSPEC" default=""></a>
<p><b>COORDINATE FLAGS</b>
<p><b>Special flags:</b>
You have the possibility to specify the sites in the coordinate files
which should be included in the comparisons:
<ul>
<li><b>NONBLANK</b>: use all sites with non-blank flags
<li><b>ALL</b>: use all site coordinates, even if they were not processed
    (no flag)
    <br>
    Because you cannot have variance-covariance information for the
    non-processed sites no variance-covariance input files
    are considered if you enter <tt>ALL</tt> here.
<li><b>SPECIAL</b>: to specify a list of flags in a separate field
</ul>

<p><i>Default value:</i> <tt>NONBLANK</tt>

<p><hr>

<p><b>COMPAR2 4: Comparison of Individual Solutions</b>

<a name="BADSOL_N" default="15"></a>
<a name="BADSOL_E" default="15"></a>
<a name="BADSOL_U" default="30"></a>
<p><b>NOTIFICATION OF POSSIBLE OUTLIERS</b>
<p>
The options in this panel allow it to automatically generate a list of
stations for which the coordinate comparison indicates anomalous behavior.
Note that enabling the options does not have any influence on the result but
does only notify possible outliers in the program output.

<p><b>Maximum tolerated residual:</b>
Specify threshold limits for coordinate repeatability residuals in north,
east, and up components.

<p><a name="BADSOLRN" default="10"></a>
   <a name="BADSOLRE" default="10"></a>
   <a name="BADSOLRU" default="20"></a>
<b>Maximum tolerated root-mean-square error</b>
Specify threshold limits for coordinate repeatability RMS in north,
east, and up components.

<p><a name="MINSOL" default="0"></a>
<b>Minimum number of solutions for each station:</b>
Stations contributing to the combined solution with fewer solutions than
specified in this field are listed in the program output.

<a name="STALISTRS" default="">
<p><b>LIST OF ACCEPTED MEAN COORDINATES/VELOCITIES</p>
All stations passed the the above specified repeatability criteria will be
listed in a station selection file.

<p><a href="/$X/DOC/EXAMPLE.FIX">Example</a>

<p><hr>

</body>
</html>
