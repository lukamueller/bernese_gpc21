package AMBXTR;
@ISA = ("RUNBPE");
# ============================================================================
#
# Name    :  AMBXTR
#
# Purpose :  Create comprehensive ambiguity resolution summary
#
# PARAMs  :
#
# Author  :  S. Schaer
# Created :  30-Mar-2005
#
# Changes :  28-Feb-2011 SS: Narrow-lane summary forced to GPS-only
#            11-Aug-2011 RD: Updated for version 5.2
#
# ============================================================================
use strict;

use lib $ENV{BPE};
use bpe_util qw(appFile);
use File::Copy;

sub run {
  my $bpe = shift;

# Get variables
# -------------
  my ($yyssss,
      $lenAmb, $lenQIF, $lenL53, $lenL12,
      $dirOut) =
  $bpe->getKeys
     ('$YSS+0',
      'V_BL_AMB','V_BL_QIF','V_BL_L53','V_BL_L12',
      'DIR_OUT');

# Initialize processing summary file
# ----------------------------------
  my $sumFil = "${dirOut}AMB${yyssss}.SUM";
  unlink $sumFil;
  my $tmpFil = "${dirOut}TMP${yyssss}.SUM";
  unlink $tmpFil;

# Run program
# -----------
  my $PGMNAM = "GPSXTR";

# Code-Based Widelane (WL) Ambiguity Resolution
# ---------------------------------------------
  $bpe->putKey("$ENV{U}/PAN/GPSXTR.INP","OUTPUT","????\$S+0_W");
  $bpe->putKey("$ENV{U}/PAN/GPSXTR.INP","QIFOUT","TMP\$YSS+0");

  $bpe->putKey("$ENV{U}/PAN/GPSXTR.INP","AMBLOD","EACH&ALL");
  $bpe->putKey("$ENV{U}/PAN/GPSXTR.INP","AMBID","WL");
  $bpe->RUN_PGMS($PGMNAM);
  if (-s $tmpFil) {
     appFile(" Code-Based Widelane (WL) Ambiguity Resolution (<${lenAmb} km)",
       0,$tmpFil,$sumFil,1);
    unlink $tmpFil }

# Code-Based Narrowlane (NL) Ambiguity Resolution
# -----------------------------------------------
  $bpe->putKey("$ENV{U}/PAN/GPSXTR.INP","OUTPUT","????\$S+0_N");

  $bpe->putKey("$ENV{U}/PAN/GPSXTR.INP","AMBLOD","GPS");
  $bpe->putKey("$ENV{U}/PAN/GPSXTR.INP","AMBID","NL");
  $bpe->RUN_PGMS($PGMNAM);
  if (-s $tmpFil) {
     appFile(" Code-Based Narrowlane (NL) Ambiguity Resolution (<${lenAmb} km)",
       0,$tmpFil,$sumFil,1);
    unlink $tmpFil }

# Phase-Based Widelane (L5) Ambiguity Resolution
# ----------------------------------------------
  $bpe->putKey("$ENV{U}/PAN/GPSXTR.INP","OUTPUT","????\$S+0_5");

  $bpe->putKey("$ENV{U}/PAN/GPSXTR.INP","AMBLOD","EACH&ALL");
  $bpe->putKey("$ENV{U}/PAN/GPSXTR.INP","AMBID","L5");
  $bpe->RUN_PGMS($PGMNAM);
  if (-s $tmpFil) {
     appFile(" Phase-Based Widelane (L5) Ambiguity Resolution (<${lenL53} km)",
       0,$tmpFil,$sumFil,1);
    unlink $tmpFil }

# Phase-Based Narrowlane (L3) Ambiguity Resolution
# ------------------------------------------------
  $bpe->putKey("$ENV{U}/PAN/GPSXTR.INP","OUTPUT","????\$S+0_3");

  $bpe->putKey("$ENV{U}/PAN/GPSXTR.INP","AMBID","L3");
  $bpe->RUN_PGMS($PGMNAM);
  if (-s $tmpFil) {
     appFile(" Phase-Based Narrowlane (L3) Ambiguity Resolution (<${lenL53} km)",
       0,$tmpFil,$sumFil,1);
    unlink $tmpFil }

  $bpe->putKey("$ENV{U}/PAN/GPSXTR.INP","OUTPUT","????\$S+0_Q");

# Quasi-Ionosphere-Free (QIF) Ambiguity Resolution
# ------------------------------------------------
  $bpe->putKey("$ENV{U}/PAN/GPSXTR.INP","AMBID","QIF");
  $bpe->RUN_PGMS($PGMNAM);
  if (-s $tmpFil) {
     appFile(" Quasi-Ionosphere-Free (QIF) Ambiguity Resolution (<${lenQIF} km)",
       0,$tmpFil,$sumFil,1);
    unlink $tmpFil }

# Direct L1/L2 Ambiguity Resolution
# ---------------------------------
  $bpe->putKey("$ENV{U}/PAN/GPSXTR.INP","OUTPUT","????\$S+0_1");

  $bpe->putKey("$ENV{U}/PAN/GPSXTR.INP","AMBID","L12");
  $bpe->RUN_PGMS($PGMNAM);
  if (-s $tmpFil) {
     appFile(" Direct L1/L2 Ambiguity Resolution (<${lenL12} km)",
       0,$tmpFil,$sumFil,1);
    unlink $tmpFil }

}
