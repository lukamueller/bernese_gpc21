package GNSAMBAP;
@ISA = ("RUNBPE");
# ============================================================================
#
# Name    :  GNSAMBAP
#
# Purpose :  Prepare parallel WL/NL ambiguity resolution process for a set of
#            baselines selected by BASLST
#
# Remark  :  The maximum length of a baseline to be selected by BASLST is
#            taken from variable V_BL_AMB
#
# PARAMs  :
#
# Author  :  S. Schaer
# Created :  30-Mar-2005
#
# Changes :  11-Aug-2011 RD: Updated for version 5.2
#            15-Jul-2013 RD: Do not include baselines shorter than V_BL_53
#
# ============================================================================
use strict;

use lib $ENV{BPE};
use bpe_util qw(initPar_Bl);

sub run {
  my $bpe = shift;

# Get variables
# -------------
  my ($yyssss, $ssss,
      $blMax, $blMin,
      $dirBsl, $dirOut, $dirRes, $dirEdt, $dirSum,
      $extBsl, $extOut, $extRes, $extEdt, $extSum) =
  $bpe->getKeys
     ('$YSS+0', '$S+0',
      'V_BL_AMB','V_BL_L53',
      'DIR_BSL', 'DIR_OUT', 'DIR_RES', 'DIR_EDT', 'DIR_SUM',
      'EXT_BSL', 'EXT_OUT', 'EXT_RES', 'EXT_EDT', 'EXT_SUM');

# Delete old files (OUT, SEL)
# ---------------------------
  my $basFil = "${dirBsl}AMB${yyssss}.${extBsl}";
  unlink ($basFil);

  unlink glob("${dirOut}????${ssss}_C.${extOut}");
  unlink glob("${dirRes}????${ssss}_C.${extRes}");
  unlink glob("${dirEdt}????${ssss}_C.${extEdt}");
  unlink glob("${dirSum}????${ssss}_C.${extSum}");
  unlink glob("${dirOut}????${ssss}_W.${extOut}");
  unlink glob("${dirOut}????${ssss}_N.${extOut}");

# Set maximum/minimum baseline length if defined
# ----------------------------------------------
  $bpe->putKey("$ENV{U}/PAN/BASLST.INP","MAXLEN",$blMax) unless $blMax eq "";
  if ( $blMin ne "" ) {
    $blMin = sprintf "%d",int(0.9*$blMin);
    $bpe->putKey("$ENV{U}/PAN/BASLST.INP","MINLEN",$blMin);
  }
  $bpe->putKey("$ENV{U}/PAN/BASLST.INP","SELBSL",$basFil);

# Run program
# -----------
  my $PGMNAM = "BASLST";
  $bpe->RUN_PGMS($PGMNAM);

# Get baselines to be processed
# -----------------------------
  open (BAS,$basFil);
  my @basLin = <BAS>;
  close BAS;
  unlink ($basFil);

  chomp(@basLin);

  map { print "$_\n"; } @basLin;

# Initialize parallel run
# -----------------------
  initPar_Bl($bpe,@basLin);

}
