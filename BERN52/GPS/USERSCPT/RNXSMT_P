package RNXSMT_P;
@ISA = ("RUNBPE");
# ============================================================================
#
# Name    :  RNXSMT_P
#
# Purpose :  Execute RNXSMT (parallel mode)
#
# PARAMs  :  PARAM1 - temporary file for parallel processing
#
# Author  :  M. Meindl
# Created :  23-Jul-2003
#
# Changes :  11-Aug-2011 RD: Updated for version 5.2
#
# ============================================================================
use strict;

use lib $ENV{BPE};
use bpe_util qw(prtMess getWarn setUserVar replaceSel prtBPEfile);

sub run {
  my $bpe = shift;

# Get variables
# -------------
  my ($ssss , $ddd , $yyssss ,
      $param1 , $subPid  , $clu   , $dirOut) =
   $bpe->getKeys
     ('$S+0', '$+0', '$YSS+0',
      'PARAM1', 'SUB_PID', 'V_CLU', 'DIR_OUT');

# Set cluster number and input files
# ----------------------------------
  my $cluster = $clu eq "" ? "$ssss$param1" : "$ssss$subPid";
  my $ffff    = $clu eq "" ? $param1        : "____";

  setUserVar($bpe,"CLUSTER",$cluster,"FFFF",$ffff,"CCC",$subPid);

  if ($clu eq "") {
    prtMess($bpe,"PROCESSING FILE $param1$ssss");
    $bpe->putKey("$ENV{U}/PAN/RNXSMT.INP","RNXFIL","$param1$ssss");
  } else {
    replaceSel($bpe,"RNXSMT","RNXFIL",$param1);
  }

# Run program
# -----------
  my $PGMNAM = "RNXSMT";
  $bpe->RUN_PGMS($PGMNAM);

# Extract warning/error messages from protocol file
# -------------------------------------------------
  getWarn($bpe,"${dirOut}RNX${yyssss}.ERR");

# Print processed stations BPE file
# ---------------------------------
  prtBPEfile($param1,0) if ($clu ne "");

}
