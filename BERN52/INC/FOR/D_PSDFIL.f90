! -------------------------------------------------------------------------
! Bernese GPS Software Version 5.2
! -------------------------------------------------------------------------

MODULE d_psdFil

! -------------------------------------------------------------------------
! Purpose:    Handle both versions of the PSD corrections that are
!             on the market (.DAT and .SNX)
!
! Remark:     PSD stands for "post-seismic deformation" and is used in
!             the context of the ITRF2014 solution
!
! Acknowledgement:
!             The routine readDat is extracted from the routine "PSDcorr"
!             that was initially provided by Zuheir Altamimi
!
! Author:     R. Dach
!
! Created:    30-Jul-2016
! Last mod.:  30-Jul-2016
!
! Changes:
!
! Copyright:  Astronomical Institute
!             University of Bern
!             Switzerland
! -------------------------------------------------------------------------

! Modules
! -------
  USE m_bern
  IMPLICIT NONE

  PRIVATE
  PUBLIC ::   t_psdSta, getPsd

! Local Parameters
! ----------------
  ! Max. number of events per station
  INTEGER(i4b), PARAMETER             :: maxPsd = 20


! Structure to contain one individual correction
! ----------------------------------------------
  TYPE t_psdRec
    REAL(r8b)                         :: epoch    ! epoch - event (in years)
    INTEGER(i4b), DIMENSION(3)        :: type     ! type of the corrections
    REAL(r8b),  DIMENSION(3,4)        :: psdCor   ! coefficients of the corr.
                                                  ! in north, east, up
  END TYPE t_psdRec


! Structure to contain the corrections for a station
! --------------------------------------------------
  TYPE t_psdSta
    CHARACTER(LEN=staNameLength)      :: staNam   ! station name
    INTEGER(i4b)                      :: nPsd     ! number of events
    TYPE(t_psdRec), DIMENSION(maxPsd) :: psdRec   ! PSD corrections
  END TYPE t_psdSta


! Local variables
! ---------------
  ! Name of the PSD file
  CHARACTER(LEN=fileNameLength)       :: psdFil = ''

  ! Type of the PSD file (either "SNX" or "DAT")
  CHARACTER(LEN=3)                    :: psdTyp = ''


CONTAINS

! -------------------------------------------------------------------------
! Return the PSD corrections for a station
! -------------------------------------------------------------------------
  FUNCTION getPsd(filNam,staNam,epoch)

  USE s_opnfil
  USE s_opnerr

! Parameters
  ! in:
  CHARACTER(LEN=fileNameLength)      :: filNam  ! Name of the PSD file
  CHARACTER(LEN=staNameLength)       :: staNam  ! station name
  REAL(r8b)                          :: epoch   ! epoch of the coordinates to
                                                ! be corrected in MJD

  ! out:
  TYPE(t_psdSta)                     :: getPsd  ! Strcuture with all relevant
                                                ! PSD corrections
! Local Parameters
  CHARACTER(LEN=15), PARAMETER :: srName = 'd_psdFil:getPsd'

! Local variables
  CHARACTER(LEN=shortLineLength)     :: line
  INTEGER(i4b)                       :: ios

! Check whether the type of the PSD file is known
! -----------------------------------------------
  IF ( filNam /= psdFil .OR. PSDTyp == '' ) THEN

    ! open the file and read the first line
    CALL opnfil(lfnloc,filNam,'OLD','FORMATTED','READONLY',' ',ios)
    CALL opnerr(lfnerr,lfnloc,ios,filNam,srName)
    READ(lfnloc,'(A)') line
    CLOSE(lfnloc)

    ! Check the type
    IF ( line(1:5) == '%=SNX' ) THEN
      psdTyp = 'SNX'
    ELSE
      psdTyp = 'DAT'
    ENDIF
    psdFil = filNam
  ENDIF


! Read the relevant values with the correct subroutine
! ----------------------------------------------------
  IF ( psdTyp == 'DAT' ) THEN
    getPsd = readDat(staNam,epoch)
  ELSE
    getPsd = readSnx(staNam,epoch)
  ENDIF

  RETURN
  END FUNCTION getPsd


! -------------------------------------------------------------------------
! Reading the DAT file format
! -------------------------------------------------------------------------
  FUNCTION readDAT(staNam,epoch)

  USE s_opnfil
  USE s_opnerr
  USE f_djul
  USE f_iyear4
  USE s_upperc
  USE s_dimtst

! Parameters
  ! in:
  CHARACTER(LEN=staNameLength)       :: staNam  ! station name
  REAL(r8b)                          :: epoch   ! epoch of the coordinates to
                                                ! be corrected in MJD

  ! out:
  TYPE(t_psdSta)                     :: readDat ! Strcuture with all relevant
                                                ! PSD corrections
! Local Parameters
  CHARACTER(LEN=16), PARAMETER :: srName = 'd_psdFil:readDat'

! Local variables
  CHARACTER(LEN=shortLineLength)     :: line1, line2, line3
  CHARACTER(LEN=shortLineLength)     :: format100, format110
  CHARACTER(LEN=shortLineLength)     :: format101, format102
  CHARACTER(LEN=shortLineLength)     :: format111, format112
  CHARACTER(LEN=staNamelength)       :: stNameP
  CHARACTER(LEN=9)                   :: domesp
  CHARACTER(LEN=4)                   :: namep
  CHARACTER(LEN=1)                   :: compe,compn,compu

  REAL(r8b)                          :: tsec
  REAL(r8b)                          :: ae1,te1,ae2,te2
  REAL(r8b)                          :: an1,tn1,an2,tn2
  REAL(r8b)                          :: au1,tu1,au2,tu2
  REAL(r8b)                          :: mjdq,rmjd

  INTEGER(i4b)                       :: iyr, iday, isec
  INTEGER(i4b)                       :: mode,modn,modu
  INTEGER(i4b)                       :: ios1, ios2, ios3
  INTEGER(i4b)                       :: ios, irc


! Initialize the structure
! ------------------------
  readDat%nPSD   = 0
  readDat%staNam = staNam

! Open PSD file
! -------------
  CALL opnfil(lfnloc,psdfil,'OLD','FORMATTED','READONLY',' ',ios)
  CALL opnerr(lfnerr,lfnloc,ios,psdfil,srName)
  IF (ios /= 0) RETURN

  FORMAT100 = '(1X,A4,4X,A9,1X,I2,1X,I3,1X,I5,1X,A1,1X,I1)'
  FORMAT110 = '(32X,A1,1X,I1)'
  FORMAT101 = '(1X,A4,4X,A9,1X,I2,1X,I3,1X,I5,1X,A1,1X,I1,' // &
              '1X,f7.2,1X,f7.4)'
  FORMAT111 = '(32X,A1,1X,I1,1X,f7.2,1X,f7.4)'
  FORMAT102 = '(1X,A4,4X,A9,1X,I2,1X,I3,1X,I5,1X,A1,1X,I1,' // &
              '2(1X,f7.2,1X,f7.4))'
  FORMAT112 = '(32X,A1,1X,I1,2(1X,f7.2,1X,f7.4))'

  loop1: DO
    READ (lfnloc,'(A)',end=90) line1
    READ (lfnloc,'(A)',end=90) line2
    READ (lfnloc,'(A)',end=90) line3
    READ (line1,FORMAT100,iostat=ios1) namep,DOMESP,iyr,iday,isec,compe,mode
    READ (line2,FORMAT110,iostat=ios2) compn,modn
    READ (line3,FORMAT110,iostat=ios3) compu,modu

    IF ( ios1+ios2+ios3 /= 0 ) THEN
90    CLOSE(lfnloc)
      RETURN
    ENDIF

    IF ( mode == 1 .OR. mode == 2 ) THEN
      READ (line1,FORMAT101,iostat=ios1) namep,DOMESP,iyr,iday,isec,compe,mode, &
                                     ae1,te1
      ae2 = 0d0
      te2 = 0d0
    ELSE
      READ (line1,FORMAT102,iostat=ios1) namep,DOMESP,iyr,iday,isec,compe,mode, &
                                     ae1,te1,ae2,te2
    ENDIF

    IF ( modn == 1 .OR. modn == 2 ) THEN
      READ (line2,FORMAT111,iostat=ios2) compn,modn,an1,tn1
      an2 = 0d0
      tn2 = 0d0
    ELSE
      READ (line2,FORMAT112,iostat=ios2) compn,modn,an1,tn1,an2,tn2
    ENDIF

    IF ( modu == 1 .OR. modu == 2 ) THEN
      READ (line3,FORMAT111,iostat=ios3) compu,modu,au1,tu1
      au2 = 0d0
      tu2 = 0d0
    ELSE
      READ (line3,FORMAT112,iostat=ios3) compu,modu,au1,tu1,au2,tu2
    ENDIF

    IF ( ios1+ios2+ios3 /= 0 ) THEN
      CLOSE(lfnloc)
      RETURN
    ENDIF

    tsec = DBLE(isec)/(86400.d0)

    Rmjd = DJUL(iyear4(iyr),1,1d0)-1d0 + DBLE(iday)
    mjdq = Rmjd + tsec

    stNameP = namep // ' ' // DOMESP
    call upperc(stNameP)

    IF (stNameP(6:14) == staNam(6:14) .AND. epoch > mjdq) THEN

      readDat%nPSD = readDat%nPSD + 1

      CALL dimtst(1,2,2,srName,'maxPsd','PSD corrections per station', &
                'Increase "maxPsd" in module "D_PSDFIL.f90".',         &
                readDat%nPSD,maxPsd,irc)

      readDat%psdRec(readDat%nPSD)%epoch       = mjdq

      readDat%psdRec(readDat%nPSD)%type(1)     = modn
      readDat%psdRec(readDat%nPSD)%psdCor(1,1) = an1 / 1000d0
      readDat%psdRec(readDat%nPSD)%psdCor(1,2) = tn1
      readDat%psdRec(readDat%nPSD)%psdCor(1,3) = an2 / 1000d0
      readDat%psdRec(readDat%nPSD)%psdCor(1,4) = tn2

      readDat%psdRec(readDat%nPSD)%type(2)     = mode
      readDat%psdRec(readDat%nPSD)%psdCor(2,1) = ae1 / 1000d0
      readDat%psdRec(readDat%nPSD)%psdCor(2,2) = te1
      readDat%psdRec(readDat%nPSD)%psdCor(2,3) = ae2 / 1000d0
      readDat%psdRec(readDat%nPSD)%psdCor(2,4) = te2

      readDat%psdRec(readDat%nPSD)%type(3)     = modu
      readDat%psdRec(readDat%nPSD)%psdCor(3,1) = au1 / 1000d0
      readDat%psdRec(readDat%nPSD)%psdCor(3,2) = tu1
      readDat%psdRec(readDat%nPSD)%psdCor(3,3) = au2 / 1000d0
      readDat%psdRec(readDat%nPSD)%psdCor(3,4) = tu2

    ENDIF
  ENDDO loop1

  RETURN
  END FUNCTION readDat



! -------------------------------------------------------------------------
! Reading the SNX file format
! -------------------------------------------------------------------------
  FUNCTION readSNX(staNam,epoch)

  USE s_opnfil
  USE s_opnerr
  USE f_djul
  USE f_iyear4
  USE s_upperc
  USE s_dimtst

! Parameters
  ! in:
  CHARACTER(LEN=staNameLength)       :: staNam  ! station name
  REAL(r8b)                          :: epoch   ! epoch of the coordinates to
                                                ! be corrected in MJD

  ! out:
  TYPE(t_psdSta)                     :: readSnx ! Strcuture with all relevant
                                                ! PSD corrections
! Local Parameters
  CHARACTER(LEN=16), PARAMETER :: srName = 'd_psdFil:readSnx'

! Local variables
  CHARACTER(LEN=shortLineLength)     :: line
  CHARACTER(LEN=staNamelength)       :: stNameP
  CHARACTER(LEN=9)                   :: domesp
  CHARACTER(LEN=4)                   :: namep
  CHARACTER(LEN=6)                   :: type
  CHARACTER(LEN=7)                   :: nameSite

  REAL(r8b)                          :: tsec
  REAL(r8b)                          :: mjdq,rmjd
  REAL(r8b)                          :: value

  INTEGER(i4b)                       :: iyr, iday, isec
  INTEGER(i4b)                       :: iSection
  INTEGER(i4b)                       :: iPsd
  INTEGER(i4b)                       :: iCor, jCor
  INTEGER(i4b)                       :: ii
  INTEGER(i4b)                       :: ios, irc


! Initialize the structure
! ------------------------
  readSnx%nPSD   = 0
  readSnx%staNam = staNam
  nameSite       = ''

! Open PSD file
! -------------
  CALL opnfil(lfnloc,psdfil,'OLD','FORMATTED','READONLY',' ',ios)
  CALL opnerr(lfnerr,lfnloc,ios,psdfil,srName)
  IF (ios /= 0) RETURN

  iSection = 0

  ! Read through the file
  Loop1: DO
    READ(lfnloc,'(A)',iostat=ios) line

    IF ( ios /= 0 ) THEN
      CLOSE(lfnloc)
      RETURN
    ENDIF

    ! Find the section with the SITE IDs
    IF ( iSection == 0 .AND. line(1:8) == '+SITE/ID' ) iSection = 1

    ! Check which station is relevant (site code and point id)
    IF ( iSection == 1 ) THEN
      namep  = line(2:5)
      domesp = line(10:18)

      stNameP = namep // ' ' // DOMESP
      call upperc(stNameP)

      IF (stNameP(6:14) == staNam(6:14)) THEN
        nameSite = line(2:8)
        iSection = 0
      ENDIF
    ENDIF

    ! Find the section with the SOLUTION/ESTIMATE
    IF ( iSection == 0 .AND. line(1:18) == '+SOLUTION/ESTIMATE' ) iSection = 2
    IF ( iSection == 2 .AND. line(1:18) == '-SOLUTION/ESTIMATE' ) THEN
      CLOSE(lfnloc)
      RETURN
    ENDIF

    ! Relevant record found
    IF ( iSection == 2 .AND. line(15:21) == nameSite ) THEN

      READ(line(28:29),*,iostat = ios) iyr
      READ(line(31:33),*,iostat = ios) iday
      READ(line(35:39),*,iostat = ios) isec

      tsec = DBLE(isec)/(86400.d0)

      Rmjd = DJUL(iyear4(iyr),1,1d0)-1d0 + DBLE(iday)
      mjdq = Rmjd + tsec

      IF ( mjdq < epoch ) THEN
        iPsd = 0
        DO ii = 1,readSnx%nPsd
          IF ( NINT((readSnx%psdRec(ii)%epoch - mjdq) * 86400d0) == 0 ) THEN
            iPsd = ii
            EXIT
          ENDIF
        ENDDO

        type = line(8:13)
        CALL upperc(type)
        READ(line(48:68),*,iostat=ios) value

        IF ( iPsd == 0 ) THEN
          readSnx%nPsd = readSnx%nPsd + 1
          CALL dimtst(1,2,2,srName,'maxPsd','PSD corrections per station', &
                    'Increase "maxPsd" in module "D_PSDFIL.f90".',         &
                    readSnx%nPSD,maxPsd,irc)

          iPsd = readSnx%nPsd
          readSnx%psdRec(iPsd)%epoch  = mjdq
          readSnx%psdRec(iPsd)%type   = 0
          readSnx%psdRec(iPsd)%psdCor = 0d0
        ENDIF

        iCor = 0
        IF ( type(6:6) == 'N' ) iCor = 1
        IF ( type(6:6) == 'E' ) iCor = 2
        IF ( type(6:6) == 'U' ) iCor = 3
        IF ( type(6:6) == 'H' ) iCor = 3

        jCor = 0
        IF ( type(1:1) == 'A' ) jCor = 1
        IF ( type(1:1) == 'T' ) jCor = 2


        ! Logaritmic
        IF ( type(2:4) == 'LOG') THEN

          ! first value into the record:
          IF ( readSnx%psdRec(iPsd)%type(iCor) == 0 ) THEN
            readSnx%psdRec(iPsd)%type(iCor) = 1
            readSnx%psdRec(iPsd)%psdCor(iCor,jCor) = value

          ! second value into the record
          ELSEIF ( ( readSnx%psdRec(iPsd)%type(iCor) == 1 .OR.    &
                     readSnx%psdRec(iPsd)%type(iCor) == 3 ) .AND. &
                   readSnx%psdRec(iPsd)%psdCor(iCor,jCor) == 0d0 ) THEN
            readSnx%psdRec(iPsd)%psdCor(iCor,jCor) = value

          ! an exponential record already available
          ELSEIF ( readSnx%psdRec(iPsd)%type(iCor) == 2 ) THEN
            readSnx%psdRec(iPsd)%psdCor(iCor,3:4) = readSnx%psdRec(iPsd)%psdCor(iCor,1:2)
            readSnx%psdRec(iPsd)%psdCor(iCor,1:2) = 0d0
            readSnx%psdRec(iPsd)%type(iCor) = 3
            readSnx%psdRec(iPsd)%psdCor(iCor,jCor) = value

          ENDIF
        ENDIF ! end of logarithmic

        ! Exponential
        IF ( type(2:4) == 'EXP') THEN

          ! first value into the record:
          IF ( readSnx%psdRec(iPsd)%type(iCor) == 0 ) THEN
            readSnx%psdRec(iPsd)%type(iCor) = 2
            readSnx%psdRec(iPsd)%psdCor(iCor,jCor) = value

          ! second value into the record
          ELSEIF ( readSnx%psdRec(iPsd)%type(iCor) == 2 .AND. &
                   readSnx%psdRec(iPsd)%psdCor(iCor,jCor) == 0d0 ) THEN
            readSnx%psdRec(iPsd)%psdCor(iCor,jCor) = value

          ! a logarithmic record already available
          ELSEIF ( readSnx%psdRec(iPsd)%type(iCor) == 1 ) THEN
            readSnx%psdRec(iPsd)%type(iCor) = 3
            readSnx%psdRec(iPsd)%psdCor(iCor,2+jCor) = value

          ! second value into the record for log+exp
          ELSEIF ( readSnx%psdRec(iPsd)%type(iCor) == 3 .AND. &
                   readSnx%psdRec(iPsd)%psdCor(iCor,2+jCor) == 0d0 ) THEN
            readSnx%psdRec(iPsd)%psdCor(iCor,2+jCor) = value

          ! an exponential record already available
          ELSEIF ( readSnx%psdRec(iPsd)%type(iCor) == 2 ) THEN
            readSnx%psdRec(iPsd)%type(iCor) = 4
            readSnx%psdRec(iPsd)%psdCor(iCor,2+jCor) = value

          ! second value into the record for exp+exp
          ELSEIF ( readSnx%psdRec(iPsd)%type(iCor) == 4 .AND. &
                   readSnx%psdRec(iPsd)%psdCor(iCor,2+jCor) == 0d0 ) THEN
            readSnx%psdRec(iPsd)%psdCor(iCor,2+jCor) = value

          ENDIF

        ENDIF ! end of exponential

      ENDIF ! check for epoch

    ENDIF ! check for station


  ENDDO loop1

  RETURN
  END FUNCTION readSnx

END MODULE d_psdFil
