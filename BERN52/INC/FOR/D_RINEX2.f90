
! -------------------------------------------------------------------------
! Bernese GNSS Software Version 5.2
! -------------------------------------------------------------------------

MODULE d_rinex2

! -------------------------------------------------------------------------
! Purpose:    This module defines global variables for RINEX 2 files
!
! Author:     R. Dach
!
! Created:    04-Sep-2013
!
! Changes:
!
! Copyright:  Astronomical Institute
!             University of Bern
!             Switzerland
! -------------------------------------------------------------------------

! MAXCOM: Maximum number of comment lines

  USE m_bern,   ONLY: i4b

  IMPLICIT NONE

  INTEGER(i4b),PARAMETER  :: maxcom=300 ! Maximum number of comment lines

END MODULE d_rinex2
