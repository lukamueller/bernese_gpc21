
#ifndef ERRORMSG_H
#define ERRORMSG_H

#include <qstring.h>

void errormsg(const QString& message, bool forceMonospace = false);

#endif

