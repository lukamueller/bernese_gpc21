/****************************************************************************
** Meta object code from reading C++ file 'datedial.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../datedial.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'datedial.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_t_datedial[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x05,
      25,   11,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
      38,   11,   11,   11, 0x08,
      49,   11,   11,   11, 0x08,
      61,   11,   11,   11, 0x08,
      70,   11,   11,   11, 0x08,
      80,   11,   11,   11, 0x08,
      88,   11,   11,   11, 0x08,
      98,   11,   11,   11, 0x08,
     108,   11,   11,   11, 0x08,
     117,   11,   11,   11, 0x08,
     126,   11,   11,   11, 0x08,
     135,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_t_datedial[] = {
    "t_datedial\0\0dateAccept()\0dateClosed()\0"
    "slotPlus()\0slotMinus()\0slotOK()\0"
    "compute()\0today()\0slotHlp()\0slotSet()\0"
    "setYMD()\0setMJD()\0setGPS()\0setDOY()\0"
};

void t_datedial::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        t_datedial *_t = static_cast<t_datedial *>(_o);
        switch (_id) {
        case 0: _t->dateAccept(); break;
        case 1: _t->dateClosed(); break;
        case 2: _t->slotPlus(); break;
        case 3: _t->slotMinus(); break;
        case 4: _t->slotOK(); break;
        case 5: _t->compute(); break;
        case 6: _t->today(); break;
        case 7: _t->slotHlp(); break;
        case 8: _t->slotSet(); break;
        case 9: _t->setYMD(); break;
        case 10: _t->setMJD(); break;
        case 11: _t->setGPS(); break;
        case 12: _t->setDOY(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData t_datedial::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject t_datedial::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_t_datedial,
      qt_meta_data_t_datedial, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &t_datedial::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *t_datedial::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *t_datedial::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_t_datedial))
        return static_cast<void*>(const_cast< t_datedial*>(this));
    return QDialog::qt_metacast(_clname);
}

int t_datedial::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    }
    return _id;
}

// SIGNAL 0
void t_datedial::dateAccept()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void t_datedial::dateClosed()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
QT_END_MOC_NAMESPACE
