/****************************************************************************
** Meta object code from reading C++ file 'r_dir.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../r_dir.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'r_dir.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_r_dir[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      17,    7,    6,    6, 0x08,
      69,   58,    6,    6, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_r_dir[] = {
    "r_dir\0\0buffer,op\0"
    "slotData(QByteArray,Q3NetworkOperation*)\0"
    "urlInfo,op\0"
    "slotNewChildren(Q3ValueList<QUrlInfo>,Q3NetworkOperation*)\0"
};

void r_dir::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        r_dir *_t = static_cast<r_dir *>(_o);
        switch (_id) {
        case 0: _t->slotData((*reinterpret_cast< const QByteArray(*)>(_a[1])),(*reinterpret_cast< Q3NetworkOperation*(*)>(_a[2]))); break;
        case 1: _t->slotNewChildren((*reinterpret_cast< const Q3ValueList<QUrlInfo>(*)>(_a[1])),(*reinterpret_cast< Q3NetworkOperation*(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData r_dir::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject r_dir::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_r_dir,
      qt_meta_data_r_dir, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &r_dir::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *r_dir::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *r_dir::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_r_dir))
        return static_cast<void*>(const_cast< r_dir*>(this));
    return QObject::qt_metacast(_clname);
}

int r_dir::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
