/****************************************************************************
** Meta object code from reading C++ file 'mainwin.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../mainwin.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwin.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_t_mainwin[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      21,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   10,   11,   10, 0x0a,
      24,   10,   10,   10, 0x0a,
      38,   10,   10,   10, 0x09,
      52,   10,   10,   10, 0x09,
      64,   10,   10,   10, 0x09,
      79,   10,   10,   10, 0x09,
      95,   10,   10,   10, 0x09,
     111,   10,   10,   10, 0x09,
     128,   10,  124,   10, 0x09,
     147,   10,  124,   10, 0x09,
     164,   10,   10,   10, 0x09,
     174,   10,   11,   10, 0x09,
     189,   10,   11,   10, 0x09,
     210,   10,   10,   10, 0x09,
     232,   10,   10,   10, 0x09,
     249,   10,   10,   10, 0x09,
     261,   10,   10,   10, 0x09,
     279,   10,   10,   10, 0x09,
     298,   10,   10,   10, 0x09,
     315,   10,   10,   10, 0x09,
     332,   10,   10,   10, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_t_mainwin[] = {
    "t_mainwin\0\0bool\0close()\0slotMenu(int)\0"
    "slotHelp(int)\0slotAbout()\0slotTopPanel()\0"
    "slotNextPanel()\0slotPrevPanel()\0"
    "slotCancel()\0int\0slotSaveAsPanels()\0"
    "slotSavePanels()\0slotRun()\0isFirstPanel()\0"
    "isLastVisiblePanel()\0slotSetActionsOnOff()\0"
    "slotLastOutput()\0slotRerun()\0"
    "slotPlusSession()\0slotMinusSession()\0"
    "slotDateAccept()\0slotDateClosed()\0"
    "slotMessageChanged(QString)\0"
};

void t_mainwin::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        t_mainwin *_t = static_cast<t_mainwin *>(_o);
        switch (_id) {
        case 0: { bool _r = _t->close();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 1: _t->slotMenu((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->slotHelp((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->slotAbout(); break;
        case 4: _t->slotTopPanel(); break;
        case 5: _t->slotNextPanel(); break;
        case 6: _t->slotPrevPanel(); break;
        case 7: _t->slotCancel(); break;
        case 8: { int _r = _t->slotSaveAsPanels();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 9: { int _r = _t->slotSavePanels();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 10: _t->slotRun(); break;
        case 11: { bool _r = _t->isFirstPanel();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 12: { bool _r = _t->isLastVisiblePanel();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 13: _t->slotSetActionsOnOff(); break;
        case 14: _t->slotLastOutput(); break;
        case 15: _t->slotRerun(); break;
        case 16: _t->slotPlusSession(); break;
        case 17: _t->slotMinusSession(); break;
        case 18: _t->slotDateAccept(); break;
        case 19: _t->slotDateClosed(); break;
        case 20: _t->slotMessageChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData t_mainwin::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject t_mainwin::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_t_mainwin,
      qt_meta_data_t_mainwin, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &t_mainwin::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *t_mainwin::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *t_mainwin::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_t_mainwin))
        return static_cast<void*>(const_cast< t_mainwin*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int t_mainwin::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    }
    return _id;
}
static const uint qt_meta_data_t_myaction[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_t_myaction[] = {
    "t_myaction\0\0myTriggered()\0"
};

void t_myaction::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        t_myaction *_t = static_cast<t_myaction *>(_o);
        switch (_id) {
        case 0: _t->myTriggered(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData t_myaction::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject t_myaction::staticMetaObject = {
    { &QAction::staticMetaObject, qt_meta_stringdata_t_myaction,
      qt_meta_data_t_myaction, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &t_myaction::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *t_myaction::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *t_myaction::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_t_myaction))
        return static_cast<void*>(const_cast< t_myaction*>(this));
    return QAction::qt_metacast(_clname);
}

int t_myaction::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAction::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
static const uint qt_meta_data_t_mymenu[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_t_mymenu[] = {
    "t_mymenu\0"
};

void t_mymenu::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData t_mymenu::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject t_mymenu::staticMetaObject = {
    { &QMenu::staticMetaObject, qt_meta_stringdata_t_mymenu,
      qt_meta_data_t_mymenu, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &t_mymenu::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *t_mymenu::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *t_mymenu::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_t_mymenu))
        return static_cast<void*>(const_cast< t_mymenu*>(this));
    return QMenu::qt_metacast(_clname);
}

int t_mymenu::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMenu::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
