/****************************************************************************
** Meta object code from reading C++ file 'server.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../server.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'server.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_t_server[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      18,   10,    9,    9, 0x05,
      57,   51,    9,    9, 0x05,

 // slots: signature, parameters, type, tag, flags
      84,   51,    9,    9, 0x0a,
     112,    9,    9,    9, 0x0a,
     128,    9,    9,    9, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_t_server[] = {
    "t_server\0\0session\0serverFinished(const t_session*)\0"
    "inMsg\0singletonFinished(QString)\0"
    "slotScriptFinished(QString)\0slotStartTail()\0"
    "slotStartScripts()\0"
};

void t_server::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        t_server *_t = static_cast<t_server *>(_o);
        switch (_id) {
        case 0: _t->serverFinished((*reinterpret_cast< const t_session*(*)>(_a[1]))); break;
        case 1: _t->singletonFinished((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->slotScriptFinished((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->slotStartTail(); break;
        case 4: _t->slotStartScripts(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData t_server::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject t_server::staticMetaObject = {
    { &Q3ServerSocket::staticMetaObject, qt_meta_stringdata_t_server,
      qt_meta_data_t_server, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &t_server::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *t_server::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *t_server::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_t_server))
        return static_cast<void*>(const_cast< t_server*>(this));
    return Q3ServerSocket::qt_metacast(_clname);
}

int t_server::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Q3ServerSocket::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void t_server::serverFinished(const t_session * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void t_server::singletonFinished(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
