/****************************************************************************
** Meta object code from reading C++ file 'keyword.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../keyword.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'keyword.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_t_keyword[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x05,

 // slots: signature, parameters, type, tag, flags
      34,   10,   10,   10, 0x08,
      66,   58,   10,   10, 0x08,
      98,   94,   10,   10, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_t_keyword[] = {
    "t_keyword\0\0keyChanged(t_keyword*)\0"
    "slotThisKeyMayChanged()\0menuaux\0"
    "slotThisKeyMayChanged(bool)\0key\0"
    "slotOtherKeyChanged(t_keyword*)\0"
};

void t_keyword::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        t_keyword *_t = static_cast<t_keyword *>(_o);
        switch (_id) {
        case 0: _t->keyChanged((*reinterpret_cast< t_keyword*(*)>(_a[1]))); break;
        case 1: _t->slotThisKeyMayChanged(); break;
        case 2: _t->slotThisKeyMayChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->slotOtherKeyChanged((*reinterpret_cast< t_keyword*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData t_keyword::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject t_keyword::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_t_keyword,
      qt_meta_data_t_keyword, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &t_keyword::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *t_keyword::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *t_keyword::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_t_keyword))
        return static_cast<void*>(const_cast< t_keyword*>(this));
    return QObject::qt_metacast(_clname);
}

int t_keyword::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void t_keyword::keyChanged(t_keyword * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
