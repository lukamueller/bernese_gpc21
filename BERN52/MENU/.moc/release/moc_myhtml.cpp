/****************************************************************************
** Meta object code from reading C++ file 'myhtml.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../myhtml.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'myhtml.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_t_myhtml[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      14,   10,    9,    9, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_t_myhtml[] = {
    "t_myhtml\0\0src\0slotSourceChanged(QUrl)\0"
};

void t_myhtml::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        t_myhtml *_t = static_cast<t_myhtml *>(_o);
        switch (_id) {
        case 0: _t->slotSourceChanged((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData t_myhtml::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject t_myhtml::staticMetaObject = {
    { &QTextBrowser::staticMetaObject, qt_meta_stringdata_t_myhtml,
      qt_meta_data_t_myhtml, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &t_myhtml::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *t_myhtml::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *t_myhtml::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_t_myhtml))
        return static_cast<void*>(const_cast< t_myhtml*>(this));
    return QTextBrowser::qt_metacast(_clname);
}

int t_myhtml::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QTextBrowser::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
