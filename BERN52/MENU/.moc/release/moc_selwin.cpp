/****************************************************************************
** Meta object code from reading C++ file 'selwin.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../selwin.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'selwin.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_t_selwin[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x05,

 // slots: signature, parameters, type, tag, flags
      20,    9,    9,    9, 0x08,
      35,    9,    9,    9, 0x08,
      57,    9,    9,    9, 0x08,
      75,    9,    9,    9, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_t_selwin[] = {
    "t_selwin\0\0changed()\0slotEvalList()\0"
    "slotEvalListSeldial()\0slotTextChanged()\0"
    "slotTimerTimeout()\0"
};

void t_selwin::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        t_selwin *_t = static_cast<t_selwin *>(_o);
        switch (_id) {
        case 0: _t->changed(); break;
        case 1: _t->slotEvalList(); break;
        case 2: _t->slotEvalListSeldial(); break;
        case 3: _t->slotTextChanged(); break;
        case 4: _t->slotTimerTimeout(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData t_selwin::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject t_selwin::staticMetaObject = {
    { &QLineEdit::staticMetaObject, qt_meta_stringdata_t_selwin,
      qt_meta_data_t_selwin, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &t_selwin::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *t_selwin::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *t_selwin::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_t_selwin))
        return static_cast<void*>(const_cast< t_selwin*>(this));
    return QLineEdit::qt_metacast(_clname);
}

int t_selwin::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QLineEdit::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void t_selwin::changed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
