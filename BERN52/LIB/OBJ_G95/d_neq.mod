G95 module created on Thu Jun 25 17:43:12 2020 from ./.fpp/D_NEQ.f90
If you edit this, you'll get what you deserve.
module-version 9
(() () () () () () () () () () () () () () () () () () () () ())

()

()

()

()

(2 't_time' 'm_time' 1 ((DERIVED UNKNOWN UNKNOWN UNKNOWN NONE NONE
SEQUENCE) (UNKNOWN) 0 0 () () () '' ((3 'mean' (REAL 8) () () 0 0 0 ())
(4 'half' (REAL 8) () () 0 0 0 ())) PUBLIC ())
5 'd_grid' 'd_grid' 1 ((MODULE UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' () ())
6 'd_neq' 'd_neq' 1 ((MODULE UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' () ())
7 'd_par' 'd_par' 1 ((MODULE UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' () ())
8 'filenamelength' 'm_bern' 1 ((PARAMETER UNKNOWN UNKNOWN UNKNOWN NONE
NONE) (INTEGER 4) 0 0 () (CONSTANT (INTEGER 4) 0 '32') () () '' () ())
9 'i4b' 'm_bern' 1 ((PARAMETER UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
INTEGER 4) 0 0 () (CONSTANT (INTEGER 4) 0 '4') () () '' () ())
10 'm_bern' 'm_bern' 1 ((MODULE UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' () ())
11 'm_global' 'm_global' 1 ((MODULE UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' () ())
12 'm_maxdim' 'm_maxdim' 1 ((MODULE UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' () ())
13 'm_time' 'm_time' 1 ((MODULE UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' () ())
14 'maxfrq' 'd_neq' 1 ((PARAMETER UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
INTEGER 4) 0 0 () (CONSTANT (INTEGER 4) 0 '2') () () '' () ())
15 'maxobst' 'd_neq' 1 ((PARAMETER UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
INTEGER 4) 0 0 () (CONSTANT (INTEGER 4) 0 '30000') () () '' () ())
16 'maxoff' 'd_neq' 1 ((PARAMETER UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
INTEGER 4) 0 0 () (CONSTANT (INTEGER 4) 0 '260') () () '' () ())
17 'maxsat' 'm_maxdim' 1 ((PARAMETER UNKNOWN UNKNOWN UNKNOWN NONE NONE)
(INTEGER 4) 0 0 () (CONSTANT (INTEGER 4) 0 '130') () () '' () ())
18 'maxstasin' 'd_neq' 1 ((PARAMETER UNKNOWN UNKNOWN UNKNOWN NONE NONE)
(INTEGER 4) 0 0 () (CONSTANT (INTEGER 4) 0 '2000') () () '' () ())
19 'maxsys' 'm_global' 1 ((PARAMETER UNKNOWN UNKNOWN UNKNOWN NONE NONE)
(INTEGER 4) 0 0 () (CONSTANT (INTEGER 4) 0 '6') () () '' () ())
20 'neqcurrentversion' 'd_neq' 1 ((PARAMETER UNKNOWN UNKNOWN UNKNOWN
NONE NONE) (INTEGER 4) 0 0 () (CONSTANT (INTEGER 4) 0 '8') () () '' () ())
21 'r8b' 'm_bern' 1 ((PARAMETER UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
INTEGER 4) 0 0 () (CONSTANT (INTEGER 4) 0 '8') () () '' () ())
22 'readrec' 'd_neq' 1 ((PROCEDURE UNKNOWN MODULE-PROC DECL NONE NONE
SUBROUTINE) (PROCEDURE 0) 0 0 (23 NONE 24 NONE 25 NONE) () () '' () ())
26 't_misc' 'd_neq' 1 ((DERIVED UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' ((27 'title' (CHARACTER 1 ((CONSTANT (INTEGER 4)
0 '132'))) (1 EXPLICIT (CONSTANT (INTEGER 4) 0 '1') (CONSTANT (INTEGER 4)
0 '2')) () 1 0 0 ()) (28 'datum' (CHARACTER 1 ((CONSTANT (INTEGER 4) 0
'16'))) () () 0 0 0 ()) (29 'nutmod' (CHARACTER 1 ((CONSTANT (INTEGER 4)
0 '16'))) () () 0 0 0 ()) (30 'submod' (CHARACTER 1 ((CONSTANT (INTEGER
4) 0 '16'))) () () 0 0 0 ()) (31 'orbfil' (CHARACTER 1 ((CONSTANT (
INTEGER 4) 0 '32'))) (1 EXPLICIT (CONSTANT (INTEGER 4) 0 '1') (CONSTANT
(INTEGER 4) 0 '2')) () 1 0 0 ()) (32 'gravfil' (CHARACTER 1 ((CONSTANT (
INTEGER 4) 0 '32'))) () () 0 0 0 ()) (33 'nobs' (REAL 8) () () 0 0 0 ())
(34 'npar' (INTEGER 4) () () 0 0 0 ()) (35 'ltpl' (REAL 8) () () 0 0 0 ())
(36 'nparms' (REAL 8) () () 0 0 0 ()) (37 'npseu' (INTEGER 4) () () 0 0
0 ()) (38 'npseuel' (INTEGER 4) () () 0 0 0 ()) (39 'nftot' (INTEGER 4)
() () 0 0 0 ()) (40 'nsmpnq' (INTEGER 4) () () 0 0 0 ()) (41 'ielvnq' (
INTEGER 4) () () 0 0 0 ()) (42 'itropo' (INTEGER 4) () () 0 0 0 ()) (43
'iextra' (INTEGER 4) () () 0 0 0 ()) (44 'itrmap' (INTEGER 4) () () 0 0
0 ()) (45 'itrgrd' (INTEGER 4) () () 0 0 0 ()) (46 'nanoff' (INTEGER 4)
() () 0 0 0 ()) (47 'nsaoff' (INTEGER 4) (1 EXPLICIT (CONSTANT (INTEGER
4) 0 '1') (CONSTANT (INTEGER 4) 0 '260')) () 1 0 0 ()) (48 'satoff' (
INTEGER 4) (2 EXPLICIT (CONSTANT (INTEGER 4) 0 '1') (CONSTANT (INTEGER 4)
0 '130') (CONSTANT (INTEGER 4) 0 '1') (CONSTANT (INTEGER 4) 0 '260')) ()
1 0 0 ()) (49 'nstat_sinex' (INTEGER 4) () () 0 0 0 ()) (50 'sinex' (
DERIVED 51) (1 EXPLICIT (CONSTANT (INTEGER 4) 0 '1') (CONSTANT (INTEGER
4) 0 '2000')) () 1 0 0 ()) (52 'grdneq' (CHARACTER 1 ((CONSTANT (
INTEGER 4) 0 '12'))) (1 EXPLICIT (CONSTANT (INTEGER 4) 0 '1') (CONSTANT
(INTEGER 4) 0 '3')) () 1 0 0 ()) (53 'nobst' (INTEGER 4) () () 0 0 0 ())
(54 'obst' (DERIVED 55) (1 EXPLICIT (CONSTANT (INTEGER 4) 0 '1') (
CONSTANT (INTEGER 4) 0 '30000')) () 1 0 0 ())) PUBLIC ())
56 't_neq' 'd_neq' 1 ((DERIVED UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' ((57 'version' (INTEGER 4) () () 0 0 0 ()) (58
'misc' (DERIVED 26) () () 0 0 0 ()) (59 'par' (DERIVED 60) (1 DEFERRED ()
()) () 1 1 0 ()) (61 'anor' (REAL 8) (1 DEFERRED () ()) () 1 1 0 ()) (
62 'bnor' (REAL 8) (1 DEFERRED () ()) () 1 1 0 ()) (63 'xxx' (REAL 8) (
1 DEFERRED () ()) () 1 1 0 ())) PUBLIC ())
55 't_obst' 'd_neq' 1 ((DERIVED UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' ((64 'obstyp' (CHARACTER 1 ((CONSTANT (INTEGER
4) 0 '3'))) (1 EXPLICIT (CONSTANT (INTEGER 4) 0 '1') (CONSTANT (INTEGER
4) 0 '4')) () 1 0 0 ())) PUBLIC ())
60 't_par' 'd_par' 1 ((DERIVED UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' ((65 'locq' (INTEGER 4) (1 EXPLICIT (CONSTANT (
INTEGER 4) 0 '1') (CONSTANT (INTEGER 4) 0 '7')) () 1 0 0 ()) (66 'name'
(CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '20'))) () () 0 0 0 ()) (67 'time'
(DERIVED 2) () () 0 0 0 ()) (68 'x0' (REAL 8) () () 0 0 0 ()) (69 'scale'
(REAL 8) () () 0 0 0 ()) (70 'techn' (CHARACTER 1 ((CONSTANT (INTEGER 4)
0 '1'))) (1 EXPLICIT (CONSTANT (INTEGER 4) 0 '1') (CONSTANT (INTEGER 4)
0 '2')) () 1 0 0 ()) (71 'obstim' (DERIVED 72) () () 0 0 0 ()) (73 'type'
(CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '2'))) () () 0 0 0 ()) (74 'omega'
(REAL 8) () () 0 0 0 ())) PUBLIC ())
51 't_sinex' 'd_neq' 1 ((DERIVED UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' ((75 'timint' (DERIVED 72) () () 0 0 0 ()) (76
'stname' (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '16'))) () () 0 0 0 ()) (
77 'antecc' (REAL 8) (1 EXPLICIT (CONSTANT (INTEGER 4) 0 '1') (CONSTANT
(INTEGER 4) 0 '3')) () 1 0 0 ()) (78 'antpcv' (DERIVED 79) (1 EXPLICIT (
CONSTANT (INTEGER 4) 0 '0') (CONSTANT (INTEGER 4) 0 '5')) () 1 0 0 ()) (
80 'antnum' (INTEGER 4) () () 0 0 0 ()) (81 'antsta' (CHARACTER 1 ((
CONSTANT (INTEGER 4) 0 '20'))) () () 0 0 0 ()) (82 'antrec' (CHARACTER 1
((CONSTANT (INTEGER 4) 0 '20'))) () () 0 0 0 ())) PUBLIC ())
79 't_sinex_pcv' 'd_neq' 1 ((DERIVED UNKNOWN UNKNOWN UNKNOWN NONE NONE)
(UNKNOWN) 0 0 () () () '' ((83 'nfrq' (INTEGER 4) () () 0 0 0 ()) (84
'antphs' (REAL 8) (2 EXPLICIT (CONSTANT (INTEGER 4) 0 '1') (CONSTANT (
INTEGER 4) 0 '3') (CONSTANT (INTEGER 4) 0 '1') (CONSTANT (INTEGER 4) 0 '2'))
() 1 0 0 ()) (85 'adopted' (INTEGER 4) () () 0 0 0 ()) (86 'individ' (
INTEGER 4) () () 0 0 0 ()) (87 'atxstr' (CHARACTER 1 ((CONSTANT (
INTEGER 4) 0 '10'))) () () 0 0 0 ())) PUBLIC ())
88 't_sinex_v1' 'd_neq' 1 ((DERIVED UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' ((89 'timint' (DERIVED 72) () () 0 0 0 ()) (90
'phasecc' (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '32'))) () () 0 0 0 ())
(91 'stname' (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '16'))) () () 0 0 0 ())
(92 'antphs' (REAL 8) (2 EXPLICIT (CONSTANT (INTEGER 4) 0 '1') (
CONSTANT (INTEGER 4) 0 '3') (CONSTANT (INTEGER 4) 0 '1') (CONSTANT (
INTEGER 4) 0 '2')) () 1 0 0 ()) (93 'antfrq' (INTEGER 4) () () 0 0 0 ())
(94 'antecc' (REAL 8) (1 EXPLICIT (CONSTANT (INTEGER 4) 0 '1') (
CONSTANT (INTEGER 4) 0 '3')) () 1 0 0 ()) (95 'antnum' (INTEGER 4) () ()
0 0 0 ()) (96 'antsta' (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '16'))) ()
() 0 0 0 ()) (97 'antrec' (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '16')))
() () 0 0 0 ())) PUBLIC ())
98 't_sinex_v2' 'd_neq' 1 ((DERIVED UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' ((99 'timint' (DERIVED 72) () () 0 0 0 ()) (
100 'phasecc' (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '32'))) () () 0 0 0
()) (101 'stname' (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '16'))) () () 0
0 0 ()) (102 'antphs' (REAL 8) (2 EXPLICIT (CONSTANT (INTEGER 4) 0 '1')
(CONSTANT (INTEGER 4) 0 '3') (CONSTANT (INTEGER 4) 0 '1') (CONSTANT (
INTEGER 4) 0 '2')) () 1 0 0 ()) (103 'antfrq' (INTEGER 4) () () 0 0 0 ())
(104 'antecc' (REAL 8) (1 EXPLICIT (CONSTANT (INTEGER 4) 0 '1') (
CONSTANT (INTEGER 4) 0 '3')) () 1 0 0 ()) (105 'antnum' (INTEGER 4) () ()
0 0 0 ()) (106 'antsta' (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '20'))) ()
() 0 0 0 ()) (107 'antrec' (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '20')))
() () 0 0 0 ())) PUBLIC ())
72 't_timint' 'm_time' 1 ((DERIVED UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' ((108 't' (REAL 8) (1 EXPLICIT (CONSTANT (
INTEGER 4) 0 '1') (CONSTANT (INTEGER 4) 0 '2')) () 1 0 0 ())) PUBLIC ())
109 'typlen' 'd_grid' 1 ((PARAMETER UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
INTEGER 4) 0 0 () (CONSTANT (INTEGER 4) 0 '12') () () '' () ())
110 'writerec' 'd_neq' 1 ((PROCEDURE UNKNOWN MODULE-PROC DECL NONE NONE
SUBROUTINE) (PROCEDURE 0) 0 0 (111 NONE 112 NONE 113 NONE) () () '' () ())
113 'rec' '' 114 ((VARIABLE UNKNOWN UNKNOWN UNKNOWN NONE NONE DIMENSION
DUMMY) (REAL 8) 0 0 () (1 ASSUMED_SHAPE () ()) () '' () ())
112 'dim' '' 114 ((VARIABLE UNKNOWN UNKNOWN UNKNOWN NONE NONE DUMMY) (
INTEGER 4) 0 0 () () () '' () ())
111 'lfn' '' 114 ((VARIABLE UNKNOWN UNKNOWN UNKNOWN NONE NONE DUMMY) (
INTEGER 4) 0 0 () () () '' () ())
79 't_sinex_pcv' 'd_neq' 1 ((DERIVED UNKNOWN UNKNOWN UNKNOWN NONE NONE)
(UNKNOWN) 0 0 () () () '' ((83 'nfrq' (INTEGER 4) () () 0 0 0 ()) (84
'antphs' (REAL 8) (2 EXPLICIT (CONSTANT (INTEGER 4) 0 '1') (CONSTANT (
INTEGER 4) 0 '3') (CONSTANT (INTEGER 4) 0 '1') (CONSTANT (INTEGER 4) 0 '2'))
() 1 0 0 ()) (85 'adopted' (INTEGER 4) () () 0 0 0 ()) (86 'individ' (
INTEGER 4) () () 0 0 0 ()) (87 'atxstr' (CHARACTER 1 ((CONSTANT (
INTEGER 4) 0 '10'))) () () 0 0 0 ())) PUBLIC ())
72 't_timint' 'm_time' 1 ((DERIVED UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' ((108 't' (REAL 8) (1 EXPLICIT (CONSTANT (
INTEGER 4) 0 '1') (CONSTANT (INTEGER 4) 0 '2')) () 1 0 0 ())) PUBLIC ())
60 't_par' 'd_par' 1 ((DERIVED UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' ((65 'locq' (INTEGER 4) (1 EXPLICIT (CONSTANT (
INTEGER 4) 0 '1') (CONSTANT (INTEGER 4) 0 '7')) () 1 0 0 ()) (66 'name'
(CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '20'))) () () 0 0 0 ()) (67 'time'
(DERIVED 2) () () 0 0 0 ()) (68 'x0' (REAL 8) () () 0 0 0 ()) (69 'scale'
(REAL 8) () () 0 0 0 ()) (70 'techn' (CHARACTER 1 ((CONSTANT (INTEGER 4)
0 '1'))) (1 EXPLICIT (CONSTANT (INTEGER 4) 0 '1') (CONSTANT (INTEGER 4)
0 '2')) () 1 0 0 ()) (71 'obstim' (DERIVED 72) () () 0 0 0 ()) (73 'type'
(CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '2'))) () () 0 0 0 ()) (74 'omega'
(REAL 8) () () 0 0 0 ())) PUBLIC ())
55 't_obst' 'd_neq' 1 ((DERIVED UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' ((64 'obstyp' (CHARACTER 1 ((CONSTANT (INTEGER
4) 0 '3'))) (1 EXPLICIT (CONSTANT (INTEGER 4) 0 '1') (CONSTANT (INTEGER
4) 0 '4')) () 1 0 0 ())) PUBLIC ())
51 't_sinex' 'd_neq' 1 ((DERIVED UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' ((75 'timint' (DERIVED 72) () () 0 0 0 ()) (76
'stname' (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '16'))) () () 0 0 0 ()) (
77 'antecc' (REAL 8) (1 EXPLICIT (CONSTANT (INTEGER 4) 0 '1') (CONSTANT
(INTEGER 4) 0 '3')) () 1 0 0 ()) (78 'antpcv' (DERIVED 79) (1 EXPLICIT (
CONSTANT (INTEGER 4) 0 '0') (CONSTANT (INTEGER 4) 0 '5')) () 1 0 0 ()) (
80 'antnum' (INTEGER 4) () () 0 0 0 ()) (81 'antsta' (CHARACTER 1 ((
CONSTANT (INTEGER 4) 0 '20'))) () () 0 0 0 ()) (82 'antrec' (CHARACTER 1
((CONSTANT (INTEGER 4) 0 '20'))) () () 0 0 0 ())) PUBLIC ())
25 'rec' '' 115 ((VARIABLE UNKNOWN UNKNOWN UNKNOWN NONE NONE DIMENSION
DUMMY) (REAL 8) 0 0 () (1 ASSUMED_SHAPE () ()) () '' () ())
24 'dim' '' 115 ((VARIABLE UNKNOWN UNKNOWN UNKNOWN NONE NONE DUMMY) (
INTEGER 4) 0 0 () () () '' () ())
23 'lfn' '' 115 ((VARIABLE UNKNOWN UNKNOWN UNKNOWN NONE NONE DUMMY) (
INTEGER 4) 0 0 () () () '' () ())
)

('d_grid' 0 5 'd_neq' 0 6 'd_par' 0 7 'filenamelength' 0 8 'i4b' 0 9
'm_bern' 0 10 'm_global' 0 11 'm_maxdim' 0 12 'm_time' 0 13 'maxfrq' 0
14 'maxobst' 0 15 'maxoff' 0 16 'maxsat' 0 17 'maxstasin' 0 18 'maxsys'
0 19 'neqcurrentversion' 0 20 'r8b' 0 21 'readrec' 0 22 't_misc' 0 26
't_neq' 0 56 't_obst' 0 55 't_par' 0 60 't_sinex' 0 51 't_sinex_pcv' 0
79 't_sinex_v1' 0 88 't_sinex_v2' 0 98 't_timint' 0 72 'typlen' 0 109
'writerec' 0 110)
