      MODULE f_sigma2
      CONTAINS

C*
      FUNCTION SIGMA2(NPAR,NPARMS,NOBS,PAR,BNOR,FTF,IAMB1,NAMB,SIGAPR)
CC
CC NAME       :  SIGMA2
CC
CC PURPOSE    :  COMPUTE RMS ERROR, USE ONLY NOT SOLVED AMB. PAR.
CC               RMS**2 = (PHI*WEIGHT*PHI - BNOR*PAR)/(NOBS-NPARMS)
CC
CC PARAMETERS :
CC         IN :  NPAR   : NUMBER OF PARAMETERS                I*4
CC               NPARMS : NUMBER OF PARAMETERS TO COMPUTE RMS I*4
CC               NOBS   : NUMBER OF OBSERVATIONS              I*4
CC               PAR(I),I=1,..,NPAR: PARAMETERS               R*8
CC               BNOR(I),I=1,..,NPAR: RIGHT HAND SIDE OF      R*8
CC                        NORMAL EQUATION SYSTEM
CC               FTF    : PHI*WEIGHT*PHI, PHI=OBS-COMP        R*8
CC               IAMB1(I),I=1,..NPAR!!                        I*4
CC                         = 0 PARAMETER ALREADY SOLVED
CC                         = 1 PARAMETER NOT YET SOVLED
CC               NAMB   : NUMBER OF AMBIGUITIES               I*4
CC       OUT  :  SIGMA2 : FUNCTION VALUE                      R*8
CC
CC REMARKS    :  ---
CC
CC AUTHOR     :  L.MERVART
CC
CC VERSION    :  3.4
CC
CC CREATED    :  01-SEP-92
CC
CC CHANGES    :  29-NOV-95 : SS: RETURN "SIGAPR" IF NEGATIVE "RMS"
CC               30-APR-03 : SS: PREVENT CRASH IN CASE OF PROBLEM
CC                               CONCERNING RMS COMPUTATION
CC               21-JUN-05 : MM: COMLFNUM.inc REMOVED, m_bern ADDED
CC               23-JUN-05 : MM: IMPLICIT NONE AND DECLARATIONS ADDED
CC               13-JAN-13 : RD: SOME MESSGES ONLY FOR "AIUB"
CC
CC COPYRIGHT  :  ASTRONOMICAL INSTITUTE
CC      1992     UNIVERSITY OF BERN
CC               SWITZERLAND
CC
C*
C DECLARATION
      USE m_bern
      IMPLICIT NONE
C
C DECLARATIONS INSTEAD OF IMPLICIT
C --------------------------------
      INTEGER*4 IPAR  , NAMB  , NOBS  , NPAR  , NPARMS
C
      REAL*8    FTF   , RMS   , SIGAPR, SIGMA2
C
CCC       IMPLICIT REAL*8 (A-H,O-Z)
C
      REAL*8 BNOR(*),PAR(*)
      INTEGER*4 IAMB1(*)
C
C
C COMPUTE RMS ERROR
      RMS=FTF
      DO 10 IPAR=1,NPAR-NAMB
        RMS=RMS-BNOR(IPAR)*PAR(IPAR)
10    CONTINUE
C
      DO 20 IPAR=NPAR-NAMB+1,NPAR
        IF (IAMB1(IPAR) .EQ. 1)
     1    RMS=RMS-BNOR(IPAR)*PAR(IPAR)
20    CONTINUE
C
      IF (RMS.GT.0 .AND. NOBS-NPARMS.GT.0) THEN
        SIGMA2=DSQRT(RMS/(NOBS-NPARMS))
      ELSE
        IF (NOBS-NPARMS.EQ.0) THEN
          SIGMA2=SIGAPR
          WRITE(LFNERR,901)
901       FORMAT(/,' ### SR SIGMA2: NO REDUNDANCY (DOF=0)',/,
     1         16X,'RMS SET TO APRIORI SIGMA OF OBSERVATION',/)
        ELSEIF (NOBS-NPARMS.LT.0) THEN
          SIGMA2=SIGAPR
          WRITE(LFNERR,902)
902       FORMAT(/,' ### SR SIGMA2: NUMBER OF OBSERVATIONS ',
     1             'SMALLER THAN NUMBER OF UNKNOWNS !',/,
     2         16X,'RMS SET TO APRIORI SIGMA OF OBSERVATION',/)
        ELSEIF (RMS.LT.0) THEN
          SIGMA2=SIGAPR
#ifdef GRP_AIUB
          WRITE(LFNERR,903)
903       FORMAT(/,' ### SR SIGMA2: NUMERICAL PROBLEM IN COMPUTING ',
     1             'THE RMS ERROR BECAUSE',/,
     2         16X,'AMBIGUITY PARAMETERS ARE POORLY INITIALIZED.',/,
     3         16X,'RMS ERROR SET TO A PRIORI SIGMA OF UNIT WEIGHT.',/)
#endif
        ENDIF
      ENDIF
C
      RETURN
      END FUNCTION

      END MODULE
