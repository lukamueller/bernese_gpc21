      MODULE s_GRVSM2
      CONTAINS

C*
      SUBROUTINE GRVSM2(GM,RSAT,RCB,A0)
CC
CC NAME       :  GRVSM2
CC
CC PURPOSE    :  COMPUTE FIRST DERIVATIVE OF FORCE DUE TO A
CC               CELESTIAL BODY ON A EARTH SATELLITE AND STORE THE
CC               RESULT IN MATRIX A0(I,K), I=1,2,3, K=1,2,3.
CC
CC PARAMETERS :
CC         IN :  GM     : GRAVIT. CONSTANT * MASS OF            R*8
CC                        DISTURBING BODY
CC               RSAT   : GEOCENTRIC POSITION OF THE SATELLITE  R*8(*)
CC               RCB    : GEOCENTRIC POSITION (RCB(I),I=1,2,3)  R*8(*)
CC                        OF THE DISTURBING BODY.
CC        OUT :  A0(I,K),I=1,2,3: RESULTING FORCE ACTING ON     R*8(*,*)
CC                        THE SATELLITE
CC
CC REMARKS    :  ---
CC
CC AUTHOR     :  G.BEUTLER
CC
CC VERSION    :  3.6  (OCT 94)
CC
CC CREATED    :  94/10/23
CC
CC CHANGES    :  23-JUN-05 : MM: IMPLICIT NONE AND DECLARATIONS ADDED
CC
CC COPYRIGHT  :  ASTRONOMICAL INSTITUTE
CC      1994     UNIVERSITY OF BERN
CC               SWITZERLAND
CC
C*
      IMPLICIT NONE
C
C DECLARATIONS INSTEAD OF IMPLICIT
C --------------------------------
      INTEGER*4 I     , K
C
      REAL*8    DELTA , DELTA3, GM
C
CCC       IMPLICIT REAL*8 (A-H,O-Z)
      REAL*8 RSAT(*),RCB(*),A0(3,*)
C
      DELTA=0.D0
      DO 10 I=1,3
        DELTA=DELTA+(RSAT(I)-RCB(I))**2
10    CONTINUE
C
      DELTA3=DSQRT(DELTA)**3
      DO 20 I=1,3
        DO 20 K=1,3
        A0(I,K)=+3*GM/DELTA3*(RSAT(I)-RCB(I))*(RSAT(K)-RCB(K))/DELTA
        IF(I.EQ.K)A0(I,K)=A0(I,K)-GM/DELTA3
20    CONTINUE
      RETURN
      END SUBROUTINE

      END MODULE
