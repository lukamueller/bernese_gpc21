      MODULE s_ECCELL
      CONTAINS

C*
      SUBROUTINE ECCELL(XSTELL,XSTECC,LOCECC)
CC
CC NAME       :  ECCELL
CC
CC PURPOSE    :  COMPUTE LOCAL ELLIPSOIDAL ECCENTRICITIES
CC               (NORTH,EAST,UP IN METERS)
CC               STARTING GLOBAL CARTESIAN WGS-84 ECCENTRICITIES
CC               (DX,DY,DZ IN METERS)
CC
CC PARAMETERS :
CC         IN :  XSTELL(I),I=1,2,3: ELLIPSOIDAL COORDINATES   R*8
CC                        OF CENTER POINT (LATITUDE,LONGITUDE,
CC                        HEIGHT IN RADIAN,RADIAN,METERS)
CC               XSTECC(I),I=1,2,3: ECCENTRICITIES IN GLOBAL  R*8
CC                        ELLIPSOIDAL SYSTEM (DX,DY,DZ IN     R*8
CC                        METERS)
CC        OUT :  LOCECC(I),I=1,2,3: LOCAL ELLIPSOIDAL ECCEN-  R*8
CC                        TRICITIES (NORTH,EAST,UP IN METERS)
CC
CC REMARKS    :  ---
CC
CC AUTHOR     :  M.ROTHACHER
CC
CC VERSION    :  3.4  (JAN 93)
CC
CC CREATED    :  87/11/05 15:59
CC
CC CHANGES    :  23-JUN-05 : MM: IMPLICIT NONE AND DECLARATIONS ADDED
CC
CC COPYRIGHT  :  ASTRONOMICAL INSTITUTE
CC      1987     UNIVERSITY OF BERN
CC               SWITZERLAND
CC
C*
      USE s_dmlmav
      IMPLICIT NONE
C
C DECLARATIONS INSTEAD OF IMPLICIT
C --------------------------------
      REAL*8    CLMB, CPHI, SLMB, SPHI
C
CCC       IMPLICIT REAL*8 (A-H,O-Z)
      REAL*8 LOCECC(3),XSTECC(3),DRMAT(3,3),XSTELL(3)
C
C SIN AND COS FUNCTIONS
C ---------------------
      SPHI=DSIN(XSTELL(1))
      CPHI=DCOS(XSTELL(1))
      SLMB=DSIN(XSTELL(2))
      CLMB=DCOS(XSTELL(2))
C
C COMPUTE ROTATION MATRIX
C -----------------------
      DRMAT(1,1)=-SPHI*CLMB
      DRMAT(1,2)=-SPHI*SLMB
      DRMAT(1,3)= CPHI
      DRMAT(2,1)=     -SLMB
      DRMAT(2,2)=      CLMB
      DRMAT(2,3)= 0.D0
      DRMAT(3,1)= CPHI*CLMB
      DRMAT(3,2)= CPHI*SLMB
      DRMAT(3,3)= SPHI
C
C ROTATE ECCENTRICITIES
C ---------------------
      CALL DMLMAV(XSTECC,DRMAT,LOCECC)
C
      RETURN
      END SUBROUTINE

      END MODULE
