MODULE s_PRTOPT
CONTAINS


! -------------------------------------------------------------------------
! Bernese GPS Software Version 5.1
! -------------------------------------------------------------------------

SUBROUTINE prtopt

! -------------------------------------------------------------------------
! Purpose:    This is a new version of the old subroutine PRTOPT.f that
!             prints the input options of the program MAUPRP
!
! Author:     L. Mervart
!
! Created:    03-JUN-2000
! Last mod.:  __-___-____
!
! Changes:    __-___-____ __:
!
! Copyright:  Astronomical Institute
!              University of Bern
!                  Switzerland
! -------------------------------------------------------------------------

  USE m_bern

  IMPLICIT NONE

END SUBROUTINE prtopt

END MODULE
