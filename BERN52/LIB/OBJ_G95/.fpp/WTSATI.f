      MODULE s_WTSATI
      CONTAINS

C*
      SUBROUTINE WTSATI(LFN   ,SVN   ,TSACLK,NSACLK,SATCLK,IRCODE)
CC
CC NAME       :  WTSATI
CC
CC PURPOSE    :  WRITE ONE RECORD OF SATELLITE CLOCK FILE
CC
CC PARAMETERS :
CC        IN  :  LFN    : LOGICAL FILE NUMBER FOR SAT.CLK FILE   I*4
CC               SVN    : SATELLITE NUMBER                       I*4
CC               TSACLK : SATELLITE CLOCK EPOCH (MJD)            R*8
CC               NSACLK : NUMBER OF SATELLITE CLOCK PARAMETERS   I*4
CC               SATCLK(I),I=1,..,NSACLK: SATELLITE CLOCK PARAM. R*8
CC                        VALUES: OFFSET (S), DRIFT (S/D), ...)
CC               IRCODE : ALWAYS 0 AT PRESENT                    I*4
CC
CC REMARKS    :  ---
CC
CC AUTHOR     :  M.ROTHACHER
CC
CC VERSION    :  4.1
CC
CC CREATED    :  19-AUG-98
CC
CC CHANGES    :  17-FEB-03 : LM: USE M_MAXDIM
CC               21-JUN-05 : MM: COMLFNUM.inc REMOVED, m_bern ADDED
CC               23-JUN-05 : MM: IMPLICIT NONE AND DECLARATIONS ADDED
CC
CC COPYRIGHT  :  ASTRONOMICAL INSTITUTE
CC      1998     UNIVERSITY OF BERN
CC               SWITZERLAND
CC
C*
      USE m_bern
      USE m_maxdim, ONLY: MAXSAC
      USE s_mjdgps
      USE s_exitrc
      IMPLICIT NONE
C
C DECLARATIONS INSTEAD OF IMPLICIT
C --------------------------------
      INTEGER*4 II    , IRCODE, LFN   , NSACLK, NWEEK
C
      REAL*8    SECOND, TSACLK
C
CCC       IMPLICIT REAL*8(A-H,O-Z)
C
C
      CHARACTER*80 FILCLK
C
      REAL*8       SATCLK(MAXSAC)
C
      INTEGER*4    SVN
C
C
C CONVERT MJD TO GPS WEEK AND SECONDS
C -----------------------------------
      CALL MJDGPS(TSACLK,SECOND,NWEEK)
C
C WRITE SATELLITE CLOCK FILE RECORD
C ---------------------------------
      WRITE(LFN,1,ERR=920) SVN,NWEEK,SECOND,NSACLK,
     1                     (SATCLK(II),II=1,NSACLK)
1     FORMAT(I3,I5,F9.0,I3,1X,10D17.9)
C
      GOTO 999
C
C ERROR READING FILE
C ------------------
920   INQUIRE(UNIT=LFN,NAME=FILCLK)
      WRITE(LFNERR,921) FILCLK, LFN
921   FORMAT(/,' *** SR WTSATI : ERROR WRITING SATELLITE CLOCK FILE',/,
     1       17X,'FILE NAME : ',A80,/,
     2       17X,'FILE UNIT : ',I6,/)
      CALL EXITRC(2)
C
C END
C ---
999   RETURN
      END SUBROUTINE

      END MODULE
