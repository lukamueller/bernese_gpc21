      MODULE s_RSWVEC
      CONTAINS

C*
      SUBROUTINE RSWVEC(YSAT,ER,ES,EW)
CC
CC NAME       :  RSWVEC
CC
CC PURPOSE    :  COMPUTE UNIT VECTORS IN RADIAL, "TANGENTIAL", AND
CC               OUT OF PLANE DIRECTIONS USING THE SATELLITES POSITION
CC               AND VELOCITY VECTORS
CC
CC PARAMETERS :
CC        IN  :  YSAT   : SATELLITE POSITION AND VELOCITY           R*8
CC        OUT :  ER     : UNIT VECTOR IN RADIAL DIRECTION           R*8
CC               ES     : UNIT VECTOR IN "TANGENTIAL" DIRECTION     R*8
CC               EW     : UNIT VECTOR IN OUT-OF-PLANE DIRECTION     R*8
CC
CC REMARKS    :
CC
CC AUTHOR     :  G.BEUTLER
CC
CC VERSION    :  4.0  (AUG 96)
CC
CC CREATED    :  96/08/24
CC
CC CHANGES    :  23-JUN-05 : MM: IMPLICIT NONE AND DECLARATIONS ADDED
CC
CC COPYRIGHT  :  ASTRONOMICAL INSTITUTE
CC      1996     UNIVERSITY OF BERN
CC               SWITZERLAND
CC
C*
      USE s_vprod
      IMPLICIT NONE
C
C DECLARATIONS INSTEAD OF IMPLICIT
C --------------------------------
      INTEGER*4 K
C
      REAL*8    RSAT, XXX
C
CCC       IMPLICIT REAL*8 (A-H,O-Z)
CCC       IMPLICIT INTEGER*4 (I-N)
      REAL*8 YSAT(*),ER(*),ES(*),EW(*)
C
      RSAT=DSQRT(YSAT(1)**2+YSAT(2)**2+YSAT(3)**2)
      DO 10 K=1,3
        ER(K)=YSAT(K)/RSAT
10    CONTINUE
C
      CALL VPROD(YSAT,YSAT(4),EW)
      XXX=DSQRT(EW(1)**2+EW(2)**2+EW(3)**2)
      DO 20 K=1,3
        EW(K)=EW(K)/XXX
20    CONTINUE
C
      CALL VPROD(EW,ER,ES)
C
999   CONTINUE
      RETURN
      END SUBROUTINE

      END MODULE
