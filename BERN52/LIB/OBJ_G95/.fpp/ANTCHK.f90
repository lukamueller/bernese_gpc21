! ------------------------------------------------------------------------------
! Bernese GNSS Software Version 5.3
! ------------------------------------------------------------------------------

MODULE f_antchk

  IMPLICIT NONE

CONTAINS

FUNCTION antchk(ant1, ant2)

! ------------------------------------------------------------------------------
! Purpose:    Check two ANTEX pattern if they are identical
!
! Author:     A. Villiger
!
! Created:    08-Jan-2018
!
! Changes:
!
! Copyright:  Astronomical Institute
!             University of Bern
!             Switzerland
! ------------------------------------------------------------------------------

! Used modules
! ------------
  USE m_bern,   ONLY: i4b
  USE m_global, ONLY: maxSys
  USE d_phaecc, ONLY: t_phasfil

! input
  TYPE(t_phasfil)             :: ant1
  TYPE(t_phasfil)             :: ant2

! output
  LOGICAL                     :: antchk



! Local parameters

! Local variables

  INTEGER(i4b)                :: isys
  INTEGER(i4b)                :: ifrq
  INTEGER(i4b)                :: icor
  INTEGER(i4b)                :: iazi
  INTEGER(i4b)                :: ielv
  INTEGER(i4b)                :: mod
  INTEGER(i4b)                :: ihelp1
  INTEGER(i4b)                :: ant1elv
  INTEGER(i4b)                :: ant1azi
  INTEGER(i4b)                :: ant2elv
  INTEGER(i4b)                :: ant2azi

  mod = 0

  ! Loop over all frequencies
  DO isys=0,maxsys-1
    ! Check number of frequencies
    IF(ant1%sys(isys)%nfreq /= ant2%sys(isys)%nfreq) THEN
       mod = 1
       EXIT
    ENDIF

    ! different offset values?
    DO ifrq=1, ant1%sys(isys)%nfreq
      DO icor=1,3
        ihelp1 = &
                 NINT(ant1%sys(isys)%freq(ifrq)%off(0,ICOR) - &
                 ANINT(ant2%sys(isys)%freq(ifrq)%off(0,ICOR)*10000d0)/10000d0)
        IF (ABS(ihelp1) > 1.D-6) THEN
          mod = 1
          EXIT
       ENDIF
      ENDDO ! coordinate
    ENDDO ! frequency

  ! different number of elevation/azimut values?
    ant1elv = -1
    ant1azi = -1
    IF (ant1%sys(isys)%typ == 0) THEN
      ant1elv = 0
    ELSEIF(ant1%sys(isys)%typ == 1) THEN
      IF (ant1%sys(isys)%resolu(2) == ant1%sys(isys)%resolu(4)) THEN
        ant1elv = 1
      ELSE
        ant1elv = ant1%sys(isys)%resolu(4) / ant1%sys(isys)%resolu(2) + 1
      ENDIF
      IF (ant1%sys(isys)%resolu(3) == 360) THEN
        ant1azi = 1
      ELSE
        ant1azi = 360 / ant1%sys(isys)%resolu(3) + 1
      ENDIF
    ENDIF

    ant2elv = -1
    ant2azi = -1
    IF (ant2%sys(isys)%typ == 0) THEN
      ant2elv = 0
    ELSEIF(ant2%sys(isys)%typ == 1) THEN
      IF (ant2%sys(isys)%resolu(2) == ant2%sys(isys)%resolu(4)) THEN
        ant2elv = 1
      ELSE
        ant2elv = ant2%sys(isys)%resolu(4) / ant2%sys(isys)%resolu(2) + 1
      ENDIF
      IF (ant2%sys(isys)%resolu(3) == 360) THEN
        ant2azi = 1
      ELSE
        ant2azi = 360 / ant2%sys(isys)%resolu(3) + 1
      ENDIF
    ENDIF

    IF(ant1elv /= ant2elv) THEN
      mod = 1
      EXIT
    ENDIF
    IF(ant1azi /= ant2azi) THEN
      mod = 1
      EXIT
    ENDIF

    ! different PCV values?
    DO ifrq=1,ant1%sys(isys)%nfreq
      DO ielv=1,ant1elv
        DO iazi=1,ant1azi
          ihelp1 = &
                  NINT(ant1%sys(isys)%freq(ifrq)%pat(0,ielv,iazi) - &
                  ant2%sys(isys)%freq(ifrq)%pat(0,ielv,iazi))
          IF (ABS(ihelp1) > 1.D-7) THEN
            mod = 1
            EXIT
          ENDIF
        ENDDO ! azimut
      ENDDO ! elevation
    ENDDO ! frequency
  ENDDO ! system

  IF(mod==0) THEN
    antchk = .TRUE.
  ELSE
    antchk = .FALSE.
  ENDIF

  RETURN
  END FUNCTION antchk

END MODULE f_antchk
