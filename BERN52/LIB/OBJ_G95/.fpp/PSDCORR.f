      MODULE s_psdcorr
      CONTAINS

       subroutine psdcorr (stName,mjd,datum,xstat,yesno)
C
C Author: Zuheir Altamimi (zuheir.altamimi@ign.fr), IGN France
C
C Last updated: August 17, 2015
C
C WARNING : This is just an example of a subroutine where the user
C           should adapt to his application AND insert his own routines
C           at two places as indicated in the comment lines below.
C
C IN:
C DOMES: point Id (could be changed to CDP number, etc.)
C mjd:   (station position epoch in mjd - double precision)
C
C IN & OUT: X, Y, Z
C
C Correction of post-seismic deformation using parametric models
C #    Model
C 0    PWL
C 1    Logarithmic Function
C 2    Exponential Function
C 3    Logarithmic + Exponential
C 4    Two Exponential Functions
C
C The model selection is determined by reading the file "psdmodel.dat"
C and calling the routine parametric()
C
C Created:    30-Sep-2015
C
C Changes:    30-Sep-2015 RD: Adapted to BSW environment
C

C Modules
C -------
      USE m_bern,   ONLY: i4b,r8b, lfnerr,
     1                    fileNameLength,staNameLength
      USE d_datum,  ONLY: t_datum
      USE d_psdFil, ONLY: t_psdSta, getPsd

      USE s_gtflna
      USE s_ellecc
      USE s_xyzell
      USE s_parametric
      IMPLICIT NONE

C List of parameters
C ------------------
C input:
      CHARACTER(LEN=staNamelength)        :: stName ! Station name
      REAL(r8b)                           :: mjd    ! Epoch
      TYPE(t_datum)                       :: datum  ! Geodetic datum
C input/output:
      REAL(r8b), DIMENSION(3)             :: xstat  ! station coordinates
C output
      LOGICAL                             :: yesno  ! returns whether PSD
                                                    ! corrections have been
                                                    ! applied or not

C Local variables
C ---------------
      TYPE(t_psdSta)                      :: psdSta

      CHARACTER(LEN=fileNameLength), SAVE :: psdFil = ''
      CHARACTER(LEN=fileNameLength), SAVE :: datFil = ''

      REAL(r8b)                           :: dtq
      REAL(r8b), DIMENSION(3)             :: locecc, xyzecc, xstell

      INTEGER(i4b)                        :: I, ii
      INTEGER(i4b)                        :: iPsd
      INTEGER(i4b)                        :: irc

C
C INIT VARIABLE
      yesno = .FALSE.

C Check for the first call
C ------------------------
      IF ( datFil == '' ) THEN
        CALL GTFLNA(1,'DATUM',datfil,irc)

C Check for the PSD file
C ----------------------
        CALL GTFLNA(0,'PSDDAT',psdfil,irc)
        IF ( psdFil == '' .AND. datum%type == 'W/O_PSD' ) THEN
          WRITE(LFNERR,'(/,A,/,17X,A,2(/,17X,A,3X,A),/)')
     1    ' ### SR PSDCORR: ' //
     2      'The datum is indicating no PSD-corrections applied so far',
     3       'but you have not specified a PSD correction file',
     4       'Geodetic datum name:',TRIM(datum%name),
     5       'Geodetic datum file:',TRIM(datfil)
        ENDIF
        IF (irc /= 0) GOTO 999

C Check the datum type
C --------------------
        IF ( datum%type /= 'W/O_PSD' ) THEN
          WRITE(LFNERR,'(/,A,2(/,17X,A),3(/,17X,A,3X,A),/)')
     1    ' ### SR PSDCORR: ' //
     2       'The datum type indicates no need for applying the',
     3       'post-seismic deformation corrections but you have',
     4       'selected a PSD-correction file:',
     4       'Geodetic datum name:',TRIM(datum%name),
     5       'Geodetic datum type:',TRIM(datum%type),
     6       'Geodetic datum file:',TRIM(datfil)
C
C Adapt the datum type
        ELSE
          I = LEN_TRIM(datum%name)
          IF (datum%name(I-1:I) == '_0') THEN
            datum%name(I-1:I) = '  '
          ELSE
            WRITE(LFNERR,'(/,A,/,17X,A,2(/,17X,A,3X,A),/,17X,A,/)')
     1      ' ### SR PSDCORR: ' //
     2      'The datum is indicating no PSD-corrections applied so far',
     3      'but the name of the datum does not end with "_0"',
     4      'Geodetic datum name:',TRIM(datum%name),
     5      'Geodetic datum file:',TRIM(datfil),
     6      'Please adapt the datum manually ' //
     7      '(e.g., by CRDMERGE program).'
          ENDIF
        ENDIF
      ELSEIF ( psdfil == '' ) THEN
        GOTO 999
      ENDIF

C Get the necessary corrections
C -----------------------------
      psdSta = getPsd(psdfil,stName,mjd)

C Loop all necessary corrections
C ------------------------------
      DO iPsd = 1, psdSta%nPsd

        dtq = (mjd - psdSta%psdRec(iPsd)%epoch) / 365.25d0
        locecc = 0d0
        DO ii = 1,3
          CALL parametric(psdSta%psdRec(iPsd)%type(ii),dtq,
     1                    psdSta%psdRec(iPsd)%psdCor(ii,1),
     2                    psdSta%psdRec(iPsd)%psdCor(ii,2),
     3                    psdSta%psdRec(iPsd)%psdCor(ii,3),
     4                    psdSta%psdRec(iPsd)%psdCor(ii,4),
     5                    locecc(ii))
        ENDDO

C
C       call your own subroutine to transform dENU --> dXYZ
C       Output --> dx, dy, dz
C
C
        CALL xyzell(datum%aell,datum%bell,datum%dxell,
     1              datum%drell,datum%scell,xstat,xstell)
        call ellecc(xstell,locecc,xyzecc)
        DO I=1,3
          xstat(i) = xstat(i) + xyzecc(i)
          yesNo = yesNo .OR. ( DABS(xyzecc(i)) > 0D0 )
        ENDDO

      ENDDO

  999 RETURN

      END subroutine psdcorr

      END MODULE s_psdcorr
