      MODULE s_CLOSES
      CONTAINS

C*
      SUBROUTINE CLOSES(NFLSES,IEXTRA)
CC
CC NAME       :  CLOSES
CC
CC PURPOSE    :  CLOSE OBSERVATION FILES AND METEO FILES AFTER
CC               HAVING PROCESSED THE SESSION (GPSEST)
CC
CC PARAMETERS :
CC         IN :  NFLSES : NUMBER OF FILE IN SESSION           I*4
CC               IEXTRA : EXTRAPOLATION OR MEASURED VALUES    I*4
CC                        FOR METEOROLOGICAL INFO
CC                        =0: VALUES FROM METEO FILES
CC                        =1: EXTRAPOLATED VALUES
CC
CC REMARKS    :  ---
CC
CC AUTHOR     :  M.ROTHACHER
CC
CC VERSION    :  3.6  (SEP 94)
CC
CC CREATED    :  23-SEP-94
CC
CC CHANGES    :  28-SEP-95 : JJ: ADD CDMY
CC               25-JUN-02 : DS: CLOSE THE COORDINATE SCRATCH FILE
CC               19-JUL-02 : DS: LFNKSC REMOVED
CC               19-JUL-02 : DS: DEALLOCATION: KINVEL,LEOPRE
CC               30-OCT-02 : MR: USE I_GPSLIB INSTEAD OF I_ASTLIB
CC               08-MAR-03 : HU: USE SR INQUIRE
CC               06-JUN-03 : HU: CALL FOR READPRE CORRECTED
CC               29-MAR-04 : CU: ADD DUMMY VARIABLE TO CALL OF SR METEO
CC               21-JUN-05 : MM: COMLFNUM.inc REMOVED, m_bern ADDED
CC               23-JUN-05 : MM: IMPLICIT NONE AND DECLARATIONS ADDED
CC               22-JUL-05 : HB: CLOSE ALSO LEO-STD file (LFNOR1)
CC               29-FEB-12 : RD: CORRECT ARRAY DIMENSIONS OF DUMMY ARGUMENTS
CC               29-Feb-12 : RD: USE METEO AS MODULE
CC               09-SEP-12 : SL: SHAPE OF METEO PARAMETER CHANGED
CC
CC COPYRIGHT  :  ASTRONOMICAL INSTITUTE
CC      1994     UNIVERSITY OF BERN
CC               SWITZERLAND
CC
C*
      USE m_bern, ONLY: lfn001, lfnor1
C
      USE s_readpre
      USE s_inquire
      USE s_readvel
      USE s_meteo
      USE s_gtflna
      IMPLICIT NONE
C
C DECLARATIONS INSTEAD OF IMPLICIT
C --------------------------------
      INTEGER*4 IEXTRA, IFIL  , IRC   , IRCPRE, IRCSCR, IRCVEL, LFNOBS,
     1          LFNTST, NFLSES
C
CCC       IMPLICIT REAL*8 (A-H,O-Z)
CCC       IMPLICIT INTEGER*4 (I-N)
C
      CHARACTER*32 FILSCR
      CHARACTER*32  FILVEL,FILPRE
      CHARACTER*16  STNAME
      CHARACTER*16 CDMY(1)
      REAL*8       DUMMY(6),DUMMY31(3,1)
C
      LOGICAL YES
C
C
C CLOSE ALL OBSERVATION FILES OF THE SESSION
C ------------------------------------------
      DO IFIL=1,NFLSES
        LFNOBS=LFN001+(IFIL-1)
        CLOSE(UNIT=LFNOBS)
      ENDDO
C
C CLOSE ALL METEO FILES OF THE SESSION
C ------------------------------------
      CALL METEO(2,1,IEXTRA,0,CDMY,0,
     1           0.D0,DUMMY31,DUMMY,DUMMY,0.D0,DUMMY,DUMMY,DUMMY,DUMMY)
C
C CLOSE AUX. KINEMATIC COORDINATES INPUT FILE "KINSCR" IF OPEN
C ------------------------------------------------------------
      CALL GTFLNA(0,'KINSCR ',FILSCR,IRCSCR)
      IF (IRCSCR.EQ.0) THEN
        CALL INQUIRE(FILE=FILSCR,OPENED=YES,NUMBER=LFNTST)
        IF (YES) THEN
          CLOSE(UNIT=LFNTST)
        ENDIF
      ENDIF
C
C CLOSE SECOND STD FILE IF OPEN
C -----------------------------
      CALL INQUIRE(UNIT=LFNOR1,OPENED=YES)
      IF (YES) CLOSE(UNIT=LFNOR1)
C
C DEALLOCATE KINEMATIC ARRAYS IF EXIST
C ------------------------------------
      CALL GTFLNA(0,'KINVEL ',FILVEL,IRCVEL)
      CALL GTFLNA(0,'LEOPRE ',FILPRE,IRCPRE)
      IF (IRCVEL==0) CALL READVEL(FILVEL,STNAME,0.D0,1,1,DUMMY,IRC)
      IF (IRCPRE==0) CALL READPRE(FILPRE,0,0.D0,1,DUMMY,IRC)
C
      RETURN
      END SUBROUTINE

      END MODULE
