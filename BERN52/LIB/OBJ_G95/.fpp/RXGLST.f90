MODULE s_RXGLST
CONTAINS

! -------------------------------------------------------------------------
! Bernese GPS Software Version 5.1
! -------------------------------------------------------------------------

SUBROUTINE rxglst(nfil,filnam,intgra,lstchr,opt)

! -------------------------------------------------------------------------
!
! Purpose:    gets a list of RINEX files corresponding to some conditions
!
! Author:     R. Dach
!
! Created:    20-Nov-2000
!
! Changes:    21-Dec-2001 HU: Use m_bern, other modules with ONLY
!             27-Feb-2003 RD: Replace path variables before writing file lists
!             16-Feb-2004 RD: Disable the options with value "0"
!             17-Jun-2011 LP: Typo in error message corrected
!             16-Aug-2013 SS: Delete list files if indicated
!             19-JAN-2014 RD: Adjust exclusion limits if requested
!             19-Jan-2014 RD: Count only observations from specific systems
!             19-Jan-2014 RD: Use M_BERN with ONLY
!
! Copyright:  Astronomical Institute
!             University of Bern
!             Switzerland
! -------------------------------------------------------------------------

! Modules
! -------
  USE m_bern,   ONLY: i4b, fileNameLength, lfnerr, lfn001, lfn002
  USE m_global, ONLY: g_strsys3
  USE p_rnxgra, ONLY: t_rnxgra_opt, maxfil,maxsat,maxchr

  USE s_opnfil
  USE s_opnerr
  USE s_rplenvar
  IMPLICIT NONE

! List of Parameters
! ------------------
! input
  INTEGER(i4b)                                :: nFil    ! # of RINEX files
  CHARACTER(LEN=fileNameLength),  &
               DIMENSION(MAXFIL)              :: FILNAM  ! names of RINEX files
  INTEGER(i4b),DIMENSION(MAXFIL,MAXSAT,MAXCHR):: INTGRA  ! array with obs.
  INTEGER(i4b)                                :: lstChr  ! Last character used
                                                 ! in one of the RINEX files
  TYPE(t_rnxgra_opt)                          :: opt     ! input options

! output

! Used functions
! --------------

! Local Types
! -----------

! Local Parameters
! ----------------

! Local Variables
! ---------------
  LOGICAL,     DIMENSION(MAXFIL)              :: inList  ! OK-Flag

  CHARACTER(LEN= 8)                           :: sysStr
  CHARACTER(LEN=13)                           :: hlpStr

  INTEGER(i4b),DIMENSION(MAXFIL)              :: NOBS    ! # obs per station
  INTEGER(i4b),DIMENSION(MAXFIL)              :: NBAD    ! # of bad obs per station
  INTEGER(i4b)                                :: mFil    ! # files in "OK-list"
  INTEGER(i4b)                                :: jFil    ! Index in file list
  INTEGER(i4b)                                :: minObs  ! helps for maxsta cond.
  INTEGER(i4b)                                :: ObsEpo  ! helps for minobs cond.
  INTEGER(i4b)                                :: BadEpo  ! helps for badobs cond.
  INTEGER(i4b)                                :: iFil    ! counter files
  INTEGER(i4b)                                :: iSat    ! counter satellites
  INTEGER(i4b)                                :: iChr    ! counter characters
  INTEGER(i4b)                                :: ios     ! iostatus
  INTEGER(i4b)                                :: n1      ! # OK files
  INTEGER(i4b)                                :: n2      ! # bad files

  CHARACTER(LEN=255)                          :: filnm2  ! Long file name

  mFil=0
  DO iFil=1,nFil
!
! Count the number of observations per station
! --------------------------------------------
    inList(iFil)=.TRUE.
    nOBS(iFil)=0
    nBad(iFil)=0
    DO iChr=1,LSTCHR
      BadEpo=0
      ObsEpo=0
      DO iSat=1,MAXSAT
        IF ( opt%obssys == -1 .OR. iSat/100 == opt%obssys ) THEN
          IF (intGra(iFil,iSat,iChr) == +1) ObsEpo=ObsEpo+1
          IF (intGra(iFil,iSat,iChr) == -1) BadEpo=BadEpo+1
        ENDIF
      ENDDO
      IF (ObsEpo > 0) nOBS(iFil)=nOBS(iFil)+ObsEpo
      IF (opt%badobs > 0 .AND. ObsEpo <= opt%badobs) &
        nBad(iFil)=nBad(iFil)+1
    ENDDO
!
! Check the min. number of obs.
! -----------------------------
   IF (nObs(iFil) < opt%minObs) THEN
      inList(iFil)=.False.
      sysStr = ''
      IF (opt%obsSys >=0) sysStr = '   (' // g_strsys3(opt%obsSys) // ')'
      hlpStr = ''
      IF (opt%adjDay) hlpStr = '   (adjusted)'
      WRITE(lfnerr,'(/,A,/,16X,A,A,2(/,16X,A,I5,A),/)')                   &
                 ' ### SR RXGLST: TOO FEW OBSERVATIONS IN FILE',          &
                         'FILE NAME               : ',TRIM(filnam(iFil)), &
                         'NUMBER OF OBSERVATIONS  : ',nobs(iFIl),         &
                                                      TRIM(sysStr),       &
                         'MIN. OBSERV. REQUESTED  : ',opt%minobs,         &
                                                      TRIM(hlpStr)
!
! Check the number of bad epochs
! ------------------------------
    ELSE IF (nBad(iFil) > opt%maxBad .AND. opt%maxBad > 0) THEN
      inList(iFil)=.False.
      sysStr = ''
      IF (opt%obsSys >=0) sysStr = '   (' // g_strsys3(opt%obsSys) // ')'
      hlpStr = ''
      IF (opt%adjDay) hlpStr = '   (adjusted)'
      WRITE(lfnerr,'(/,A,/,16X,A,A,/,16X,A,I5,A,/,16X,A,I5,/,16X,A,I5,A,/)')        &
                 ' ### SR RXGLST: TOO MANY EPOCHS WITH FEW OBSERVATIONS', &
                         'FILE NAME               : ',TRIM(filnam(iFil)), &
                         'NUMBER OF EPOCHS        : ',nbad(iFil),         &
                                                      TRIM(sysStr),       &
                         'WITH LESS THAN OBSERV.  : ',opt%badobs+1,       &
                         'MAX. # OF EPOCHS ALLOWED: ',opt%maxBad,         &
                                                      TRIM(hlpStr)
!
! Count the number of good files
! ------------------------------
    ELSE
      mFil=mFil+1
!      WRITE(*,*) 'OK : ',TRIM(filnam(iFil)),nobs(iFIl),nbad(iFil),mFil
    ENDIF

  ENDDO
!
! Select only maxfil
! ------------------
  DO WHILE (mFil > opt%maxsta .AND. opt%maxsta /= 0)
    minObs=999999
    jFil=-1
    DO iFil=1,nFil
      IF (inList(iFil) .AND. nObs(iFil) < minObs) THEN
        jFil=iFil
        minOBs=nObs(iFIl)
      ENDIF
    ENDDO
    inList(jFil)=.False.
    sysStr = ''
    IF (opt%obsSys >=0) sysStr = '   (' // g_strsys3(opt%obsSys) // ')'
    WRITE(lfnerr,'(/,A,/,16X,A,A,/,16X,A,I5,A,/,16X,A,I5,/)')         &
             ' ### SR RXGLST: TOO MANY FILES IN LIST',                &
                     'FILE NAME               : ',TRIM(filnam(jFil)), &
                     'NUMBER OF OBSERVATIONS  : ',nobs(jFil),         &
                                                  TRIM(sysStr),       &
                     'MAX # OF FILES ALLOWED  : ',opt%maxsta
    mFil=mFil-1
  ENDDO
!
! Write file names into the lists
! -------------------------------
  IF (LEN_TRIM(opt%lstfil) > 0) THEN
    CALL OPNFIL(LFN001,opt%lstfil,'UNKNOWN','FORMATTED',' ',' ',IOS)
    CALL OPNERR(LFNERR,LFN001,IOS,opt%lstfil,'RXGLST')
  ENDIF
!
  IF (LEN_TRIM(opt%delfil) > 0) THEN
    CALL OPNFIL(LFN002,opt%delfil,'UNKNOWN','FORMATTED',' ',' ',IOS)
    CALL OPNERR(LFNERR,LFN002,IOS,opt%lstfil,'RXGLST')
  ENDIF
!
  n1=0
  n2=0
  DO iFil=1,nFil
    IF (inList(iFil)) THEN
      IF (LEN_TRIM(opt%lstfil) > 0) THEN
        filnm2=FILNAM(iFil)
        CALL rplenvar(1,filnm2)
        WRITE(lfn001,'(A)') TRIM(FILNM2)
        n1=n1+1
      ENDIF
    ELSE
      IF (LEN_TRIM(opt%delfil) > 0) THEN
        filnm2=FILNAM(iFil)
        CALL rplenvar(1,filnm2)
        WRITE(lfn002,'(A)') TRIM(FILNM2)
        n2=n2+1
      ENDIF
    ENDIF
  ENDDO
!
  IF (LEN_TRIM(opt%lstfil) > 0) THEN
    IF (n1 > 0) THEN
      CLOSE(LFN001)
    ELSE
      CLOSE(LFN001,STATUS='DELETE')
    ENDIF
  ENDIF
  IF (LEN_TRIM(opt%delfil) > 0) THEN
    IF (n2 > 0) THEN
      CLOSE(LFN002)
    ELSE
      CLOSE(LFN002,STATUS='DELETE')
    ENDIF
  ENDIF
!
  END SUBROUTINE rxglst

END MODULE
