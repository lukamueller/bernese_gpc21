G95 module created on Thu Jun 25 17:49:31 2020 from ./.fpp/P_BPE.f90
If you edit this, you'll get what you deserve.
module-version 9
(() () () () () () () () () () () () () () () () () () () () ())

()

()

()

()

(2 'flgsingle' 'p_bpe' 1 ((PARAMETER UNKNOWN UNKNOWN UNKNOWN NONE NONE)
(CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '1'))) 0 0 () (CONSTANT (
CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '1'))) 0 1 'S') () () '' () ())
3 'i4b' 'm_bern' 1 ((PARAMETER UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
INTEGER 4) 0 0 () (CONSTANT (INTEGER 4) 0 '4') () () '' () ())
4 'init_pcf' 'p_bpe' 1 ((PROCEDURE UNKNOWN MODULE-PROC DECL NONE NONE
SUBROUTINE) (PROCEDURE 0) 0 0 (5 NONE) () () '' () ())
6 'kdescr' 'p_bpe' 1 ((PARAMETER UNKNOWN UNKNOWN UNKNOWN NONE NONE
DIMENSION) (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '31'))) 0 0 () (ARRAY (
CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '31'))) 1 (((CONSTANT (CHARACTER 1
((CONSTANT (INTEGER 4) 0 '31'))) 0 31 'List of BPE scripts            ')
()) ((CONSTANT (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '31'))) 0 31
'Special actions for the scripts') ()) ((CONSTANT (CHARACTER 1 ((
CONSTANT (INTEGER 4) 0 '31'))) 0 31 'Parameters for the scripts     ') ())
((CONSTANT (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '31'))) 0 31
'PCF default variables          ') ())) ()) (1 EXPLICIT (CONSTANT (
INTEGER 4) 0 '1') (CONSTANT (INTEGER 4) 0 '4')) () '' () ())
7 'kwords' 'p_bpe' 1 ((PARAMETER UNKNOWN UNKNOWN UNKNOWN NONE NONE
DIMENSION) (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '15'))) 0 0 () (ARRAY (
CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '15'))) 1 (((CONSTANT (CHARACTER 1
((CONSTANT (INTEGER 4) 0 '15'))) 0 15 'LIST_OF_SCRIPTS') ()) ((CONSTANT
(CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '15'))) 0 15 'SPECIALS       ') ())
((CONSTANT (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '15'))) 0 15
'PARAMETERS     ') ()) ((CONSTANT (CHARACTER 1 ((CONSTANT (INTEGER 4) 0
'15'))) 0 15 'PCF_VARIABLES  ') ())) ()) (1 EXPLICIT (CONSTANT (INTEGER
4) 0 '1') (CONSTANT (INTEGER 4) 0 '4')) () '' () ())
8 'linelength' 'm_bern' 1 ((PARAMETER UNKNOWN UNKNOWN UNKNOWN NONE NONE)
(INTEGER 4) 0 0 () (CONSTANT (INTEGER 4) 0 '255') () () '' () ())
9 'm_bern' 'm_bern' 1 ((MODULE UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' () ())
10 'maxdsc' 'p_bpe' 1 ((PARAMETER UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
INTEGER 4) 0 0 () (CONSTANT (INTEGER 4) 0 '60') () () '' () ())
11 'maxpid' 'p_bpe' 1 ((PARAMETER UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
INTEGER 4) 0 0 () (CONSTANT (INTEGER 4) 0 '400') () () '' () ())
12 'maxwat' 'p_bpe' 1 ((PARAMETER UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
INTEGER 4) 0 0 () (CONSTANT (INTEGER 4) 0 '10') () () '' () ())
13 'numval' 'p_bpe' 1 ((PARAMETER UNKNOWN UNKNOWN UNKNOWN NONE NONE
DIMENSION) (INTEGER 4) 0 0 () (ARRAY (INTEGER 4) 1 (((CONSTANT (INTEGER
4) 0 '16') ()) ((CONSTANT (INTEGER 4) 0 '14') ()) ((CONSTANT (INTEGER 4)
0 '12') ()) ((CONSTANT (INTEGER 4) 0 '3') ())) ()) (1 EXPLICIT (
CONSTANT (INTEGER 4) 0 '1') (CONSTANT (INTEGER 4) 0 '4')) () '' () ())
14 'p_bpe' 'p_bpe' 1 ((MODULE UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' () ())
15 'pcf_header' 'p_bpe' 1 ((PARAMETER UNKNOWN UNKNOWN UNKNOWN NONE NONE
DIMENSION) (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '120'))) 0 0 () (ARRAY
(CHARACTER 1 ()) 1 (((CONSTANT (CHARACTER 1 ()) 0 120
'PID SCRIPT   OPT_DIR  CAMPAIGN CPU      F WAIT FOR....                                                                  ')
()) ((CONSTANT (CHARACTER 1 ()) 0 120
'3** 8******* 8******* 8******* 8******* 1 3** 3** 3** 3** 3** 3** 3** 3** 3** 3**                                       ')
()) ((CONSTANT (CHARACTER 1 ()) 0 120
'PID USER         PASSWORD PARAM1   PARAM2   PARAM3   PARAM4   PARAM5   PARAM6   PARAM7   PARAM8   PARAM9                ')
()) ((CONSTANT (CHARACTER 1 ()) 0 120
'3** 12********** 8******* 8******* 8******* 8******* 8******* 8******* 8******* 8******* 8******* 8*******              ')
()) ((CONSTANT (CHARACTER 1 ()) 0 120
'VARIABLE DESCRIPTION                              DEFAULT                                                               ')
()) ((CONSTANT (CHARACTER 1 ()) 0 120
'8******* 40************************************** 30****************************                                        ')
())) ()) (1 EXPLICIT (CONSTANT (INTEGER 4) 0 '1') (CONSTANT (INTEGER 4)
0 '6')) () '' () ())
16 'special' 'p_bpe' 1 ((PARAMETER UNKNOWN UNKNOWN UNKNOWN NONE NONE
DIMENSION) (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '8'))) 0 0 () (ARRAY (
CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '8'))) 1 (((CONSTANT (CHARACTER 1 (
(CONSTANT (INTEGER 4) 0 '8'))) 0 8 'SKIP    ') ()) ((CONSTANT (
CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '8'))) 0 8 '        ') ()) ((
CONSTANT (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '8'))) 0 8 'PARALLEL') ())
((CONSTANT (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '8'))) 0 8 'NEXTJOB ')
()) ((CONSTANT (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '8'))) 0 8 'CONT_ERR')
())) ()) (1 EXPLICIT (CONSTANT (INTEGER 4) 0 '1') (CONSTANT (INTEGER 4)
0 '5')) () '' () ())
17 't_job' 'p_bpe' 1 ((DERIVED UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' ((18 'ipid' (INTEGER 4) () () 0 0 0 ()) (19
'script' (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '8'))) () () 0 0 0 ()) (
20 'option' (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '8'))) () () 0 0 0 ())
(21 'camp' (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '8'))) () () 0 0 0 ())
(22 'cpu' (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '8'))) () () 0 0 0 ()) (
23 'user' (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '12'))) () () 0 0 0 ())
(24 'paswrd' (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '8'))) () () 0 0 0 ())
(25 'n_wait' (INTEGER 4) () () 0 0 0 ()) (26 'iwait' (INTEGER 4) (1
EXPLICIT (CONSTANT (INTEGER 4) 0 '1') (CONSTANT (INTEGER 4) 0 '10')) ()
1 0 0 ()) (27 'ptype' (INTEGER 4) () () 0 0 0 ()) (28 'params' (
CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '80'))) (1 EXPLICIT (CONSTANT (
INTEGER 4) 0 '1') (CONSTANT (INTEGER 4) 0 '9')) () 1 0 0 ()) (29 'jflags'
(CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '1'))) () () 0 0 0 ())) PUBLIC ())
30 't_pcf' 'p_bpe' 1 ((DERIVED UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' ((31 'n_pid' (INTEGER 4) () () 0 0 0 ()) (32
'job' (DERIVED 17) (1 EXPLICIT (CONSTANT (INTEGER 4) 0 '1') (CONSTANT (
INTEGER 4) 0 '400')) () 1 0 0 ()) (33 'nvar' (INTEGER 4) () () 0 0 0 ())
(34 'var' (DERIVED 35) (1 EXPLICIT (CONSTANT (INTEGER 4) 0 '1') (
CONSTANT (INTEGER 4) 0 '60')) () 1 0 0 ()) (36 'ntxt' (INTEGER 4) () ()
0 0 0 ()) (37 'txt' (DERIVED 38) (1 DEFERRED () ()) () 1 1 0 ())) PUBLIC
())
38 't_txt' 'p_bpe' 1 ((DERIVED UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' ((39 'section' (INTEGER 4) () () 0 0 0 ()) (40
'line' (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '255'))) () () 0 0 0 ()) (
41 'pids' (INTEGER 4) (1 EXPLICIT (CONSTANT (INTEGER 4) 0 '1') (
CONSTANT (INTEGER 4) 0 '2')) () 1 0 0 ())) PUBLIC ())
35 't_var' 'p_bpe' 1 ((DERIVED UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' ((42 'varnam' (CHARACTER 1 ((CONSTANT (INTEGER
4) 0 '8'))) () () 0 0 0 ()) (43 'vardsc' (CHARACTER 1 ((CONSTANT (
INTEGER 4) 0 '40'))) () () 0 0 0 ()) (44 'vardef' (CHARACTER 1 ((
CONSTANT (INTEGER 4) 0 '32'))) () () 0 0 0 ())) PUBLIC ())
38 't_txt' 'p_bpe' 1 ((DERIVED UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' ((39 'section' (INTEGER 4) () () 0 0 0 ()) (40
'line' (CHARACTER 1 ((CONSTANT (INTEGER 4) 0 '255'))) () () 0 0 0 ()) (
41 'pids' (INTEGER 4) (1 EXPLICIT (CONSTANT (INTEGER 4) 0 '1') (
CONSTANT (INTEGER 4) 0 '2')) () 1 0 0 ())) PUBLIC ())
35 't_var' 'p_bpe' 1 ((DERIVED UNKNOWN UNKNOWN UNKNOWN NONE NONE) (
UNKNOWN) 0 0 () () () '' ((42 'varnam' (CHARACTER 1 ((CONSTANT (INTEGER
4) 0 '8'))) () () 0 0 0 ()) (43 'vardsc' (CHARACTER 1 ((CONSTANT (
INTEGER 4) 0 '40'))) () () 0 0 0 ()) (44 'vardef' (CHARACTER 1 ((
CONSTANT (INTEGER 4) 0 '32'))) () () 0 0 0 ())) PUBLIC ())
5 'pcf' '' 45 ((VARIABLE UNKNOWN UNKNOWN UNKNOWN NONE NONE DUMMY) (
DERIVED 30) 0 0 () () () '' () ())
)

('flgsingle' 0 2 'i4b' 0 3 'init_pcf' 0 4 'kdescr' 0 6 'kwords' 0 7
'linelength' 0 8 'm_bern' 0 9 'maxdsc' 0 10 'maxpid' 0 11 'maxwat' 0 12
'numval' 0 13 'p_bpe' 0 14 'pcf_header' 0 15 'special' 0 16 't_job' 0 17
't_pcf' 0 30 't_txt' 0 38 't_var' 0 35)
