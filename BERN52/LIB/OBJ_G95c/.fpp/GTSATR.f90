MODULE s_GTSATR

! -------------------------------------------------------------------------
! Bernese GPS Software Version 5.3
! -------------------------------------------------------------------------

! -------------------------------------------------------------------------
! Purpose:    This is the new version of SR GTSATR which reads structure
!             satfil instead of file "SATELL" and returns parameters of
!             radiation pressure model
!
! Author:     D. Svehla
!
! Created:    15-Mar-2001
!
! Changes:    16-Dec-2001 HU: Use implicit none
!             21-Dec-2001 HU: Use m_bern, ONLY for modules
!             16-May-2003 CU: Initialize structure
!             19-Nov-2003 HB: Use boundary NSARAD for copying from
!                             satfil-type
!             18-Oct-2006 MP: returns parameters of rpr model only for
!                             one specified satellite instead of all
!                             satellites
!             12-Mar-2007 CU: Correct format statements for error messages
!             11-Jul-2008 DT: Add formf, C_rpr, adrag, admod
!             03-Jun-2010 HU: Read orbital plane
!             02-Sep-2010 CR: Read space vehicle number
!             29-Aug-2011 PS: Epoch in error message
!             29-Aug-2011 SL: use m_bern with ONLY
!             14-Sep-2011 SL/PS: format statement corrected
!             13-Jul-2013 PS: dimension of svnnr 3 => 4
!             05-Dec-2014 RD: if no RPR model indicated use no time window
!             27-Apr-2015 RD: Solve rounding problem when reading sat-info file
!             21-Aug-2017 RD: Complete revision to overcome the repro-problem
!
! Copyright:  Astronomical Institute
!             University of Bern
!             Switzerland
! -------------------------------------------------------------------------

! Modules
! -------
  USE m_bern,   ONLY: i4b, fileNameLength
  USE m_global, ONLY: maxprn

  USE d_satfil, ONLY: t_satfil

  IMPLICIT NONE

! Declare access rights
  PRIVATE
  PUBLIC  :: gtsatr, gtsatr_init

! Local index varaiables
! ----------------------
  TYPE t_index
    LOGICAL      :: fixed
    INTEGER(i4b) :: idxSatellite
    INTEGER(i4b) :: idxRPRmodel
  END TYPE

  TYPE(t_index), DIMENSION(maxprn),   SAVE :: prnIdx

! Save the content of the sat-info file
! -------------------------------------
  CHARACTER(LEN=fileNameLength),      SAVE :: filename = ''  ! Filename
  TYPE(t_satfil),                     SAVE :: SATFIL         ! satellite description

  LOGICAL, ALLOCATABLE, DIMENSION(:), SAVE :: prtWarn        ! warning for sat.



CONTAINS


! -------------------------------------------------------------------------
! Subroutine to return the parameters of radiation pressure model
! -------------------------------------------------------------------------

SUBROUTINE gtsatr(ISTOP,SVN,TMJD,RPRMOD,SVNNR,IBLOCK,MASS,ANLTYP,PLANE,  &
                  formf,C_rpr,admod,adrag)

! Modules
! -------
  USE m_bern,   ONLY: i4b, r8b, fileNameLength, timStrgLength, &
                      lfnErr
  USE d_satfil, ONLY: t_rprmod

  USE s_exitrc
  IMPLICIT NONE

! List of Parameters
! ------------------
! input:
  INTEGER(i4b)                    :: ISTOP   ! IF NO MODEL FOUND:
                                             !         0 REUTRN
                                             !         1 WARNING
                                             !         2 STOP WITH ERROR
  INTEGER(i4b)                    :: SVN     ! PRN number of satellite
  REAL(r8b)                       :: TMJD    ! Epoch in MJD


! output:
  TYPE(t_RPRMOD)                  :: RPRMOD  ! Satellite description
  INTEGER(i4b)                    :: IBLOCK  ! Block number
  CHARACTER(LEN=4)                :: SVNNR   ! Space vehicle number
  REAL(r8b)                       :: MASS    ! Mass of satellite
  CHARACTER(LEN=8)                :: ANLTYP
  CHARACTER(LEN=3)                :: PLANE   ! plane number
  REAL(r8b)                       :: formf   ! area/mass
  REAL(r8b)                       :: C_rpr   ! radiation pressure factor
  INTEGER(i4b)                    :: admod   ! model for air drag/albedo
  REAL(r8b)                       :: adrag   ! coefficient for air drag


! Local Parameters
! ----------------

! List of functions
! -----------------

! Local Variables
! ---------------
  INTEGER(i4b)                    :: isat,jsat


! If called for the first time, read the entire satellite file SATELL
! -------------------------------------------------------------------
  IF (LEN_TRIM(filename) == 0) CALL gtsatr_read()


! FIND SATELLITE INDEX FOR SATELLITE PARAMETER ARRAYS
! ---------------------------------------------------
  CALL gtsatr_makeIdx(iStop, svn, tmjd)
  isat = prnIdx(svn)%idxSatellite
  jsat = prnIdx(svn)%idxRPRmodel


! Return the satellite related information (first part)
! -----------------------------------------------------
  IF (isat > 0) THEN
     anltyp = satfil%rpmodel
     svnnr  = satfil%satellite(isat)%svnnr
     iblock = satfil%satellite(isat)%iblock
     mass   = satfil%satellite(isat)%mass
     plane  = satfil%satellite(isat)%plane

     formf  = satfil%satellite(isat)%formf
     C_rpr  = satfil%satellite(isat)%radpres
     admod  = satfil%satellite(isat)%admodel
     adrag  = satfil%satellite(isat)%adrag
  END IF


! Return the RPR model related information (third part)
! -----------------------------------------------------
  IF (jsat > 0) THEN
    rprmod = satfil%rprmod(jsat)
  ELSE
    rprmod%svn      = svn
    rprmod%timint%t = (/ 0d0,1d20 /)
    rprmod%rpmodel  = 0
    rprmod%nrprcoe  = 0
    NULLIFY(rprmod%rprcoe)
  END IF

  RETURN

END SUBROUTINE gtsatr



! -------------------------------------------------------------------------
! Reading the satellite information file into the local buffer
! -------------------------------------------------------------------------
SUBROUTINE gtsatr_read()

! Modules
! -------
  USE m_bern,    ONLY: i4b
  USE d_satfil,  ONLY: init_satfil
  USE s_gtflna
  USE s_rdsatfil
  USE s_alcerr

  IMPLICIT NONE

! Local Variables
! ---------------
  INTEGER(i4b)                    :: irc

! Get the satellite info file name
! --------------------------------
  CALL gtflna(1,'SATELL ',filename,irc)

! Read satellite info file (SATELL)
! ---------------------------------
  CALL init_satfil(satfil)
  CALL rdsatfil(filename,satfil)

! Init warning statistics
! -----------------------
  ALLOCATE(prtWarn(satfil%nsatellite),stat=irc)
  CALL alcerr(irc, 'prtWarn', (/satfil%nrprmod/), 'gtsatr')
  prtWarn=.TRUE.

! Init satellite index
! --------------------
  prnIdx(:)%fixed        = .FALSE.
  prnIdx(:)%idxSatellite = 0
  prnIdx(:)%idxRPRmodel  = 0

END SUBROUTINE gtsatr_read



! -------------------------------------------------------------------------
! Set the index based on one MJD
! -------------------------------------------------------------------------
SUBROUTINE gtsatr_makeIdx(istop,svn,tmjd)

! Modules
! -------
  USE m_bern,   ONLY: i4b, r8b, timStrgLength2, lfnErr

  USE s_timst2
  USE s_exitrc

  IMPLICIT NONE

! List of parameters
! ------------------
  INTEGER(i4b)                    :: ISTOP   ! IF NO MODEL FOUND:
                                             !         0 REUTRN
                                             !         1 WARNING
                                             !         2 STOP WITH ERROR
  INTEGER(i4b)                    :: SVN     ! PRN number of satellite
  REAL(r8b)                       :: TMJD    ! Epoch in MJD


! Local Variables
! ---------------
  INTEGER(i4b)                    :: ii,isat,jsat

  CHARACTER(LEN=timStrgLength2)   :: timstr


! If called for the first time, read the entire satellite file SATELL
! -------------------------------------------------------------------
  IF (LEN_TRIM(filename) == 0) CALL gtsatr_read()

! Nothing to search
! -----------------
  IF ( prnIdx(svn)%fixed ) RETURN

! Search through the first section of the satellite information file
! ------------------------------------------------------------------
  isat = -1
  DO ii=1,satfil%nsatellite
     IF ((satfil%satellite(ii)%svn == svn).AND.           &
          (TMJD >= satfil%satellite(ii)%timint%t(1)-1d0/86400d0).AND. &
          (TMJD <= satfil%satellite(ii)%timint%t(2)+2d0/86400d0)) THEN
        isat = ii
        EXIT
     END IF
  END DO
  IF (isat == -1.AND.istop > 0) THEN
     CALL timst2(1,1,tmjd,timstr)
     WRITE(lfnerr,'(/,A,/,A,I4,/,A,A,/)')                      &
       ' *** SR GTSATR: Could not read satellite description', &
       '                for satellite:   ', svn,               &
       '                Requested Epoch: ', TRIM(timstr)
     CALL exitrc(2)
  ENDIF

  IF ( prnIdx(svn)%idxSatellite > 0 .AND. isat > 0 .AND. &
       isat /= prnIdx(svn)%idxSatellite ) THEN
    WRITE(lfnerr,'(/,A,/,16X,A,/,16X,A,A,/,16X,A,I5,/,16X,A,2I5)')               &
    ' *** SR GTSATR: The satellite is changing its configuration in the first',  &
                    'section of the satellite information file.',                &
                    'Satellite info file:',TRIM(filename),                       &
                    'Satellite (PRN):    ',svn,                                  &
                    'Index 1/2:          ',isat,prnIdx(svn)%idxSatellite
    CALL timst2(1,1,satfil%rprmod(isat)%timint%t,timstr)
    WRITE(lfnerr,'(16X,A,A)') 'Interval 1:         ',timstr
    CALL timst2(1,1,satfil%rprmod(prnIdx(svn)%idxSatellite)%timint%t,timstr)
    WRITE(lfnerr,'(16X,A,A,/)') 'Interval 2:         ',timstr
    CALL exitrc(2)
  ENDIF


! Search through the third section of the satellite information file
! ------------------------------------------------------------------
  jsat = -1
  DO ii=1,satfil%nrprmod
     IF ((satfil%rprmod(ii)%svn == svn).AND.           &
          (TMJD >= satfil%rprmod(ii)%timint%t(1)-1d0/86400d0).AND. &
          (TMJD <= satfil%rprmod(ii)%timint%t(2)+2d0/86400d0)) THEN
        jsat = ii
        EXIT
     END IF
  END DO

  IF (jsat == -1) THEN
     IF (ISTOP==2) THEN
        CALL timst2(1,1,tmjd,timstr)
        WRITE(lfnerr,'(/,A,/,A,I4,/,A,A,/)')                               &
          ' *** SR GTSATR: Could not read radiation pressure parameters ', &
          '                for satellite:   ', svn,                        &
          '                Requested Epoch: ', TRIM(timstr)
        CALL exitrc(2)
     END IF
     IF (ISTOP==1.AND.prtwarn(isat)) THEN
        CALL timst2(1,1,tmjd,timstr)
        WRITE(lfnerr,'(/,A,/,A,I4,/,A,A,/)')                               &
          ' ### SR GTSATR: Could not read radiation pressure parameters ', &
          '                for satellite:   ', svn,                        &
          '                Requested Epoch: ', TRIM(timstr)
        prtwarn(isat) = .FALSE.
     END IF
  ENDIF

  IF ( prnIdx(svn)%idxRPRmodel > 0 .AND. jsat > 0 .AND. &
       jsat /= prnIdx(svn)%idxRPRmodel ) THEN
    WRITE(lfnerr,'(/,A,/,16X,A,/,16X,A,A,/,16X,A,I5,/,16X,A,2I5)')               &
    ' *** SR GTSATR: The satellite is changing its configuration in the third',  &
                    'section of the satellite information file.',                &
                    'Satellite info file:',TRIM(filename),                       &
                    'Satellite (PRN):    ',svn,                                  &
                    'Index 1/2:          ',jsat,prnIdx(svn)%idxRPRmodel
    CALL timst2(1,1,satfil%rprmod(jsat)%timint%t,timstr)
    WRITE(lfnerr,'(16X,A,A)') 'Interval 1:         ',timstr
    CALL timst2(1,1,satfil%rprmod(prnIdx(svn)%idxRPRmodel)%timint%t,timstr)
    WRITE(lfnerr,'(16X,A,A,/)') 'Interval 2:         ',timstr
    CALL exitrc(2)
  ENDIF

  prnIdx(svn)%idxSatellite = iSat
  prnIdx(svn)%idxRPRmodel  = jSat

END SUBROUTINE gtsatr_makeIdx



! -------------------------------------------------------------------------
! Set the index based on two MJD (interval)
! -------------------------------------------------------------------------
SUBROUTINE gtsatr_init(istop,svn,tint,window,irCode)

! Modules
! -------
  USE m_bern,   ONLY: i4b, r8b, timStrgLength, lfnErr

  USE s_timst2
  USE s_exitrc

  IMPLICIT NONE

! List of parameters
! ------------------
  INTEGER(i4b)                    :: ISTOP   ! IF NO MODEL FOUND:
                                             !         0 RETURN
                                             !         1 WARNING
                                             !         2 STOP WITH ERROR
  INTEGER(i4b)                    :: SVN     ! PRN number of satellite
                                             ! Intervals in MJD:
  REAL(r8b), DIMENSION(2)         :: TINT    ! covered with data from TAB/PRE files
  REAL(r8b), DIMENSION(2)         :: WINDOW  ! covered by the orbital arc
  INTEGER(i4b)                    :: irCode  ! Return code:
                                             ! 0: OK
                                             ! 1: error

! Local variables
! ---------------
  INTEGER(i4b)                    :: isat, jsat

! Init variables
! --------------
  irCode = 1

! Check the Windows boundaries (should not be in different intervals of SAT-INFO)
! -------------------------------------------------------------------------------
  CALL gtsatr_makeIdx(0,     svn, window(1))
  CALL gtsatr_makeIdx(0,     svn, window(2))

! Check lower boundary of data interval (has to be found in SAT-INFO)
! -------------------------------------------------------------------
  CALL gtsatr_makeIdx(istop, svn, tint(1))
  iSat = prnIdx(svn)%idxSatellite
  jSat = prnIdx(svn)%idxRPRmodel
  if ( iSat > 0 .AND. jSat > 0 ) THEN

! Check upper boundary of data interval
! (has to be found in the same interval of SATINFO)
! -------------------------------------------------------------------
    CALL gtsatr_makeIdx(iStop, svn, tint(2))
    iSat = prnIdx(svn)%idxSatellite
    jSat = prnIdx(svn)%idxRPRmodel
    if ( iSat > 0 .AND. jSat > 0 ) THEN
      irCode = 0
      prnIdx(svn)%fixed = .TRUE.
    ENDIF
  ENDIF

END SUBROUTINE gtsatr_init

END MODULE
