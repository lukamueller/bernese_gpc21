MODULE s_CMPRDOPT
CONTAINS


! -------------------------------------------------------------------------
! Bernese GPS Software Version 5.2
! -------------------------------------------------------------------------

SUBROUTINE cmprdopt(nFil,filnam,nflag,flags,bslfil,iopbas,estVelo,opt)

! -------------------------------------------------------------------------
! Purpose:    This routine reads the input options of the program COMPAR2
!
! Author:     R. Dach
!
! Created:    16-Mar-2016
!
! Changes:
!
! Copyright:  Astronomical Institute
!             University of Bern
!             Switzerland
! -------------------------------------------------------------------------

  USE m_bern,   ONLY: r8b, i4b, lfnerr, lfnprt, &
                      keyValueLength, fileNameLength, staNameLength, staFlaglength
  USE p_addneq, ONLY: t_opt, staInfo, prtCrx
  USE d_stacrx, ONLY: init_stacrux

  USE s_alcerr
  USE s_gtfile2
  USE s_readkeys
  USE s_gtflna
  USE s_ckoptc
  USE s_ckoptl
  USE s_ckopti
  USE s_ckoptb
  USE s_ckoptr
  USE s_ckoptz
  USE s_getco3
  USE s_readcrux
  USE s_exitrc
  IMPLICIT NONE

! List of Parameters
! ------------------
  INTEGER(i4b)                 :: nFil    ! Number of files
  CHARACTER(LEN=fileNameLength),&
    DIMENSION(:,:), POINTER    :: filNam  ! File names (1: crd, 2: cov)
  INTEGER(i4b)                 :: nflag   ! number of flags
  CHARACTER(LEN=staFlagLength), &
    DIMENSION(:),  POINTER     :: flags   ! flags of coordinates to be
                                          ! included in the comparison
                                             ! @: all
                                             ! #: all non-blank flags
  CHARACTER(LEN=fileNameLength):: bslfil  ! baseline file for repeatability
  INTEGER(i4b)                 :: iopbas  ! baseline repeatability options
                                          ! 0: no baselines
                                          ! 1: baselines in L, B, H, LENGTH
                                          ! 2: baselines in X, Y, Z, LENGTH
  INTEGER(i4b)                 :: estVelo ! Estimate linear velocities
  TYPE(t_opt)                  :: opt     ! Option structure from ADDNEQ2
                                          ! opt%title,
                                          ! opt%plotrs,    opt%coordrs,
                                          ! opt%velors,    opt%staInfo,
                                          ! opt%timRefCrd, opt%ipHelm,
                                          ! opt%minSol,    opt%badSol


! Local Parameters
! ----------------
  CHARACTER(LEN=8), PARAMETER  :: srName = 'cmprdopt'

! Local Variables
! ---------------
  CHARACTER(LEN=keyValueLength), &
    DIMENSION(:), POINTER       :: keyValue
  CHARACTER(LEN=staNameLength),  &
    DIMENSION(:), POINTER       :: cDummy
  CHARACTER(LEN=fileNameLength) :: velFil

  REAL(r8b)                     :: ref1,ref2

  INTEGER(i4b)                  :: nDummy
  INTEGER(i4b)                  :: refHlp
  INTEGER(i4b)                  :: check
  INTEGER(i4b)                  :: ii
  INTEGER(i4b)                  :: flgNum
  INTEGER(i4b)                  :: irc, iac, irCode

! Init variables
! --------------
  irCode = 0
  NULLIFY(keyValue)
  NULLIFY(cDummy)
  opt%prt = 0

! Get the file names
! ------------------
  CALL gtflna(0,'PLOTRS', opt%plotrs, irc)
  CALL gtflna(0,'STAINFO', opt%stacrux, irc)
  CALL gtflna(0,'COORDRS',opt%coordrs,irc)
  CALL gtflna(0,'VELORS', opt%velors, irc)
  estVelo = 1 - irc

  CALL gtfile2('COOFIL',1,nFil,filnam)

! Read the list of flags
! ----------------------
  CALL readKeys('FLAG', keyValue, irc)
  CALL ckoptc(1,'FLAG',keyValue,(/'ALL     ','NONBLANK','SPECIAL '/), &
              srName, 'Coordinate flags', irc,irCode,                 &
              maxVal = 1, result1 = flgNum)

! Special flags found
! -------------------
  IF (flgNum < 3) THEN
    nFlag = 1

    ALLOCATE(flags(nFlag),stat=irc);
    CALL alcerr(irc,'flags',(/nFlag/),srName)
    flags = ''

    IF (flgNum == 1) flags(1) = '@'
    IF (flgNum == 2) flags(1) = '#'

! A real list of flags
! --------------------
  ELSE IF (flgNum == 3) THEN

    CALL readKeys('FLAGSPEC', keyValue, irc)
    nFlag = 0
    DO ii = 1,SIZE(keyValue)
      IF (LEN_TRIM(keyValue(ii)) > 0) nFlag = nFlag + 1
    ENDDO

    IF (nFlag == 0) THEN
      WRITE(lfnerr,'(/,A,/)') &
      ' ### SR COMPRI: No flags selected. Nothing to do for PGM compar!'
      CALL exitrc(0)
    ENDIF

    ALLOCATE(flags(nFlag),stat=irc);
    CALL alcerr(irc,'flags',(/nFlag/),'compri')

    nFlag = 0
    DO ii = 1,SIZE(keyValue)
      IF (LEN_TRIM(keyValue(ii)) > 0) THEN
        nFlag = nFlag + 1
        flags(nFlag) = keyValue(ii)(1:staFlaglength)
      ENDIF
    ENDDO


! Incorrect flag specification
! ----------------------------
  ELSE
    irCode = irCode + 1
  END IF

! Get the title line
! ------------------
  CALL readKeys('TITLE', keyValue, irc)
  CALL ckoptl(0,'TITLE', keyValue, srName, 'Title', irc, irCode, &
              empty=' ',maxLength=80, maxVal=1,result1=opt%title)

! Baseline repeatibility
! ----------------------
  CALL gtflna(0,'BASDEF',bslfil,iopbas)
  IF ( iopbas /= 0 ) THEN
    iopbas = 0
  ELSE
    CALL readKeys('BASREP', keyValue, irc)

    CALL ckoptc(1,'BASREP', keyValue,                              &
                (/'NO        ','LOCAL     ','GEOCENTRIC'/),        &
                srName,'Repeatability option',irc,irCode,          &
                maxVal = 1, valList=(/0,1,2/),result1=iopbas)
  ENDIF

! Reference coordincate correction
! --------------------------------
  opt%timRefCrd = 0d0
  CALL gtflna(0,'VELAPR',velFil,irc)

  IF (LEN_TRIM(velFil) > 0 .OR. estVelo == 1) THEN
    CALL readKeys('REFEPO', keyValue, irc)

    CALL ckoptc(1,'REFEPO', keyValue,                                      &
               (/'AS_IT_IS','FIRST   ','LAST    ','MEAN    ','MANUAL  '/), &
               srName,'Reference epoch correction',irc,irCode,             &
               maxVal = 1, result1=refHlp)

    ! First
    IF (refHlp == 2) THEN
      CALL getco3(filnam(1,1),1,(/'@'/),nDummy,cDummy,timCrd=opt%timRefCrd)
    ! Last
    ELSE IF (refHlp == 3) THEN
      CALL getco3(filnam(1,nFil),1,(/'@'/),nDummy,cDummy,timCrd=opt%timRefCrd)
    ELSE IF (refHlp == 4) THEN
      CALL getco3(filnam(1,1),1,(/'@'/),nDummy,cDummy,timCrd=ref1)
      CALL getco3(filnam(1,nFil),1,(/'@'/),nDummy,cDummy,timCrd=ref2)
      opt%timRefCrd = (ref1+ref2) / 2d0
    ELSE IF (refHlp == 5) THEN
      CALL readKeys('EPOCH', keyValue, irc)

      CALL ckoptz(1,'EPOCH', keyValue,                                     &
                 srName,'Manually specified reference epoch',irc,irCode,   &
                 maxVal = 1, empty = 0d0, result1=opt%timRefCrd)
    ENDIF
  ENDIF

! Read the station information file (only section 1: station renaming)
! --------------------------------------------------------------------
  CALL init_stacrux(staInfo)
  IF (LEN_TRIM(opt%staCrux) > 0) THEN
    CALL readCrux(opt%staCrux,staInfo)

    DEALLOCATE(staInfo%stainfo, stat=iac)
    DEALLOCATE(staInfo%staprob, stat=iac)
    DEALLOCATE(staInfo%coovel,  stat=iac)
    DEALLOCATE(staInfo%staRelPar,stat=iac)
    DEALLOCATE(staInfo%statype, stat=iac)

    staInfo%ninfo    = 0
    staInfo%nprob    = 0
    staInfo%ncoovel  = 0
    staInfo%nrelpar  = 0
    staInfo%nstatype = 0

    CALL ckoptb(1,(/ 'CRXWARN' /),srName,'Print warning for station info', &
                irCode,result1=opt%prt(prtCrx))
  ENDIF


! Define a bad solution for comparison
! ------------------------------------
  opt%ipHelm(:) = 0
  opt%minSol = 0
  opt%badSol = 0d0
  IF (nFil > 1) THEN
    CALL readKeys('MINSOL', keyValue,irc)
    CALL ckopti(1,'MINSOL', keyValue,srname, &
                'Minimum number of solutions for each station',irc,irCode, &
                maxVal=1,ge=0,empty=0,result1=opt%minSol)

    CALL readKeys('BADSOL_N', keyValue,irc)
    CALL ckoptr(1,'BADSOL_N', keyValue,srname, &
                'Maximum tolerated residual, north',irc,irCode, &
                maxVal=1,ge=0d0,empty=0d0,result1=opt%badSol(1,1))

    CALL readKeys('BADSOL_E', keyValue,irc)
    CALL ckoptr(1,'BADSOL_E', keyValue,srname, &
                'Maximum tolerated residual, east',irc,irCode, &
                maxVal=1,ge=0d0,empty=0d0,result1=opt%badSol(1,2))

    CALL readKeys('BADSOL_U', keyValue,irc)
    CALL ckoptr(1,'BADSOL_U', keyValue,srname, &
                'Maximum tolerated residual, up',irc,irCode, &
                maxVal=1,ge=0d0,empty=0d0,result1=opt%badSol(1,3))

    CALL readKeys('BADSOLRN', keyValue,irc)
    CALL ckoptr(1,'BADSOLRN', keyValue,srname, &
                'Maximum tolerated RMS error, north',irc,irCode, &
                maxVal=1,ge=0d0,empty=0d0,result1=opt%badSol(2,1))

    CALL readKeys('BADSOLRE', keyValue,irc)
    CALL ckoptr(1,'BADSOLRE', keyValue,srname, &
                'Maximum tolerated RMS error, east',irc,irCode, &
                maxVal=1,ge=0d0,empty=0d0,result1=opt%badSol(2,2))

    CALL readKeys('BADSOLRU', keyValue,irc)
    CALL ckoptr(1,'BADSOLRU', keyValue,srname, &
                'Maximum tolerated RMS error, up',irc,irCode, &
                maxVal=1,ge=0d0,empty=0d0,result1=opt%badSol(2,3))
  ENDIF

  IF ( estVelo > 0 .AND. opt%minSol < 2 ) opt%minSol = 2

! Write a priori information (check comparison of solutions)
! ----------------------------------------------------------
  check = 0
  WRITE(lfnprt,'(A,/,A,/)')                       &
    ' Check comparison of individual solutions:', &
    ' ----------------------------------------'

  IF (opt%minSol > 0) THEN
    WRITE(lfnprt,'(A,I6,/)') &
      ' Minimum number of solutions contributed:  ',opt%minSol
    check = 1
  ENDIF
  IF (opt%badSol(1,1) > 0) THEN
    WRITE(lfnprt,'(A,F8.1,A)')   &
      ' Maximum residuals accepted in      north: ',opt%badSol(1,1),' mm'
    check = 1
  ENDIF
  IF (opt%badSol(1,2) > 0) THEN
    WRITE(lfnprt,'(A,F8.1,A)')   &
      ' Maximum residuals accepted in      east:  ',opt%badSol(1,2),' mm'
    check = 1
  ENDIF
  IF (opt%badSol(1,3) > 0) THEN
    WRITE(lfnprt,'(A,F8.1,A,/)') &
      ' Maximum residuals accepted in      up:    ',opt%badSol(1,3),' mm'
    check = 1
  ENDIF
  IF (opt%badSol(2,1) > 0) THEN
    WRITE(lfnprt,'(A,F8.1,A)')   &
      ' Maximum component rms accepted in  north: ',opt%badSol(2,1),' mm'
    check = 1
  ENDIF
  IF (opt%badSol(2,2) > 0) THEN
    WRITE(lfnprt,'(A,F8.1,A)')   &
      ' Maximum component rms accepted in  east:  ',opt%badSol(2,2),' mm'
    check = 1
  ENDIF
  IF (opt%badSol(2,3) > 0) THEN
    WRITE(lfnprt,'(A,F8.1,A)')   &
      ' Maximum component rms accepted in  up:    ',opt%badSol(2,3),' mm'
    check = 1
  ENDIF

  IF (check == 0)  WRITE(lfnprt,'(A)') ' No check required'
  WRITE(lfnprt,'(/)')

  DEALLOCATE(keyValue,stat=irc)

! Problems reading input options
! ------------------------------
  IF (irCode /= 0) THEN
    WRITE(lfnerr,"(/,'  Number of errors: ',I2)") irCode
    CALL exitrc(2)
  END IF

END SUBROUTINE CMPRDOPT


END MODULE s_CMPRDOPT
