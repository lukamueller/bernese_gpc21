      MODULE s_PDTRNS
      CONTAINS

C*
      SUBROUTINE PDTRNS(PARTYP,ICOEF,UNITIN,XSAT,T0,TOBS,ST0,DER)
CC
CC NAME       :  PDTRNS
CC
CC PURPOSE    :  COMPUTE PARTIAL DERIVATIVES OF THE DISTANCE
CC               RECEIVER - SATELLITE  WITH RESPECT TO
CC               THE FOLLOWING PARAMETERS OF THE TRANSFORMATION
CC               BETWEEN THE EARTH FIXED AND THE INERTIAL
CC               COORDINATE SYSTEM:
CC
CC               XP : X-COORDINATE OF THE POLE (UNIT MAS, MAS/DAY, ..)
CC               YP : Y-COORDINATE OF THE POLE (UNIT MAS, MAS/DAY, ..)
CC               DT : UT1-UTC (MSEC, MSEC/DAY, ...)
CC               DEPS : NUTATION IN OBLIQUITY
CC               DPSI : NUTATION IN (ECLIPTIC) LONGITUDE
CC
CC               EACH PARAMETER MAY BE MODELED BY A POLYNOMIAL:
CC
CC               P = P1 + P2*(T-T0) + ...
CC
CC               THE SR RETURNS THE COEFFICIENT PI, WHERE I=ICOEF
CC
CC PARAMETERS :
CC         IN :  PARTYP : 1=XP,2=YP,3=DT,4=DEPS,5=DPSI        I*4
CC               ICOEF  : COEFFICIENT OF TERM PROPORTIONAL    I*4
CC                        (T-T0)**(ICOEF-1)
CC               UNITIN : UNIT VECTOR RECEIVER --> SATELLITE  R*8(*)
CC                        IN "MEAN EQUATOR-EQUINOX SYSTEM"
CC                        OF EPOCH
CC               XSAT   : GEOC. SAT VECTOR IN THE SAME SYSTEM R*8(*)
CC                        AS UNIT
CC               T0     : TIME ORIGIN (MJD)                   R*8
CC               TOBS   : OBSERVATION TIME                    R*8
CC               ST0    : SIDEREAL TIME IN GRRENWICH          R*8
CC        OUT :  DER    : RESULTING DERIVATIVE                R*8(2)
CC
CC REMARKS    :
CC
CC AUTHOR     :  G. BEUTLER
CC
CC VERSION    :  3.5
CC
CC CREATED    :  09-APR-94
CC
CC CHANGES    :  05-JUN-96 : TS: SOME COSMETICS
CC               16-JUN-05 : MM: COMCONST.inc REPLACED BY d_const
CC               23-JUN-05 : MM: IMPLICIT NONE AND DECLARATIONS ADDED
CC               28-FEB-07 : AG: USE 206264... FROM DEFCON
CC
CC COPYRIGHT  :  ASTRONOMICAL INSTITUTE
CC      1992     UNIVERSITY OF BERN
CC               SWITZERLAND
CC
C*
      USE d_const, ONLY: OMEGA, ars
      IMPLICIT NONE
C
C DECLARATIONS INSTEAD OF IMPLICIT
C --------------------------------
      INTEGER*4 ICOEF
C
      REAL*8    CST   , DER   , EPS   , SEPS  , SST   , ST0   , ST0OLD,
     1          T0    , TIMCOE, TOBS  , TU
C
CCC       IMPLICIT REAL*8 (A-H,O-Z)
C
C GLOBAL DECLARATIONS
C -------------------
      INTEGER*4 PARTYP
C
      REAL*8    UNITIN(*),XSAT(*)
C
C INTERNAL DECLARATIONS
C ---------------------
      REAL*8    UNITEF(3)
C
C
      DATA ST0OLD/0.D0/
C
C UNIT VECTOR IN EARTH FIXED SYSTEM :
C ---------------------------------
      IF(ST0OLD.EQ.0.D0)THEN
        TU =(TOBS-51544.5)/36525.D0
        EPS=84381.45-46.815*TU-0.0006*TU*TU+0.00181*TU*TU*TU
        EPS=EPS/ars
        SEPS=DSIN(EPS)
      END IF
C
C DEFINE SIN(EPS), EPS=OBLIQUITY OF ECLIPTIC
      UNITEF(3)=UNITIN(3)
      CST=DCOS(ST0)
      SST=DSIN(ST0)
      UNITEF(1)= CST*UNITIN(1)+SST*UNITIN(2)
      UNITEF(2)=-SST*UNITIN(1)+CST*UNITIN(2)
C
      ST0OLD=ST0
C
C (T-T0)**(ICOEF-1)
C -----------------
      IF (ICOEF-1.EQ.0) THEN
        TIMCOE=1.D0
      ELSE
        IF (DABS(TOBS-T0).LT.1.D-7) THEN
          TIMCOE=0.D0
        ELSE
          TIMCOE=(TOBS-T0)**(ICOEF-1)
        END IF
      END IF
C
C COMPUTE PARTIALS FOR THE 5 CASES
C --------------------------------
      IF(PARTYP.EQ.1)THEN
C
C 1. DERIVATIVE WITH RESPECT TO THE X-COORDINATE OF THE POLE
        DER= XSAT(3)*UNITEF(1)-(CST*XSAT(1)+SST*XSAT(2))*UNITEF(3)
        DER=DER/ars/1.D3
      ELSE IF(PARTYP.EQ.2)THEN
C
C 2. DERIVATIVE WITH RESPECT TO THE Y-COORDINATE OF THE POLE
        DER=-XSAT(3)*UNITEF(2)+(-SST*XSAT(1)+CST*XSAT(2))*UNITEF(3)
        DER=DER/ars/1.D3
      ELSE IF(PARTYP.EQ.3)THEN
C
C 3. DERIVATIVE WITH RESPECT TO THE SIDEREAL TIME
        DER=(CST*XSAT(2)-SST*XSAT(1))*UNITEF(1)-
     1      (SST*XSAT(2)+CST*XSAT(1))*UNITEF(2)
        DER=DER*OMEGA/1000.0
      ELSE IF(PARTYP.EQ.4)THEN
C
C 4. DERIVATIVE WITH RESPECT TO THE NUTATION IN OBLIQUITY
        DER=-(UNITEF(1)*SST+UNITEF(2)*CST)*XSAT(3)+XSAT(2)*UNITEF(3)
        DER=DER/ars/1.D3
      ELSE IF(PARTYP.EQ.5)THEN
C
C 5. DERIVATIVE WITH RESPECT TO THE NUTATION IN (ECLIPTICAL) LONGITUDE
        DER= (-UNITEF(1)*CST+UNITEF(2)*SST)*XSAT(3)+XSAT(1)*UNITEF(3)
        DER=SEPS*DER
        DER=DER/ars/1.D3
      END IF
C
C MULTIPLY WITH TIME ARGUMENT :
C ---------------------------
      DER=TIMCOE*DER
C
      RETURN
      END SUBROUTINE

      END MODULE
