MODULE f_EXTRINT
CONTAINS

! -------------------------------------------------------------------------
! Bernese GNSS Software Version 5.2
! -------------------------------------------------------------------------

FUNCTION extrint(string,size)

! -------------------------------------------------------------------------
! Purpose:    Extract an integer from a string - from the right.
!
! Author:     S. Lutz, R. Dach
!
! Created:    04-Oct-2010
!
! Changes:    16-Sep-2011 SL: if condition expanded
!             06-Dec-2013 SL: extrInt==undef_i not allowed
!             19-Aug-2014 SL: If string equal undef_i return undef_i
!
! Copyright:  Astronomical Institute
!             University of Bern
!             Switzerland
! -------------------------------------------------------------------------

! Modules
! -------
  USE m_bern,   ONLY: i4b, lfnErr
  USE d_stacrx, ONLY: undef_i

! List of Parameters
! ------------------
! Input:
  CHARACTER(LEN=*)        :: string
  INTEGER(i4b),OPTIONAL   :: size

! Output:
  INTEGER(i4b)            :: extrInt

! Variables
! ---------
  INTEGER(i4b)            :: i,j
  INTEGER(i4b)            :: x

! Check if string == undef_i
! --------------------------
  IF(string(1:7) .EQ. '999999 ') THEN
    extrInt = undef_i
    RETURN
  ENDIF

! Extract integer from string
! ---------------------------
  extrInt = 0
  j = 1
  DO i = LEN_TRIM(string),1,-1
    x = IACHAR(string(i:i))
    IF(x>47.AND.x<58) THEN
      extrInt = extrInt+(x-48)*j
      IF(extrInt == undef_i) THEN
        WRITE(lfnErr, &
          "(/,' ### SR EXTRINT: ', &
          &       'Integer is undef_i. Trying again...', &
          & /,17X,'String : ',A, &
          & /)") TRIM(string)
        extrInt = extrInt-(x-48)*j
        j = j/10;
      ENDIF
      j = j*10
      IF(PRESENT(size)) THEN
        IF(j == 10**size) EXIT
      ENDIF
    ENDIF
  ENDDO

END FUNCTION extrint

END MODULE
