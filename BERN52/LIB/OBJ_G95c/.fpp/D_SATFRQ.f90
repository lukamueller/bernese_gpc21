! ------------------------------------------------------------------------------
! Bernese GPS Software Version 5.1
! ------------------------------------------------------------------------------

MODULE d_satfrq

! ------------------------------------------------------------------------------
! Purpose:    This module defines satellite frequencies from COMFRQ.inc
!
! Author:     R. Dach
!
! Created:    17-Okt-2001
!             __-___-____
!
! Changes:    __-___-____ __:
!
!
! Copyright:  Astronomical Institute
!              University of Bern
!                  Switzerland
! ------------------------------------------------------------------------------

  USE m_bern
!  IMPLICIT NONE

  INCLUDE 'COMFREQ.inc'

END MODULE d_satfrq
