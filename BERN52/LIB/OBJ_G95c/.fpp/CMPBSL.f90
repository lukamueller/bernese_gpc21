MODULE s_CMPBSL
CONTAINS


! -------------------------------------------------------------------------
! Bernese GPS Software Version 5.2
! -------------------------------------------------------------------------

SUBROUTINE cmpbsl(iopbas,nstat,stname,staIndex,nfil,yyddds,neq)

! -------------------------------------------------------------------------
! Purpose:    This routine writes the baseline statistics for program COMPAR2
!
! Author:     R. Dach
!
! Created:    16-Mar-2016
!
! Changes:
!
! Copyright:  Astronomical Institute
!             University of Bern
!             Switzerland
! -------------------------------------------------------------------------

  USE m_bern,   ONLY: r8b, i4b, lfnprt, lfnerr, lfnloc, &
                      fileNameLength, staNameLength
  USE d_const,  ONLY: ae
  USE d_datum,  ONLY: datum
  USE d_neq,    ONLY: t_neq

  USE s_alcerr
  USE s_gtflna
  USE s_opnfil
  USE s_opnerr
  USE s_exitrc
  USE s_xyzell
  USE f_lincount
  IMPLICIT NONE

! List of Parameters
! ------------------
  INTEGER(i4b)                    :: iopbas   ! baseline repeatability options
                                              ! 0: no baselines
                                              ! 1: baselines in L, B, H, LENGTH
                                              ! 2: baselines in X, Y, Z, LENGTH
  INTEGER(i4b)                    :: nstat    ! Number of stations
  CHARACTER(LEN=staNameLength),    &
      DIMENSION(:)                :: stname   ! List of station names
  INTEGER(i4b), DIMENSION(:,:)    :: staIndex ! Index into the NEQs (x-coord)
                                              ! usage: staIndex(ista,ifil)
                                              ! ifil = 0: index for neq_1
  INTEGER(i4b)                    :: nfil     ! Number of input files
  CHARACTER(LEN=6),DIMENSION(:)   :: yyddds   ! Epoch string for each file
  TYPE(t_neq),  DIMENSION(:), POINTER :: neq  ! Input coordinates in NEQ structure


! Local Parameters
! ----------------
  CHARACTER(LEN=6), PARAMETER  :: srName = 'cmpbsl'

! Local Variables
! ---------------
  CHARACTER(LEN=fileNameLength)   :: bslfil
  CHARACTER(LEN=staNameLength),    &
      DIMENSION(:,:), ALLOCATABLE :: staBsl
  CHARACTER(LEN=4)                :: lcode

  INTEGER(i4b)                    :: maxbsl
  INTEGER(i4b)                    :: ibsl, jbsl
  INTEGER(i4b)                    :: nbsl
  INTEGER(i4b)                    :: ircbsl
  INTEGER(i4b)                    :: istat1, istar2, istat2
  INTEGER(i4b)                    :: ifil
  INTEGER(i4b),    DIMENSION(nfil):: basfil
  INTEGER(i4b)                    :: ipar1, ipar2
  INTEGER(i4b)                    :: ibase
  INTEGER(i4b)                    :: nbase
  INTEGER(i4b)                    :: ii, kk
  INTEGER(i4b)                    :: ios, iac

  REAL(r8b)                       :: bassum
  REAL(r8b)                       :: basdif
  REAL(r8b)                       :: basrms
  REAL(r8b)                       :: basrpp
  REAL(r8b)                       :: basppm
  REAL(r8b),   DIMENSION(nfil)    :: baslen
  REAL(r8b),   DIMENSION(3)       :: posme1
  REAL(r8b),   DIMENSION(3,nfil)  :: posdif
  REAL(r8b),   DIMENSION(3)       :: possig
  REAL(r8b),   DIMENSION(3)       :: xstell1, xstell2

! Get baseline definition file name
! ---------------------------------
  nbsl = 0
  CALL gtflna(0,'BASDEF ',bslfil,ircbsl)
  IF (ircbsl == 0) THEN

    maxbsl=LINCOUNT(bslfil,0)
    ALLOCATE(stabsl(2,maxbsl),stat=iac)
    CALL alcerr(iac,'stabsl',(/2,maxbsl/),srname)

    CALL opnfil(lfnloc,bslfil,'OLD','FORMATTED',' ',' ',ios)
    CALL opnerr(lfnerr,lfnloc,ios,bslfil,srName)

! Read baselines to be printed
    DO ibsl=1,maxbsl
      READ(lfnloc,'(A16,1X,A)',iostat=ios) (stabsl(kk,ibsl),kk=1,2)

      IF ( ios /= 0 ) THEN
        WRITE(lfnerr,'(/,A,/,16X,2A,/,16X,A,I5,/)') &
        ' *** SR CMPBSL: ERROR READING BASELINE DEFINITION FILE', &
                        'BASELINE DEF. FILE: ',bslfil,            &
                        'LINE NUMBER       :',ibsl
        CALL exitrc(2)
      ENDIF

      nbsl=ibsl
      IF (stabsl(1,ibsl) == ' ') EXIT
    ENDDO

    CLOSE(unit=lfnloc)
  ENDIF


! Write baseline length title
! ---------------------------
  WRITE(lfnprt,'(//,1X,131("-"),/,1X,A,/,1X,131("-"),/)') &
               'Comparison of baseline lengths (reference: first station)'

! Loop over all combinations of baselines
! ---------------------------------------
  DO istat1=1,nstat

! Set variable of second station loop
    IF (ircbsl == 0) THEN
      istar2=1
    ELSE
      istar2=istat1+1
    ENDIF

    DO istat2=istar2,nstat

      ! Check baseline def.file
      IF (ircbsl == 0) THEN
        IF (istat1 == istat2) CYCLE

        jBsl = 0
        DO ibsl = 1,nbsl
          IF (stabsl(1,ibsl) == stname(istat1) .AND. &
              stabsl(2,ibsl) == stname(istat2)) THEN
            jBsl = ibsl
            EXIT
          ENDIF
        ENDDO
        IF ( jBsl == 0 ) CYCLE
      ENDIF

      ! Compute the coordinate difference from each file
      nbase=0
      bassum=0.d0
      DO iFil = 1,nFil
        ipar1 = staIndex(istat1,iFil+1)
        ipar2 = staIndex(istat2,iFil+1)
        IF ( ipar1 * ipar2 == 0 ) CYCLE
        nbase=nbase+1
        basfil(nbase)=ifil

        ! Distinguish the two system (blh, xyz)
        IF (iopbas == 1) THEN
          CALL xyzell(datum%aell,datum%bell,datum%dxell,datum%drell, &
                      datum%scell, neq(iFil)%xxx(ipar1:ipar1+2), &
                      xstell1(1:3))
          CALL xyzell(datum%aell,datum%bell,datum%dxell,datum%drell, &
                      datum%scell, neq(iFil)%xxx(ipar2:ipar2+2), &
                      xstell2(1:3))
          DO ii=1,3
            posdif(ii,nbase) = xstell2(ii)-xstell1(ii)
            IF (ii == 1 ) THEN
              posdif(ii,nbase) = posdif(ii,nbase) * ae
            ELSEIF( ii == 2 ) THEN
              posdif(ii,nbase) = posdif(ii,nbase) * ae * DCOS(XSTELL1(1))
            ENDIF
          ENDDO
        ELSE
          DO ii=0,2
            posdif(ii+1,nbase)=neq(iFil)%xxx(ipar2+ii) - neq(iFil)%xxx(ipar1+ii)
          ENDDO
        ENDIF

        ! Update the statistics on the baseline length
        baslen(nbase)=0.d0
        DO ii=0,2
          baslen(nbase) = baslen(nbase) + &
                        ( neq(iFil)%xxx(ipar2+ii) - neq(iFil)%xxx(ipar1+ii))**2
        ENDDO
        baslen(nbase)=DSQRT(baslen(nbase))
        bassum=bassum+baslen(nbase)
      ENDDO

      ! Check number of baseline length obtained
      IF (nbase < 2) CYCLE


! Compute mean and rms for baseline length, lat, long, and height
! ---------------------------------------------------------------
      bassum=bassum/nbase

      ! L,B,H / X,Y,Z - DIFFERENCE OF COMBINED SOLUTION
      DO ii = 1,3
        posme1(ii)=0.D0
        DO kk=1,nbase
          posme1(ii)=posme1(ii)+posdif(ii,kk)/nbase
        ENDDO
      ENDDO

      basrms=0.d0
      DO ibase=1,nbase
        basrms = basrms + (baslen(ibase)-bassum)**2
      ENDDO

      DO ii = 1,3
        possig(ii)=0.d0
        DO kk = 1,nbase
          possig(ii)=possig(ii)+(posdif(ii,kk)-posme1(ii))**2
        ENDDO
        IF (nbase > 1) THEN
          possig(ii)=DSQRT(possig(ii)/(nbase-1))
        ELSE
          possig(ii)=0.d0
        ENDIF
      ENDDO

      IF (nbase > 1) THEN
        basrms = DSQRT(basrms/(nbase-1))
      ELSE
        basrms = 0.d0
      ENDIF

      ! Print baseline length and deviation from mean length
      DO ibase = 1,nbase
        basdif=baslen(ibase)-bassum

        IF (baslen(ibase) /= 0.D0) THEN
          basppm=basdif/baslen(ibase)*1.d6
        ELSE
          basppm=0.d0
        ENDIF

        IF (iopbas == 1) THEN
          IF(ibase == 1) THEN
            WRITE(lfnprt,'(1X,A,A16,A,A16,/,1X,131("-"),/,2A,/)') &
            'Baseline : ',stname(istat1),'  to  ',stname(istat2), &
            ' File    Base.length    D(lat)    D(lon)    D(hgt)    D(l', &
            'gt) D(lgt)(ppm) code   yyddds   Station 1        Station 2'
          ENDIF
          lcode='_bl_'
          IF( yyddds(basfil(ibase)) == 'yyddds') lcode='_xx_'
          WRITE(lfnprt,'(1X,I4,F15.5,4F10.5,F9.3,4X,A4,3X,A6,3X,A16,1X,A16)', &
               iostat=ios) basfil(ibase),baslen(ibase),              &
               posdif(1,ibase)-posme1(1),posdif(2,ibase)-posme1(2),  &
               posdif(3,ibase)-posme1(3),basdif,basppm,lcode,        &
               yyddds(basfil(ibase)),stname(istat1),stname(istat2)
        ELSE
          IF(ibase == 1) THEN
            WRITE(lfnprt,'(1X,A,A16,A,A16,/,1X,131("-"),/,3A,/)') &
            'Baseline : ',stname(istat1),'  to  ',stname(istat2), &
            ' File         DX(m)      DDX(m)         DY(m)      DDY(m)  ', &
            '       DZ(m)      DDZ(m)     Base.length    DDL(m)   DDL(pp', &
            'm)   code   yyddds   Station 1        Station 2'
            END IF
          lcode='_bl_'
          IF( yyddds(basfil(ibase)) == 'yyddds') lcode='_xx_'
          WRITE(lfnprt,'(1X,I4,4(F16.5,F10.5),F10.5,4X,A4,3X,A6,3X,A16,1X,A16)',&
          iostat=ios) basfil(ibase), &
          posdif(1,ibase),posdif(1,ibase)-posme1(1), &
          posdif(2,ibase),posdif(2,ibase)-posme1(2), &
          posdif(3,ibase),posdif(3,ibase)-posme1(3), &
          baslen(ibase),basdif,basppm,lcode,yyddds(basfil(ibase)), &
          stname(istat1),stname(istat2)
        ENDIF
      ENDDO

      ! Print mean baseline length and rms
      IF (bassum /= 0.D0) THEN
        basrpp=basrms/bassum*1.d6
      ELSE
        basrpp=0.d0
      ENDIF

      IF (iopbas == 1) THEN
        WRITE(lfnprt,&
        '(1X,131("-"),/,1X,I4,F15.5,4F10.5,F9.3,4X,"_TT_",12X,A16,1X,A16,/)', &
        iostat=ios)nbase,bassum,(possig(kk),kk=1,3),basrms,basrpp, &
        stname(istat1),stname(istat2)
      ELSE
        WRITE(lfnprt, &
        '(1X,131("-"),/,1X,I4,4(F16.5,F10.5),F10.5,4X,"_TT_",12X,A16,1X,A16,/)', &
        iostat=ios) nbase,(posme1(kk),possig(kk),kk=1,3),bassum,basrms,basrpp, &
        stname(istat1),stname(istat2)
      ENDIF

! Next baseline
    ENDDO
  ENDDO

! Print final line
! ----------------
  WRITE(lfnprt,'(/,1X,131("-"),//)')

! Deallocate the memory
! ---------------------
  DEALLOCATE(stabsl,stat=iac)

END SUBROUTINE CMPBSL


END MODULE s_CMPBSL
