      MODULE s_RXRDOR
      CONTAINS

C*
      SUBROUTINE RXRDOR(LFNOBS,LFNERR,MAXSAT,
     1                  NUMTYP,EPOCH,IFLAG,NSATEP,SATEPO,
     2                  OBSEPO,ISIGN,LLI,IRCODE)
CC
CC NAME       :  RXRDOR
CC
CC PURPOSE    :  READ  OBSERVATION RECORDS OF A
CC               RINEX OBSERVATION FILE
CC
CC PARAMETERS :
CC         IN :  LFNOBS : LOGICAL FILE NUMBER                  I*4
CC               LFNERR : LFN FOR ERROR MESSAGES               I*4
CC               MAXSAT : MAXIMUM NUMBER OF SATELLITES         I*4
CC                        -> CORRESPONDS TO ROW DECLARATIONS OF
CC                           ARRAYS OBSEPO,SIGNAL,LLI
CC               NUMTYP : NUMBER OF DIFFERENT OBS.TYPES        I*4
CC        OUT :  EPOCH  : OBSERVATION EPOCH (RECEIVER TIME)    R*8
CC               IFLAG  : FLAG FOR CURRENT RECORD              I*4
CC               NSATEP : NUMBER OF SATELLITES                 I*4
CC               SATEPO : LIST OF SATELLITE NUMBERS            I*4(*)
CC               OBSEPO : LIST OF OBSERVATIONS                 R*8(*,*)
CC                        OBSEPO(I,J), I: SATELLITE, J: OBS.TYPE
CC               ISIGN  : S/N RATIOS                           I*4(*,*)
CC                        ISIGN (I,J), I: SATELLITE, J: OBS.TYPE
CC               LLI    : LOSS OF LOCK INDICATORS              I*4(*,*)
CC                        LLI   (I,J), I: SATELLITE, J: OBS.TYPE
CC               IRCODE : RETURN CODE                          I*4
CC                        0: OK
CC                        3: END OF FILE WITHIN OBS.RECORD
CC                        4: ERROR DECODING DATA
CC                        5: START OF NEW HEADER FOUND
CC                        9: END OF FILE
CC
CC REMARKS    :  ---
CC
CC AUTHOR     :  W. GURTNER
CC
CC VERSION    :  3.4  (JAN 93)
CC
CC CREATED    :  89/04/07 08:00
CC
CC CHANGES    :  01-JUL-99 : PF: CALL IYEAR4 FOR CONVERSION YY->YYYY
CC               23-JUN-05 : MM: IMPLICIT NONE AND DECLARATIONS ADDED
CC
CC COPYRIGHT  :  ASTRONOMICAL INSTITUTE
CC      1989     UNIVERSITY OF BERN
CC               SWITZERLAND
CC
C*
      USE f_djul
      USE f_iyear4
      IMPLICIT NONE
C
C DECLARATIONS INSTEAD OF IMPLICIT
C --------------------------------
      INTEGER*4 I     , IDAY  , IFLAG , IHOUR , IRCODE, IYEAR ,
     1          J     , K     , LFNERR, LFNOBS, MAXSAT, MINUTE, MONTH ,
     2          NSATEP, NUMTYP
C
      REAL*8    DAY   , EPOCH , SEC
C
CCC       IMPLICIT REAL*8 (A-H,O-Z)
C
C GLOBAL DECLARATIONS
C -------------------
      INTEGER*4    SATEPO(*),LLI(MAXSAT,*),ISIGN(MAXSAT,*)
      REAL*8       OBSEPO(MAXSAT,*)
C
C  LOCAL DECLARATIONS
C  ------------------
      CHARACTER    STRING*80
C
C RECORD 1        EPOCH/SAT
200   READ(LFNOBS,222,END=990) STRING
222   FORMAT(A80)
      IF(STRING.EQ.' ') GOTO 200
      IF(STRING(61:65).EQ.'RINEX') GOTO 950
      READ(STRING,1,ERR=940) IYEAR,MONTH,IDAY,IHOUR,MINUTE,SEC,IFLAG,
     1                       NSATEP,(SATEPO(K),K=1,NSATEP)
1     FORMAT(5I3,F11.7,19I3)
      DAY=IDAY+IHOUR/24.D0+MINUTE/1440.D0+SEC/86400.D0
      IYEAR = IYEAR4(IYEAR)
      EPOCH=DJUL(IYEAR,MONTH,DAY)
C
C RECORD 2 FF     OBSERVATIONS
      J=0
      DO 100 I=1,NSATEP
        READ(LFNOBS,222,END=930) STRING
        IF(SATEPO(I).NE.0) THEN
          J=J+1
          READ(STRING,111,ERR=940) (OBSEPO(J,K),LLI(J,K),ISIGN(J,K),
     1                             K=1,NUMTYP)
111       FORMAT(5(F14.3,2I1))
          SATEPO(J)=SATEPO(I)
        END IF
100   CONTINUE
      NSATEP=J
      IF(NSATEP.EQ.0) GOTO 200
C
      IRCODE=0
      GOTO 999
C
C  END OF FILE WITHIN OBS.RECORD
930   IRCODE=3
      WRITE(LFNERR,931)
931   FORMAT(' SR RXRDOR: END OF FILE WITHIN OBS.RECORD')
      GOTO 999
C
C  ERROR DECODING DATA
940   IRCODE=4
      WRITE(LFNERR,941) STRING(1:79)
941   FORMAT(' SR RXRDOR: ERROR DECODING DATA ON THE FOLLOWING LINE:',
     1       /,1X,A)
      GOTO 999
C
C  START OF NEW HEADER FOUND
950   IRCODE=5
      BACKSPACE LFNOBS
      WRITE(LFNERR,951) STRING(1:79)
951   FORMAT(' SR RXRDOR: START OF A NEW HEADER FOUND:',
     1       /,1X,A)
      GOTO 999
C
C  END OF FILE
990   IRCODE=9
      GOTO 999
C
999   RETURN
      END SUBROUTINE

      END MODULE
