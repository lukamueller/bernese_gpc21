      MODULE s_R2RDMH
      CONTAINS
C*
      SUBROUTINE R2RDMH(LFNMET,LFNERR,MAXCOM,PRGNAM,RUNBY,CRDATE,CRTIME,
     1                  NCOM,COMENT,STANAM,NUMTYP,OBSTYP,IRXVRS,IRCODE)
CC
CC NAME       :  R2RDMH
CC
CC PURPOSE    :  READ  THE ENTIRE HEADER INFORMATION OF A
CC               RINEX METEOROLOGICAL DATA FILE
CC
CC PARAMETERS :
CC         IN :  LFNMET : LOGICAL FILE NUMBER                  I*4
CC               LFNERR : LFN FOR ERROR MESSAGES               I*4
CC               MAXCOM : MAXIMUM NUMBER OF COMMENT LINES      I*4
CC        OUT :  PRGNAM : PROGRAM NAME                        CH*20
CC               RUNBY  : NAME OF AGENCY CREATING RINEX FILE  CH*20
CC               CRDATE : CREATION DATE                       CH*9
CC               CRTIME : CREATION TIME                       CH*5
CC               NCOM   : NUMBER OF COMMENT LINES              I*4
CC               COMENT : COMENT LINES                        CH*60(*)
CC               STANAM : STATION NAMES                       CH*60
CC               NUMTYP : NUMBER OF DIFFERENT OBS.TYPES        I*4
CC               OBSTYP : LIST OF OBSERVATION TYPES           CH*2(*)
CC               IRXVRS : RINEX VERSION NUMBER                 I*4
CC               IRCODE : RETURN CODE                          I*4
CC                        0: OK
CC                        1: WRONG FILE TYPE
CC                        2: NOT ANTICIPATED VERSION NUMBER
CC                        3: END OF FILE WITHIN HEADER
CC                        4: ERROR DECODING DATA
CC                        9: END OF FILE AT BEGINNING OF HEADER
CC
CC REMARKS    :  READS VERSION 1 OR VERSION 2 HEADERS
CC
CC AUTHOR     :  W.GURTNER
CC
CC CREATED    :  22-FEB-94
CC
CC CHANGES    :  24-FEB-97 : ??: ERROR MESSAGE FOR ILLEGAL VERSION WAS WRONG
CC               28-MAY-99 : MR: CHANGE R2RDNH TO R2RDMH IN ERROR MESSAGES
CC               01-MAR-02 : DI: ADD ERR= TO ALL READ STATEMENTS
CC               23-JUN-05 : MM: IMPLICIT NONE AND DECLARATIONS ADDED
CC               02-DEC-11 : SL: ACCEPT (BUT DO NOT USE) RINEX 2 ENTRIES
CC
CC COPYRIGHT  :  ASTRONOMICAL INSTITUTE
CC      1989     UNIVERSITY OF BERN
CC               SWITZERLAND
CC
C*
      USE m_bern,    ONLY: i4b, r8b
      USE s_upperc
      IMPLICIT NONE
C
C DECLARATIONS INSTEAD OF IMPLICIT
C --------------------------------
      INTEGER*4 IRCODE, IRVERS, IRXVRS, JNUM  , JPGM  , JSTA  , K     ,
     1          LFNERR, LFNMET, MAXCOM, NCOM  , NUMTYP
C
CCC       IMPLICIT REAL*8 (A-H,O-Z)
C
C GLOBAL DECLARATIONS
C -------------------
      CHARACTER    PRGNAM*20,RUNBY*20,STANAM*60
      CHARACTER    COMENT(*)*60,CRDATE*9,CRTIME*5
      CHARACTER    OBSTYP(*)*2
C
C  LOCAL DECLARATIONS
C  ------------------
      CHARACTER    HEAD*20,RXTYPE*1,STRING*60,RXT*1
C
      INTEGER(i4b), PARAMETER                  :: maxTyp = 10
C
      CHARACTER(LEN=20)                        :: staNum
      CHARACTER(LEN=20), DIMENSION(maxTyp)     :: sMod,sTyp
      REAL(r8b), DIMENSION(maxTyp)             :: sAcc,sX,sY,sZ,sH
      CHARACTER(LEN=2), DIMENSION(maxTyp)      :: sObsM,sObsP
      INTEGER(i4b)                             :: iM,iP
C
C RINEX FILE TYPE
      DATA RXTYPE/'M'/
C
C MAXIMUM RINEX FORMAT VERSION
      DATA IRVERS/2/
C
      NCOM=0
      JPGM=0
      JSTA=0
      JNUM=0
      iM = 1
      iP = 1
C
C RECORD 1        RINEX VERSION / TYPE
101   READ(LFNMET,222,END=990,ERR=940) STRING,HEAD
222   FORMAT(A60,A20)
      CALL UPPERC(STRING)
      CALL UPPERC(HEAD)
      IF(HEAD.NE.'RINEX VERSION / TYPE') GOTO 950
      READ(STRING,1,ERR=940) IRXVRS,RXT
1     FORMAT(I6,14X,A1)
      IF(RXT.NE.RXTYPE) GOTO 910
      IF(IRXVRS.LE.0.OR.IRXVRS.GT.IRVERS) GOTO 920
C
C  LOOP OVER ALL REMAINING LINES
200   READ(LFNMET,222,END=930,ERR=940) STRING,HEAD
      CALL UPPERC(STRING)
      CALL UPPERC(HEAD)
C
      IF(HEAD.EQ.'COMMENT') THEN
        IF(NCOM.EQ.MAXCOM) THEN
           WRITE(LFNERR,201)
201        FORMAT(' SR R2RDMH: TOO MANY COMMENT LINES')
        ELSE
          NCOM=NCOM+1
          COMENT(NCOM)=STRING
        END IF
C
C   PGM / RUN BY / DATE
      ELSEIF(HEAD.EQ.'PGM / RUN BY / DATE ') THEN
        READ(STRING,2,ERR=940) PRGNAM,RUNBY,CRDATE,CRTIME
2       FORMAT(A20,A20,A9,1X,A5)
        JPGM=1
C
C   MARKER NAME
      ELSEIF(HEAD.EQ.'MARKER NAME') THEN
        READ(STRING,4,ERR=940) STANAM
4       FORMAT(A60)
        JSTA=1
C
C   MARKER NUMBER
      ELSEIF(HEAD.EQ.'MARKER NUMBER') THEN
        READ(STRING,6,ERR=940) STANUM
6       FORMAT(A20)
C
C   # / TYPES OF OBSERV
      ELSEIF(HEAD.EQ.'# / TYPES OF OBSERV') THEN
        READ(STRING,5,ERR=940) NUMTYP,(OBSTYP(K),K=1,NUMTYP)
5       FORMAT(I6,9(4X,A2))
        JNUM=1
C
C   SENSOR MOD
      ELSEIF(HEAD.EQ.'SENSOR MOD/TYPE/ACC') THEN
        READ(STRING,7,ERR=940) SMOD(iM),STYP(iM),SACC(iM),SOBSM(iM)
7       FORMAT(A20,A20,6X,F7.1,4X,A2,1X)
        iM = iM + 1
C
C   SENSOR POS
      ELSEIF(HEAD.EQ.'SENSOR POS XYZ/H') THEN
        READ(STRING,8,ERR=940) SX(iP),SY(iP),SZ(iP),SH(iP),SOBSP(iP)
8       FORMAT(3F14.4,1F14.4,1X,A2,1X)
        iP = iP + 1
C
C  BLANK LINE OR END OF HEADER
      ELSEIF ((HEAD.EQ.'END OF HEADER').OR.
     1        (IRXVRS.EQ.1.AND.STRING.EQ.' ')) THEN
        GOTO 300
      ELSE
C  UNKNOWN RECORD TYPE
        WRITE(LFNERR,941) STRING,HEAD
941     FORMAT(' SR R2RDMH: UNKNOWN HEADER RECORD TYPE FOUND:',
     1       /,1X,2A)
      END IF
      GOTO 200
C
C  ARE ALL NECESSARY RECORDS READ?
300   IF(JPGM.EQ.0) WRITE(LFNERR,301) '"PGM / RUN BY / DATE "'
      IF(JSTA.EQ.0) WRITE(LFNERR,301) '"MARKER NAME "'
      IF(JNUM.EQ.0) WRITE(LFNERR,301) '"# / TYPES OF OBSERV "'
301   FORMAT(' SR R2RDMH: ',A,' RECORD MISSING')
C
900   IRCODE=0
      GOTO 999
C
C  WRONG FILE TYPE
910   IRCODE=1
      WRITE(LFNERR,911) RXT
911   FORMAT(' SR R2RDMH: WRONG FILE TYPE: "',A,'"')
      BACKSPACE LFNMET
      GOTO 999
C
C  NOT ANTICIPATED VERSION NUMBER
920   IRCODE=2
      WRITE(LFNERR,921) IRXVRS
921   FORMAT(' SR R2RDMH: NOT ANTICIPATED VERSION NUMBER:',I5)
      GOTO 999
C
C  END OF FILE WITHIN HEADER
930   IRCODE=3
      WRITE(LFNERR,931)
931   FORMAT(' SR R2RDMH: END OF FILE WITHIN HEADER')
      GOTO 999
C
C  ERROR DECODING DATA
940   IRCODE=4
      WRITE(LFNERR,942) STRING,HEAD(1:19)
942   FORMAT(' SR R2RDMH: ERROR DECODING DATA ON THE FOLLOWING LINE:',
     1       /,1X,2A)
      GOTO 999
C
C  FIRST LINE NOT VERSION LINE
950   IRCODE=5
      WRITE(LFNERR,951) STRING,HEAD
951   FORMAT(' SR R2RDMH: FIRST LINE NOT "VERSION"-LINE:',
     1       /,1X,2A)
      GOTO 999
C
C  END OF FILE AT FIRST RECORD
990   IRCODE=9
      GOTO 999
C
999   RETURN
C
      END SUBROUTINE
C
      END MODULE
