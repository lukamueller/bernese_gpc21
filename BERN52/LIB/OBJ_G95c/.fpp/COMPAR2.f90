
! -------------------------------------------------------------------------
! Bernese GNSS Software Version 5.2
! -------------------------------------------------------------------------

PROGRAM compar2

! -------------------------------------------------------------------------
! Purpose:    Comparing coordinate files, estimate velocities
!
! Author:     R. Dach
!
! Created:    16-Mar-2016
!
! Changes:    03-Aug-2016 AV: Init ADDNEQ-opt structure opt%elimi
!
! Copyright:  Astronomical Institute
!             University of Bern
!             Switzerland
! -------------------------------------------------------------------------

  USE m_bern,   ONLY: i4b, r8b, lfnprt, lfnerr, shortLineLength, &
                      timStrgLength, fileNameLength, staNameLength, &
                      staFlagLength
  USE m_maxdim, ONLY: maxsta
  USE m_time,   ONLY: t_timint, OPERATOR(+)
  USE m_cpu,    ONLY: cpu_start
  USE d_inpkey, ONLY: inpKey,init_inpkey
  USE d_neq,    ONLY: t_neq
  USE d_datum,  ONLY: datum, t_datum
  USE d_stalst, ONLY: t_stalist
  USE p_addneq, ONLY: t_dx,opt

  USE s_readinpf
  USE s_opnsys
  USE s_defcon
  USE s_pritit
  USE s_prflna
  USE s_exitrc
  USE s_cmprdopt
  USE s_timst2
  USE s_getdat
  USE s_gtflna
  USE s_getco3
  USE s_gtvelo
  USE s_crdvel
  USE s_alcerr
  USE s_statis
  USE s_xyzell
  USE s_prista
  USE s_privel
  USE s_neqinit
  USE s_neqalloc
  USE s_rdstacrx
  USE s_difcomp
  USE s_difprint
  USE s_crdstore
  USE s_syminvg
  USE s_solve
  USE s_writstsg
  USE s_cmpbsl
  USE s_jmt
  USE s_psdcorr
  USE f_djul
  USE f_ikf
  USE f_parfac

  IMPLICIT NONE

! Local parameters
! ----------------
  CHARACTER(LEN=7), PARAMETER :: srName = 'COMPAR2'
  REAL(r8b),        PARAMETER :: dtSim  = 1d0/86400d0


! Local variables
! ---------------
  TYPE(t_neq)                                 :: neq_1
  TYPE(t_neq),  DIMENSION(:),   POINTER       :: neq
  TYPE(t_dx),   DIMENSION(:,:), ALLOCATABLE   :: dx
  TYPE(t_datum)                               :: datVel
  TYPE(t_staList)                             :: staLst
  TYPE(t_timint)                              :: crdWindow, hlpWin

  CHARACTER(LEN=fileNameLength)                              :: bslfil
  CHARACTER(LEN=fileNameLength)                              :: lstfil
  CHARACTER(LEN=fileNameLength), DIMENSION(:,:), POINTER     :: filnam
  CHARACTER(LEN=staFlagLength),  DIMENSION(:),   POINTER     :: flags
                                          ! flags of coordinates to be
                                          ! included in the comparison
                                          ! @: all; #: all non-blank flags
  CHARACTER(LEN=staNameLength),  DIMENSION(:),   POINTER     :: stName
  CHARACTER(LEN=staNameLength),  DIMENSION(:),   ALLOCATABLE :: staList, hlpList
  CHARACTER(LEN=staFlagLength),  DIMENSION(:),   POINTER     :: hlpflg
  CHARACTER(LEN=staFlagLength),  DIMENSION(:),   ALLOCATABLE :: velflg
  CHARACTER(LEN=shortLineLength)                             :: title
  CHARACTER(LEN=timStrgLength)                               :: epofil
  CHARACTER(LEN=16)                                          :: datum0
  CHARACTER(LEN= 6),             DIMENSION(:),   ALLOCATABLE :: yyddds

  INTEGER(i4b)                              :: iFil
  INTEGER(i4b)                              :: nFil
  INTEGER(i4b)                              :: nFlag
  INTEGER(i4b)                              :: iSta, jSta, kSta
  INTEGER(i4b)                              :: nSta
  INTEGER(i4b)                              :: iPar
  INTEGER(i4b)                              :: ii, jj
  INTEGER(i4b)                              :: nStat
  INTEGER(i4b), DIMENSION(:,:), ALLOCATABLE :: staIndex, hlpIndex
  INTEGER(i4b), DIMENSION(:),   ALLOCATABLE :: stanum
  INTEGER(i4b), DIMENSION(:),   ALLOCATABLE :: icentr
  INTEGER(i4b)                              :: iopbas
  INTEGER(i4b)                              :: estVelo
  INTEGER(i4b)                              :: nSing
  INTEGER(i4b)                              :: ircVel
  INTEGER(i4b)                              :: yyyy, mm, ssss
  INTEGER(i4b)                              :: iac, irc

  REAL(r8b)                                 :: timfil
  REAL(r8b),    DIMENSION(:,:), POINTER     :: hlpxyz
  REAL(r8b),    DIMENSION(:,:), ALLOCATABLE :: xvel
  REAL(r8b),    DIMENSION(:,:), ALLOCATABLE :: statist
  REAL(r8b),    DIMENSION(:,:), ALLOCATABLE :: xstat
  REAL(r8b),    DIMENSION(:,:), ALLOCATABLE :: xstell
  REAL(r8b),    DIMENSION(:,:), ALLOCATABLE :: xstecc
  REAL(r8b),    DIMENSION(:),   ALLOCATABLE :: staEpo
  REAL(r8b)                                 :: dd
  REAL(r8b)                                 :: dt1, dt2
  REAL(r8b),    DIMENSION(3)                :: anor
  REAL(r8b),    DIMENSION(2)                :: bnor
  REAL(r8b),    DIMENSION(2)                :: xxx

  LOGICAL,      DIMENSION(:),   ALLOCATABLE :: vfound
  LOGICAL                                   :: ynPSD


! Start CPU Counter
! -----------------
  CALL cpu_start(.TRUE.)

! Get the name of the input file
! ------------------------------
  CALL init_inpkey(inpKey)
  CALL readinpf(' ',inpKey)

! Open system files, define constants
! -----------------------------------
  CALL opnsys
  CALL defcon(1)

! Automatic output generation
! ---------------------------
  CALL pritit('COMPAR2','Coordinate comparison',131)
  CALL prflna(131)

! Get the file names and the processing options
! ---------------------------------------------
  CALL cmprdopt(nFil,filnam,nflag,flags,bslfil,iopbas,estVelo,opt)

  ALLOCATE(neq(nFil),stat=iac)
  CALL alcerr(iac,'neq',(/nFil/),srName)

  ALLOCATE(yyddds(nFil),stat=iac)
  CALL alcerr(iac,'yyddds',(/nFil/),srName)
  yyddds = ''

! Init some components of the ADDNEQ-opt structure
! ------------------------------------------------
  ALLOCATE(opt%req(0),stat=iac)
  CALL alcerr(iac,'opt%req',(/0/),srname)

  ALLOCATE(opt%sigma(0),stat=iac)
  CALL alcerr(iac,'opt%sigma',(/0/),srname)

  ALLOCATE(opt%elimi(0),stat=iac)
  CALL alcerr(iac,'opt%elimi',(/0/),srname)

  ALLOCATE(opt%neqFileName(nFil),stat=iac)
  CALL alcerr(iac,'opt%neqFileName',(/nFil/),srName)
  opt%neqFileName(1:nFil) = filnam(1,1:nFil)

! Prepare the global list of stations
! -----------------------------------
  datum%name = ''

  nstat = 0
  ALLOCATE(staList(maxsta),stat=iac)
  CALL alcerr(iac,'staList',(/maxsta/),srName)
  staList = ''

  ALLOCATE(staIndex(maxsta,0:nFil),stat=iac)
  CALL alcerr(iac,'staIndex',(/maxsta,nFil+1/),srName)
  staIndex = 0


! Read station coordinates, put them into the NEQ-structure and
! generate the global list of stations
! -------------------------------------------------------------
  WRITE(lfnprt,'(A,/,1X,131("-"))') &
        ' Num  Filename                          ' // &
        '#sta  Coordinate epoch     Title in file'

  DO ifil = 1, nfil

    ! Read all station coordinates with specified flags
    NULLIFY(stname)
    NULLIFY(hlpflg)
    NULLIFY(hlpxyz)

    CALL getco3(filnam(1,iFil),nFlag,flags,nsta,stname,staflg=hlpflg, &
                xstat=hlpxyz,datum=datum0,title=title,timcrd=timfil)

    ! Check the datum string
    IF ( datum%name == '' ) THEN
      datum%name = datum0
      CALL getdat(datum%name,datum%aell,datum%bell,datum%dxell, &
              datum%drell,datum%scell)
    ENDIF
    IF ( datum%name /= datum0 ) THEN
      WRITE(lfnerr,'(/,A,/,17X,A,/)') &
      ' ### PG COMPAR2: The coordinate files you are going to compare refer to', &
      'different geodetic datum: "'//TRIM(datum%name)//'" and "'//TRIM(datum0)//'"'
    ENDIF

    ! Report to program output
    CALL timst2(1,1,timfil,epofil)
    IF ( LEN_TRIM(title) <=65 ) THEN
      WRITE(lfnprt,'(I4,2X,A32,I6,2X,A,2X,A)') &
            ifil,filnam(1,iFil), nsta, epofil, TRIM(title(1:65))
    ELSE
      WRITE(lfnprt,'(I4,2X,A32,I6,2X,A,2X,A)') &
            ifil,filnam(1,iFil), nsta, epofil, TRIM(title(1:61)) // ' ...'
    ENDIF

    ! Fill the epoch string
    CALL jmt(timfil,yyyy,mm,dd)
    ssss = NINT(timfil - djul(yyyy,1,1d0) + 1) * 10
    WRITE(yyddds(ifil),'(I2,I4.4)') MOD(yyyy,100), ssss

    ! Get the coordinate time window
    IF ( ifil == 1 ) THEN
      crdWindow%t(:) = timfil
    ELSE
      hlpWin%t(:) = timfil
      crdWindow = crdWindow + hlpWin
    ENDIF

    ! Adopt the epoch of the coordinates
    IF (opt%timRefCrd /= 0D0 .AND. estVelo /= 1) THEN

      ALLOCATE(xvel(3,nsta),stat=iac)
      CALL alcerr(iac,'xvel',(/3,nsta/),srname)
      ALLOCATE(velflg(nsta),stat=iac)
      CALL alcerr(iac,'velflg',(/nsta/),srName)
      ALLOCATE(vfound(nsta),stat=iac)
      CALL alcerr(iac,'vfound',(/nsta/),srname)

      CALL gtvelo('VELAPR',0,nFlag,flags,nSta, &
                  hlpxyz,stname,xvel,velflg,vfound,ircVel,datVel%name)

      ! Check the datum string
      IF ( datVel%name /= '' ) THEN
        CALL getdat(datVel%name,datVel%aell,datVel%bell,datVel%dxell, &
                    datVel%drell,datVel%scell,datVel%type)
      ENDIF

      IF (ircVel == 0) THEN
        CALL crdvel(NSTA,hlpxyz,xvel,vfound,opt%timRefCrd,timfil)

        DO ista=1,nsta
          anor(1:3) = hlpxyz(1:3,ista)
          CALL PSDCORR(stname(ista),timfil,datVel,hlpxyz(1:3,ista),ynPSD)
          hlpxyz(1:3,ista) = hlpxyz(1:3,ista) - 2*(hlpxyz(1:3,ista)-anor(1:3))
        ENDDO

!        timfil = opt%timRefCrd
      ENDIF

      DEALLOCATE(xvel,stat=iac)
      DEALLOCATE(velflg,stat=iac)
      DEALLOCATE(vfound,stat=iac)
    ENDIF


    ! Put the coordinate information into the NEQ structure
    CALL neqinit(neq(ifil))
    CALL neqalloc(neq(ifil),3*nsta)
    ALLOCATE(neq(ifil)%xxx(3*nsta),stat=iac)
    CALL alcerr(iac,'neq%xxx',(/3*nsta/),srName)
    iPar = 0
    DO iSta = 1,nSta
      DO ii = 1,3
        iPar = iPar + 1
        neq(ifil)%par(ipar)%locq(:)    = 0
        neq(ifil)%par(ipar)%locq(1)    = 1
        neq(ifil)%par(ipar)%locq(2)    = iSta
        neq(ifil)%par(ipar)%locq(3)    = ii
        neq(ifil)%par(ipar)%locq(4)    = 1
        neq(ifil)%par(iPar)%name       = stname(iSta)
        neq(ifil)%par(iPar)%time%mean  = timfil
        neq(ifil)%par(iPar)%time%half  = 0d0
        neq(ifil)%par(ipar)%scale      = 1d0

        neq(ifil)%par(ipar)%x0         = 0d0
        neq(ifil)%xxx(ipar)            = hlpxyz(ii,iSta)
        neq(ifil)%anor(ikf(ipar,ipar)) = 1d0
        neq(ifil)%bnor(ipar)           = hlpxyz(ii,iSta)
      ENDDO
    ENDDO

    ! finish processing a coordinate input file
    neq(ifil)%misc%npar = iPar

    ! Apply station renaming from sta-info file
    CALL rdstacrx(ifil,neq(ifil))

    DO iPar = 1,neq(ifil)%misc%npar
      IF ( neq(ifil)%par(ipar)%locq(1) /= 1 ) CYCLE
      IF ( neq(ifil)%par(ipar)%locq(3) /= 1 ) CYCLE

      ! Check whether the station is already in the global list
      kSta = 0
      DO jSta = 1,nstat
        IF ( neq(ifil)%par(ipar)%name(1:staNameLength) == staList(jSta) ) THEN
          kSta = jSta
          EXIT
        ENDIF
      ENDDO

      ! Add a new station/extent the list (if needed)
      IF ( kSta == 0 ) THEN
        IF ( nstat == SIZE(staList) ) THEN
          ALLOCATE(hlpList(nstat),stat=iac)
          CALL alcerr(iac,'hlpList',(/nstat/),srName)
          hlpList = staList
          DEALLOCATE(staList,stat=iac)
          ALLOCATE(staList(nstat+maxsta),stat=iac)
          CALL alcerr(iac,'staList',(/nstat+maxsta/),srName)
          staList = ''
          staList(1:nstat)=hlpList(1:nstat)
          DEALLOCATE(hlpList,stat=iac)
          ALLOCATE(hlpIndex(nstat,0:nfil),stat=iac)
          CALL alcerr(iac,'hlpIndex',(/nstat,nfil+1/),srName)
          hlpIndex = staIndex
          DEALLOCATE(staIndex,stat=iac)
          ALLOCATE(staIndex(nstat+maxsta,0:nfil),stat=iac)
          CALL alcerr(iac,'staIndex',(/nstat+maxsta,nfil+1/),srName)
          staIndex = 0
          staIndex(1:nstat,0:nfil)=hlpIndex(1:nstat,0:nfil)
          DEALLOCATE(hlpIndex,stat=iac)
        ENDIF

        nStat = nStat + 1
        kSta = nStat
        staList(nStat) = neq(ifil)%par(ipar)%name(1:staNameLength)
      ENDIF
      staIndex(ksta,ifil) = ipar
    ENDDO

    DEALLOCATE(stname,stat=iac)
    DEALLOCATE(hlpflg,stat=iac)
    DEALLOCATE(hlpxyz,stat=iac)
  ENDDO ! Next coordinate file


! Sort the stations in the global list
! ------------------------------------
  ALLOCATE(hlpList(1),stat=iac)
  CALL alcerr(iac,'hlpList',(/1/),srName)
  ALLOCATE(hlpIndex(1,0:nfil),stat=iac)
  CALL alcerr(iac,'hlpIndex',(/1,nfil+1/),srName)

  kSta = 1
  DO WHILE ( kSta /= 0 )
    kSta = 0
    DO iSta = 1,nStat-1
      IF ( staList(iSta) > staList(iSta+1) ) THEN
        kSta = iSta
        hlpList(1)      = staList(iSta)
        staList(iSta)   = staList(iSta+1)
        staList(iSta+1) = hlpList(1)

        hlpIndex(1,0:nFil)      = staIndex(iSta,0:nFil)
        staIndex(iSta,0:nFil)   = staIndex(iSta+1,0:nFil)
        staIndex(iSta+1,0:nFil) = hlpIndex(1,0:nFil)
      ENDIF
    ENDDO
  ENDDO

  DEALLOCATE(hlpList,stat=iac)
  DEALLOCATE(hlpIndex,stat=iac)


! Compute the mean coordinates of the stations
! --------------------------------------------
  ALLOCATE(staEpo(nstat),stat=iac)
  CALL alcerr(iac,'staEpo',(/nstat/),srName)

  ALLOCATE(xstat(3,nstat),stat=iac)
  CALL alcerr(iac,'xstat',(/3,nstat/),srName)
  ALLOCATE(xvel(3,nstat),stat=iac)
  CALL alcerr(iac,'xvel',(/3,nstat/),srName)
  ALLOCATE(xstell(3,nstat),stat=iac)
  CALL alcerr(iac,'xstell',(/3,nstat/),srName)
  ALLOCATE(xstecc(3,nstat),stat=iac)
  CALL alcerr(iac,'xstecc',(/3,nstat/),srName)
  ALLOCATE(stanum(nstat),stat=iac)
  CALL alcerr(iac,'stanum',(/nstat/),srName)
  ALLOCATE(icentr(nstat),stat=iac)
  CALL alcerr(iac,'icentr',(/nstat/),srName)


  CALL neqinit(neq_1)
  CALL neqalloc(neq_1,3*nstat*(estVelo+1))
  ALLOCATE(neq_1%xxx(3*nstat*(estVelo+1)),stat=iac)
  CALL alcerr(iac,'neq_1%xxx',(/3*nstat*(estVelo+1)/),srName)

  iPar = 0
  DO iSta = 1,nStat
    DO ii = 1,1+estVelo
      DO jj = 0,2
        iPar = iPar + 1
        neq_1%par(ipar)%locq(:)    = 0
        neq_1%par(ipar)%locq(1)    = 1
        neq_1%par(ipar)%locq(2)    = iSta
        neq_1%par(ipar)%locq(3)    = jj+1
        neq_1%par(ipar)%locq(4)    = ii
        neq_1%par(iPar)%name       = staList(iSta)
        neq_1%par(iPar)%time%mean  = 0d0
        neq_1%par(iPar)%time%half  = 0d0
        neq_1%par(ipar)%scale      = 1d0
        neq_1%par(ipar)%x0         = 0d0
        neq_1%xxx(ipar)            = 0d0
        neq_1%anor(ikf(ipar,ipar)) = 1d0

        ! Simply average the coordinates
        IF ( estVelo == 0 ) THEN
          jSta = 0
          DO iFil = 1,nFil
            IF ( staIndex(iSta,iFil) == 0 ) CYCLE
            jSta = jSta + 1
            neq_1%xxx(ipar) = neq_1%xxx(ipar) + neq(iFil)%xxx(staIndex(iSta,iFil)+jj)
            neq_1%par(iPar)%time%mean = neq_1%par(iPar)%time%mean + neq(iFil)%par(staIndex(iSta,iFil)+jj)%time%mean
          ENDDO
          neq_1%xxx(ipar) = neq_1%xxx(ipar) / jSta
          neq_1%par(iPar)%time%mean = neq_1%par(iPar)%time%mean / jSta
          neq_1%bnor(ipar) = neq_1%xxx(ipar)

        ! Estimate coordinates and velocities
        ELSE
          neq_1%par(iPar)%time%mean = crdWindow%t(ii)
          IF ( ii == 2 ) THEN
            anor = 0d0
            bnor = 0d0
            jSta = 0
            DO iFil = 1,nFil
              IF ( staIndex(iSta,iFil) == 0 ) CYCLE
              jSta = jSta + 1
              dt1 = parfac(neq(iFil)%par(staIndex(iSta,iFil)+jj)%time%mean, &
                           crdWindow%t(1), crdWindow%t(2)-crdWindow%t(1),dtSim)
              dt2 = parfac(neq(iFil)%par(staIndex(iSta,iFil)+jj)%time%mean, &
                           crdWindow%t(2), crdWindow%t(2)-crdWindow%t(1),dtSim)
              anor(1) = anor(1) + dt1**2
              anor(2) = anor(2) + dt1*dt2
              anor(3) = anor(3) + dt2**2
              bnor(1) = bnor(1) + dt1 * neq(iFil)%xxx(staIndex(iSta,iFil)+jj)
              bnor(2) = bnor(2) + dt2 * neq(iFil)%xxx(staIndex(iSta,iFil)+jj)
            ENDDO
            IF ( jSta == 1 ) THEN
              anor(1) = anor(1) + 10000d0
              anor(2) = anor(2) - 10000d0
              anor(3) = anor(3) + 10000d0
            ENDIF
            CALL syminvg(2,anor,0,nSing)
            IF ( nSing == 0 ) THEN
              CALL solve(2,anor,bnor,xxx)
              neq_1%xxx(ipar-3) = xxx(1)
              neq_1%xxx(ipar)   = xxx(2)
            ELSE
              neq_1%xxx(ipar-3) = bnor(1)
              neq_1%xxx(ipar)   = bnor(2)
            ENDIF
            neq_1%bnor(ipar-3) = neq_1%xxx(ipar-3)
            neq_1%bnor(ipar)   = neq_1%xxx(ipar)
          ENDIF
        ENDIF

        IF ( ii == 1 ) THEN
          staEpo(iSta) = neq_1%par(iPar)%time%mean
          xStat(jj+1,iSta) = neq_1%xxx(ipar)
          stanum(ista) = ista
          IF ( jj == 0 ) staIndex(ista,0) = ipar
        ELSE
          xVel(jj+1,iSta)  = ( neq_1%xxx(ipar) - neq_1%xxx(ipar-3)) / &
                             ( neq_1%par(iPar)%time%mean - neq_1%par(iPar-3)%time%mean ) * 365.25d0
          IF ( opt%timRefCrd == 0d0 ) THEN
            xStat(jj+1,iSta)   = neq_1%xxx(ipar-3)
          ELSE
            xStat(jj+1,iSta)   = neq_1%xxx(ipar-3) + xVel(jj+1,iSta) * (opt%timRefCrd-neq_1%par(iPar-3)%time%mean) / 365.25d0
            neq_1%par(iPar-3)%time%mean = opt%timRefCrd
            neq_1%xxx(ipar-3)  = xStat(jj+1,iSta)
            neq_1%bnor(ipar-3) = neq_1%xxx(ipar-3)
          ENDIF
        ENDIF
      ENDDO
    ENDDO
    icentr(ista)     = ista
    xstecc(1:3,iSta) = 0d0
    CALL xyzell(datum%aell,datum%bell,datum%dxell,datum%drell,datum%scell, &
                xStat(1:3,ista),xstell(1:3,ista))
  ENDDO
  neq_1%misc%npar = iPar

  IF ( estVelo == 1 ) THEN
    IF ( opt%timRefCrd == 0d0 ) opt%timRefCrd = crdWindow%t(1)
  ELSEIF ( opt%timRefCrd == 0d0 ) THEN
    CALL statis(nstat,staEpo,xMed=opt%timRefCrd)
  ENDIF


! Report the mean coordinate
! --------------------------
  WRITE(lfnprt,'(//,2(1X,A,/))') &
  'Averaged station coordinates:', '----------------------------'
  CALL prista(nstat,staList(:),stanum,xStat,xStell,xStecc,0,iCentr, &
                  0,(/1/),0,(/1/),opt%timRefCrd,datum%name)

! Report the mean velocities
! --------------------------
  IF ( estVelo == 1 ) THEN
    WRITE(lfnprt,'(//,2(1X,A,/))') &
    'Averaged station velocities:', '----------------------------'
    CALL privel(nstat,staList(:),stanum,xStell,xVel,0,(/1/),0,(/1/))
  ENDIF

! Run through the comparison procedure from ADDNEQ2
! -------------------------------------------------
  ALLOCATE( dx(nFil,neq_1%misc%npar), stat=iac )
  CALL alcerr(iac, 'dx', (/nFil,neq_1%misc%npar/), srName)
  ALLOCATE( statist(nFil,9), stat=iac )
  CALL alcerr(iac, 'statist', (/nFil,9/), srName)
  statist = 0

  DO iFil = 1,nFil
    CALL difcomp(ifil,neq_1,neq(iFil),dx(iFil,:))
  ENDDO

  ! Print the differences; remove stations that are "problematic"
  ! according to the repeatability  verification options
  ALLOCATE(hlpList(nstat),stat=iac)
  CALL alcerr(iac,'hlpList',(/nstat/),srName)
  hlpList = staList(1:nstat)

  CALL difprint(neq_1,nFil,dx,statist,nstat,hlpList)

! Write the list of stations accepted by the repeatability verification
! ---------------------------------------------------------------------
  CALL gtflna(0,'STALISTRS',lstFil,irc)
  IF ( LEN_TRIM(lstFil) > 0 ) THEN
    ALLOCATE(staLst%staNam(nstat),stat=iac)
    CALL alcerr(iac, 'staLst%staNam', (/nstat/), srName)
    staLst%nSta = 0
    DO iSta = 1,nstat
      IF ( LEN_TRIM(hlpList(iSta)) == 0 ) CYCLE
      staLst%nSta = staLst%nSta + 1
      staLst%staNam(staLst%nSta) = hlpList(iSta)
    ENDDO
    staLst%title = opt%title
    CALL writstsg(lstFil, 0, staLst)
    DEALLOCATE(staLst%staNam,stat=iac)
  ENDIF

  ! Clean the result NEQ-structure from the not-accepted stations
  DO ipar = 1,neq_1%misc%npar
    IF ( neq_1%par(ipar)%locq(1) == 1 ) THEN
      jSta = 0
      DO iSta = 1,nstat
        IF ( hlpList(iSta) == neq_1%par(ipar)%name(1:staNameLength) ) THEN
          jSta = iSta
          EXIT
        ENDIF
      ENDDO
      IF ( jSta == 0 ) neq_1%par(ipar)%locq(1) = 0
    ENDIF
  ENDDO

  DEALLOCATE(hlpList,stat=iac)


! Store mean coordinates or coordinates and velocities
! ----------------------------------------------------
  CALL crdstore(neq_1)


! Create baseline statistics if requested
! ---------------------------------------
  IF ( iopbas > 0 .AND. nfil > 1 ) &
    CALL cmpbsl(iopbas,nstat,staList,staIndex,nfil,yyddds,neq)


! End the program
! ---------------
  CALL exitrc(0)

END PROGRAM compar2
