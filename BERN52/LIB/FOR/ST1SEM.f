      MODULE s_ST1SEM
      CONTAINS

C*
      SUBROUTINE ST1SEM(XSTA,XSUN,XMON,F2SUN,F2MON,XCOSTA)
CC
CC  PURPOSE   :  .
CC THIS SUBROUTINE GIVES THE OUT-OF-PHASE CORRECTIONS INDUCED BY
CC MANTLE INELASTICITY IN THE DIURNAL BAND
CC
CC
CC PARAMETERS :
CC         IN :  XSTA,XSUN,XMON,F2SUN,F2MON
CC     IN/OUT :  XCOSTA
CC
CC REMARKS    :  ---
CC
CC AUTHOR     :  V. DEHANT, S. MATHEWS AND J. GIPSON
CC
CC VERSION    :  4.1
CC
CC CREATED    :  06-OCT-1997
CC
CC CHANGES    :  06-OCT-97 : TS: ADDEPTED FOR THE BERNESE SOFTWARE
CC               23-JUN-05 : MM: IMPLICIT NONE AND DECLARATIONS ADDED
CC
CC COPYRIGHT  :  ASTRONOMICAL INSTITUTE
CC      1997     UNIVERSITY OF BERN
CC               SWITZERLAND
CC
C*
      IMPLICIT NONE
C
C DECLARATIONS INSTEAD OF IMPLICIT
C --------------------------------
      REAL*8    COSLA , COSPHI, CTWOLA, DE    , DEMON , DESUN , DHI   ,
     1          DLI   , DN    , DNMON , DNSUN , DR    , DRMON , DRSUN ,
     2          F2MON , F2SUN , RMON  , RSTA  , RSUN  , SINLA , SINPHI,
     3          STWOLA
C
CCC       IMPLICIT REAL*8 (A-H,O-Z)
C
      REAL*8 XSTA(*),XSUN(*),XMON(*),XCOSTA(*)
C
      DATA DHI/-0.0022D0/,DLI/-0.0007D0/
C
      RSTA=DSQRT(XSTA(1)**2+XSTA(2)**2+XSTA(3)**2)
      SINPHI=XSTA(3)/RSTA
      COSPHI=DSQRT(XSTA(1)**2+XSTA(2)**2)/RSTA
      SINLA=XSTA(2)/COSPHI/RSTA
      COSLA=XSTA(1)/COSPHI/RSTA
      CTWOLA=COSLA**2-SINLA**2
      STWOLA=2.D0*COSLA*SINLA
      RMON=XMON(4)
      RSUN=XSUN(4)
      DRSUN=-3.D0/4.D0*DHI*COSPHI**2*F2SUN*((XSUN(1)**2-XSUN(2)**2)*
     1       STWOLA-2.D0*XSUN(1)*XSUN(2)*CTWOLA)/RSUN**2
      DRMON=-3.D0/4.D0*DHI*COSPHI**2*F2MON*((XMON(1)**2-XMON(2)**2)*
     1       STWOLA-2.D0*XMON(1)*XMON(2)*CTWOLA)/RMON**2
      DNSUN=3.D0/2.D0*DLI*SINPHI*COSPHI*F2SUN*((XSUN(1)**2-
     1       XSUN(2)**2)*STWOLA-2.D0*XSUN(1)*XSUN(2)*CTWOLA)/RSUN**2
      DNMON=3.D0/2.D0*DLI*SINPHI*COSPHI*F2MON*((XMON(1)**2-
     1       XMON(2)**2)*STWOLA-2.D0*XMON(1)*XMON(2)*CTWOLA)/RMON**2
      DESUN=-3.D0/2.D0*DLI*COSPHI*F2SUN*((XSUN(1)**2-XSUN(2)**2)*
     1       CTWOLA+2.D0*XSUN(1)*XSUN(2)*STWOLA)/RSUN**2
      DEMON=-3.D0/2.D0*DLI*COSPHI*F2MON*((XMON(1)**2-XMON(2)**2)*
     1       CTWOLA+2.D0*XMON(1)*XMON(2)*STWOLA)/RMON**2
      DR=DRSUN+DRMON
      DN=DNSUN+DNMON
      DE=DESUN+DEMON
      XCOSTA(1)=DR*COSLA*COSPHI-DE*SINLA-DN*SINPHI*COSLA
      XCOSTA(2)=DR*SINLA*COSPHI+DE*COSLA-DN*SINPHI*SINLA
      XCOSTA(3)=DR*SINPHI+DN*COSPHI
C
      RETURN
      END SUBROUTINE

      END MODULE
