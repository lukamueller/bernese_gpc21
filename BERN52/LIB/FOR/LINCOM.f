      MODULE s_LINCOM
      CONTAINS

C*
      SUBROUTINE LINCOM(ICARR,ISVN,OBSL1,OBSL2,FLGL1,FLGL2,
     1                  OBSRES,FLGRES)
CC
CC NAME       :  LINCOM
CC
CC PURPOSE    :  FORM LINEAR COMBINATIONS OF THE TWO INDIVIDUAL
CC               CARRIER FREQUENCIES
CC
CC PARAMETERS :
CC         IN :  ICARR  : REQUESTED LINEAR COMBINATION        I*4
CC               ISVN   : SV NUMBER                           I*4
CC               OBSL1  : OBSERVATION IN L1                   R*8
CC               OBSL2  : OBSERVATION IN L2                   R*8
CC               FLGL1  : FLAG OF L1 OBSERVATION              CH*1
CC               FLGL2  : FLAG OF L2 OBSERVATION              CH*1
CC        OUT :  OBSRES : RESULTING OBSERVATION IN ICARR      R*8
CC               FLGRES : RESULTING FLAG                      CH*1
CC
CC REMARKS    :  ---
CC
CC AUTHOR     :  G.BEUTLER, M.ROTHACHER
CC
CC VERSION    :  3.4  (JAN 93)
CC
CC CREATED    :  88/05/16 13:44
CC
CC CHANGES    :  28-JUL-98 : HH: ADD SATELLITE TO PARAMETER LIST
CC               15-AUG-99 : JJ: RM UNUSED VAR IFIRST
CC               20-JAN-02 : MR,DS: HANDLE LC IN THE CASE OF ZERO VALUE
CC               16-JUN-05 : MM: UNUSED COMCONST.inc REMOVED
CC               23-JUN-05 : MM: IMPLICIT NONE AND DECLARATIONS ADDED
CC
CC COPYRIGHT  :  ASTRONOMICAL INSTITUTE
CC      1988     UNIVERSITY OF BERN
CC               SWITZERLAND
CC
C*
      USE f_ior
      IMPLICIT NONE
C
C DECLARATIONS INSTEAD OF IMPLICIT
C --------------------------------
      INTEGER*4 ICARR , INTEG , ISVN
C
      REAL*8    OBSL1 , OBSL2 , OBSRES
C
CCC       IMPLICIT REAL*8 (A-H,O-Z)
CCC       IMPLICIT INTEGER*4 (I-N)
C
      CHARACTER*1 FLGL1,FLGL2,FLGRES
C
      INCLUDE 'COMFREQ.inc'
C
C L1
      IF(ICARR.EQ.1)THEN
        OBSRES=OBSL1
        IF(OBSRES.NE.0.D0)THEN
          FLGRES=FLGL1
        ELSE
          FLGRES=CHAR(1)
        END IF
C
C L2
      ELSE IF(ICARR.EQ.2)THEN
        OBSRES=OBSL2
        IF(OBSRES.NE.0.D0)THEN
          FLGRES=FLGL2
        ELSE
          FLGRES=CHAR(1)
        END IF
C
C L3,L4,L5
      ELSE
        IF(OBSL1.EQ.0.D0.OR.OBSL2.EQ.0.D0)THEN
          OBSRES=0.D0
          FLGRES=CHAR(1)
        ELSE
          OBSRES=FACLIN(ICARR,1,ISVN)*OBSL1+FACLIN(ICARR,2,ISVN)*OBSL2
C
C CHANGE IN THE CASE OF ZERO OBSERVATION
          IF (OBSRES.EQ.0.D0) OBSRES=1.D-10
          INTEG=IOR(ICHAR(FLGL1),ICHAR(FLGL2))
          FLGRES=CHAR(INTEG)
        END IF
      END IF
C
      RETURN
      END SUBROUTINE

      END MODULE
