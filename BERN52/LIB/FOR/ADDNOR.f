      MODULE s_ADDNOR
      CONTAINS

C*
      SUBROUTINE ADDNOR(NEQ   ,NPAR  ,WGTGEN,P     ,AOBS  ,INDA  ,
     1                  INDI  ,INDK  ,HELP  ,MXCEQN,BOBS  ,ANOR  ,
     2                  BNOR  ,RMSSUM,NOBS  )
CC
CC NAME       :  ADDNOR
CC
CC PURPOSE    :  UPDATE NORMAL EQUATION SYSTEM IN BERNESE GPS
CC               SOFTWARE:
CC                                        T
CC               ANOR = ANOR + WGTGEN*AOBS * P * AOBS ,
CC                                        T
CC               BNOR = BNOR + WGTGEN*AOBS * P * BOBS
CC
CC PARAMETERS :
CC         IN :  NEQ    : NUMBER OF OBSERVATION EQUATIONS     I*4
CC               NPAR   : NUMBER OF PARAMETERS                I*4
CC               WGTGEN : GENERAL WEIGHT FACTOR               R*8
CC               P      : WEIGHT MATRIX, UPPER TRIANGULAR     R*8
CC                        PART, COLUMNWISE LINEARIZED
CC               AOBS   : FIST DESIGN MATRIX                  R*8
CC                        COLUMNWISE LINEARIZED
CC               INDA   : INDEX FOR MATRIX AOBS (SAME STRUC-  I*2
CC                        TURE) AS AOBS
CC               INDI   : AUXILIARY 1-DIM ARRAY               I*2
CC               INDK   : AUXILIARY 1-DIM ARRAY               I*2
CC               HELP   : AUXILIARY 1-DIM ARRAY               R*8
CC               MXCEQN : MAX. NUMBER OF EQNS PER EPOCH       I*4
CC               BOBS   : RIGHT HAND SIDE OF OBS EQNS.        R*8
CC     IN/OUT :  ANOR   : NORMAL EQN MATRIX, UPPER TRIANGULAR R*8
CC                        PART, COLUMNWISE LINEARIZED
CC               BNOR   : RIGHT HAND SIDE OF NEQ-SYSTEM       R*8
CC               RMSSUM : SUM OF RESIDUALS**2
CC               NOBS   : TOTAL NUMBER OF OBSERVATIONS        I*4
CC
CC REMARKS    :  ONLY NON-ZERO ELEMENTS ARE GIVEN IN AOBS.
CC               INDA CONTAINS INDICES OF THESE ELEMENTS IN
CC               "ORIGINAL" MATRIX
CC
CC AUTHOR     :  G.BEUTLER, M.ROTHACHER
CC
CC REMARK     :  ---
CC
CC VERSION    :  3.4  (JAN 93)
CC
CC CREATED    :  87/10/21 13:33
CC
CC CHANGES    :  21-JUN-92 : ??: TURBO-VERSION BY G.BEUTLER. "IEMPTY".
CC               12-APR-95 : ??: NEW VERSION BASED ON SR ADNHLP (GB)
CC               13-JUN-95 : ??: MAXSNG --> MAXPPE, MAXPPE=450
CC               22-JUN-95 : MR: REORDER DATA STATEMENT
CC               21-FEB-96 : TS: CHANGES MAXEQN/MAXPPE FROM 450 TO 525
CC               26-MAR-96 : TS: CHANGED MAXEQN FROM 525 TO 1375 FOR
CC                               CLOCKS
CC               06-JUN-96 : MR: PLATFORM-SPECIFIC DIMENSIONS
CC               21-JUL-96 : TS: INCREASED MAXEQN FROM 1350 --> 1500
CC                5-AUG-96 : TS: INCREASED MAXEQN FROM 1500 --> 1625
CC               27-SEP-96 : TS: INCREASED MAXEQN TO MAXSAS*MAXFLS=1750
CC               12-AUG-97 : SS: "MAXPPE" FROM 600 TO 2100
CC               25-NOV-97 : TS: MAXSAS IN INCLUDED FILE
CC               16-AUG-99 : RD: DIMENSIONS (SMALL/MEDIUM/LARGE)
CC               09-FEB-02 : RD: P: REAL*4->REAL*8
CC               17-FEB-03 : LM: USE M_MAXDIM, PREPROC COMMANDS
CC               27-AUG-03 : HU: SHARED DO LABELS REMOVED
CC               19-NOV-04 : RD: Interface for ADNHLP.f
CC               26-MAY-05 : GB/RD: DYN. ARRAYS->LOCALLY ALLOCATED ARRAYS
CC               23-JUN-05 : MM: IMPLICIT NONE AND DECLARATIONS ADDED
CC
CC COPYRIGHT  :  ASTRONOMICAL INSTITUTE
CC      1987     UNIVERSITY OF BERN
CC               SWITZERLAND
CC
C*
      USE m_bern, ONLY: i4b
c      USE m_maxdim, ONLY: MAXFLS, MAXSAS
      USE f_ikf
      USE s_adnhlp
      USE s_alcerr
      IMPLICIT NONE
C
C DECLARATIONS INSTEAD OF IMPLICIT
C --------------------------------
      INTEGER*4 IAC   , ICOL  , IEQ   , IFIRST, IK    , IP    ,
     1          IPAR  , KEQ   , KP    , KPAR  , L     , LEQ   , LINE  ,
     2          LK    , MXCEQN, MXCSNG, MXXEQN, NEQ   , NOBS  , NPAR  ,
     3          NPEFF
C
      REAL*8    HLPSKL, RMSHLP, RMSSUM, WGTGEN
C
CCC       IMPLICIT REAL*8 (A-H,O-Z)
CCC       IMPLICIT INTEGER*4 (I-N)
C
C
c      PARAMETER (MAXEQN=MAXSAS*MAXFLS)
C
      CHARACTER(LEN=6), PARAMETER :: SRNAME= 'ADDNOR'
      CHARACTER*6 MXNEQN,MXNSNG
C
      INTEGER*4 INDA(MXCEQN,*),INDI(*),INDK(*)
c      INTEGER*4 INDA(*),INDI(*),INDK(*)
c      INTEGER*4 NLIN(NPAR),IPAEXT(NPAR)
c      INTEGER*4 LINNUM(NEQ,NPAR),COLNUM(NEQ,NPAR)
      INTEGER(i4b), DIMENSION(:),   ALLOCATABLE :: NLIN                  ! (NPAR)
      INTEGER(i4b), DIMENSION(:),   ALLOCATABLE :: IPAEXT                ! (NPAR)
      INTEGER(i4b), DIMENSION(:,:), ALLOCATABLE :: LINNUM                ! (NEQ,NPAR)
      INTEGER(i4b), DIMENSION(:,:), ALLOCATABLE :: COLNUM                ! (NEQ,NPAR)
C
      REAL*8    AOBS(MXCEQN,*),BOBS(*),ANOR(*),BNOR(*),HELP(*)
c      REAL*8    AOBS(*),BOBS(*),ANOR(*),BNOR(*),HELP(*)
C
      REAL*8    P(*)
C
      COMMON/MCMEQN/MXXEQN,MXNEQN
      COMMON/MCMSNG/MXCSNG,MXNSNG
C
      DATA IFIRST/1/
C
      IF(IFIRST.EQ.1)THEN
        IFIRST = 0
c        CALL MAXTST(1,'ADDNOR',MXNEQN,MAXEQN,MXCEQN,IRC1)
c        IF(IRC1.NE.0) CALL EXITRC(2)
      END IF
C
C ALLOCATE LOCAL ARRAYS
      ALLOCATE(NLIN(NPAR),STAT=IAC)
      CALL ALCERR(IAC,'NLIN',(/NPAR/),SRNAME)
      NLIN=0
C
      ALLOCATE(IPAEXT(NPAR),STAT=IAC)
      CALL ALCERR(IAC,'IPAEXT',(/NPAR/),SRNAME)
      IPAEXT=0
C
      ALLOCATE(LINNUM(NEQ,NPAR),STAT=IAC)
      CALL ALCERR(IAC,'LINNUM',(/NEQ,NPAR/),SRNAME)
      LINNUM=0
C
      ALLOCATE(COLNUM(NEQ,NPAR),STAT=IAC)
      CALL ALCERR(IAC,'COLNUM',(/NEQ,NPAR/),SRNAME)
      COLNUM=0
C
C ORGANIZE PROCESSING
      CALL ADNHLP(NEQ,NPAR,INDA,NPAR,NEQ,MXCEQN,INDI,NPEFF,
     1            IPAEXT,NLIN,LINNUM,COLNUM)
C
C UPDATE NORMAL EQUATION SYSTEM
C -----------------------------
C
C HELP= ONE LINE OF MATRIX PRODUCT TRN(AOBS)*P
      DO 50 IP=1,NPEFF
        IPAR=IPAEXT(IP)
        DO 20 KEQ=1,NEQ
          HELP(KEQ)=0.D0
          DO 19 L=1,NLIN(IP)
            LINE=LINNUM(L,IP)
            ICOL=COLNUM(L,IP)
            LK=IKF(LINE,KEQ)
c            INDX=(ICOL-1)*MXCEQN+LINE
c            HELP(KEQ)=HELP(KEQ)+AOBS(INDX)*P(LK)
            HELP(KEQ)=HELP(KEQ)+AOBS(LINE,ICOL)*P(LK)
19        CONTINUE
20      CONTINUE
C
C TERMS "OBSERVED-COMPUTED"
C -------------------------
        HLPSKL=0.D0
        DO 35 LEQ=1,NEQ
          HLPSKL=HLPSKL+HELP(LEQ)*BOBS(LEQ)
35      CONTINUE
        BNOR(IPAR)=BNOR(IPAR)+HLPSKL*WGTGEN
        DO 40 KP=1,IP
          KPAR=IPAEXT(KP)
          HLPSKL=0.D0
          DO 36 L=1,NLIN(KP)
            LINE=LINNUM(L,KP)
            ICOL=COLNUM(L,KP)
c            INDX=(ICOL-1)*MXCEQN+LINE
c            HLPSKL=HLPSKL+HELP(LINE)*AOBS(INDX)
            HLPSKL=HLPSKL+HELP(LINE)*AOBS(LINE,ICOL)
36        CONTINUE
          IK=IKF(IPAR,KPAR)
          ANOR(IK)=ANOR(IK)+HLPSKL*WGTGEN
40      CONTINUE
50    CONTINUE
C
C SUM OF RESIDUALS SQUARE
      DO 60 IEQ=1,NEQ
        HELP(IEQ)=0.D0
        DO 61 KEQ=1,NEQ
          IK=IKF(IEQ,KEQ)
          HELP(IEQ)=HELP(IEQ)+BOBS(KEQ)*P(IK)
61      CONTINUE
60    CONTINUE
      RMSHLP=0.D0
      DO 70 KEQ=1,NEQ
        RMSHLP=RMSHLP+HELP(KEQ)*BOBS(KEQ)
70    CONTINUE
      RMSSUM=RMSSUM+RMSHLP*WGTGEN
C
C NUMBER OF OBSERVATIONS
      NOBS=NOBS+NEQ
C
C DEALLOCATE LOCAL ARRAYS
      DEALLOCATE(NLIN)
      DEALLOCATE(IPAEXT)
      DEALLOCATE(LINNUM)
      DEALLOCATE(COLNUM)
C
      RETURN
      END SUBROUTINE

      END MODULE
