      MODULE s_WRTCMD
      CONTAINS

C*
      SUBROUTINE WRTCMD(ILFNUM,CVAR,IVAL,LGH,ITYP)
CC
CC NAME       :  WRTCMD
CC
CC PURPOSE    :  WRITE A SETENV VARIABLE COMMAND
CC
CC PARAMETERS :
CC
CC         IN : ILFNUM: LOGICAL UNIT TO WRITE ON
CC              CVAR : CHARACTER VARIABLE TO BE WRITTEN
CC              IVAL : VALUE TO BE WRITTEN
CC              LGH  : LENGTH OF VALUE STRING
CC              ITYP : TYP OF COMMAND FILE
CC                     (1: MS-DOS; 2: VAX; 3: UNIX-SH; 4: UNIX-CSH; 5: PERL)
CC
CC REMARKS    :  *** MODFIED VERSION OF WRSET ***
CC
CC AUTHOR     :  T.A. SPRINGER, R. DACH
CC
CC VERSION    :  4.1
CC
CC CREATED    :  13-OCT-99
CC
CC CHANGES    :  12-NOV-99 : RD: USE SHELP FOR COPLETING THE STRING
CC               23-JUN-05 : MM: IMPLICIT NONE AND DECLARATIONS ADDED
CC
CC COPYRIGHT  :  ASTRONOMICAL INSTITUTE
CC      1999     UNIVERSITY OF BERN
CC               SWITZERLAND
CC
C*
      USE s_sjustl
      USE f_lengt0
      USE f_lengt1
      IMPLICIT NONE
C
C DECLARATIONS INSTEAD OF IMPLICIT
C --------------------------------
      INTEGER*4 I     , ILEN  , ILEN1 , ILEN2
C
      CHARACTER*(*) CVAR
      INTEGER*4     IVAL,ILFNUM,LGH,ITYP
      CHARACTER*80  CVAL,CMDLN,SHELP
C
      WRITE(CVAL,*) IVAL
      CALL SJUSTL(CVAL)
C
      DO I=1,LGH
        IF (LENGT0(CVAL).LT.LGH) THEN
          SHELP=CVAL
          CVAL = '0'//SHELP
        ENDIF
      ENDDO
C
      ILEN1 = LENGT1(CVAR)
      ILEN2 = LENGT0(CVAL)
C
      IF (ILEN2.GT.0) THEN
C
        GOTO (100,200,300,400,500) ITYP
C
C_KEEP_LOWER_ON
C
C MS-DOS COMMAND
C
100     CMDLN = 'SET '//CVAR(1:ILEN1)//'='//CVAL(1:ILEN2)
        GOTO 900
C
C VMS COMMAND
C
200     CMDLN = '$  '//CVAR(1:ILEN1)//' == "'//CVAL(1:ILEN2)//'"'
        GOTO 900
C
C UNIX SH COMMAND
C
300     CMDLN = CVAR(1:ILEN1)//'='//CVAL(1:ILEN2)//
     &          '; export '//CVAR(1:ILEN1)
        GOTO 900
C
C UNIX CSH COMMAND
C
400     CMDLN = 'setenv '//CVAR(1:ILEN1)//' '//CVAL(1:ILEN2)
        GOTO 900
C
C PERL COMMAND
C
500     CMDLN = '$'//CVAR(1:ILEN1)//' = "'//CVAL(1:ILEN2)//'";'
        GOTO 900
C
C_KEEP_LOWER_OFF
C
900     CONTINUE
C
        ILEN = LENGT0(CMDLN)
        WRITE(ILFNUM,*) CMDLN(1:ILEN)
C
      ENDIF
C
      RETURN
      END SUBROUTINE

      END MODULE
